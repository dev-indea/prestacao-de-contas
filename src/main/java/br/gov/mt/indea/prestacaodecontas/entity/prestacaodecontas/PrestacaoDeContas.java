package br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.prestacaodecontas.entity.dbindea.Unidade;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.Atividade;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.RecursosDiaria;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.StatusViagem;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.TipoEntregaRelatorioViagemFisico;
import br.gov.mt.indea.prestacaodecontas.util.LocalDateUtil;

@Audited
@Entity
@Table(name="prestacao_contas")
@SecondaryTables({
    @SecondaryTable(name="servidor"),
    @SecondaryTable(name="unidade")
})
public class PrestacaoDeContas extends BaseEntity<Long> implements Serializable{

	
	private static final long serialVersionUID = 3642609101467322939L;

	@Id
	@SequenceGenerator(name="prestacao_contas_seq", sequenceName="prestacao_contas_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="prestacao_contas_seq")
	private Long id;

	@Column(name="protocolo", length=15)
	private String protocolo;
	
	@Column(name="os", length=15)
	private String os;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@JoinColumn(name = "id_servidor")
	private Servidor servidor = new Servidor();
	
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name="entregue_em")
	private Date entregueEm;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_entrega_relatorio_fisico")
	private TipoEntregaRelatorioViagemFisico tipoEntregaRelatorioViagemFisico;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private StatusViagem status = StatusViagem.ABERTA;
	
	@Column(name="nr_boletim_urs", length=50)
	private String nrBoletimUrs;
	
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name="data_reprovacao")
	private Date dataReprovacao;
	
	@Column(name="motivo_reprovacao", length=2000)
	private String motivoReprovacao;
	
	@Column(name="motivo_cancelamento", length=2000)
	private String motivoCancelamento;
	
	@Transient
	private String motivoReabertura;
	
	@Column(name="id_unidade_origem")
	private String idUnidadeOrigem;
	
	@Transient
	private Unidade unidadeOrigem;
	
	@Column(name="id_unidade_solicitante")
	private String idUnidadeSolicitante;
	
	@Transient
	private Unidade unidadeSolicitante;
	
	@Column(name="id_unidade_autorizada")
	private String idUnidadeAutorizada;
	
	@Transient
	private Unidade unidadeAutorizada;
	
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name="dt_inicio")
	private Date dtInicio;
	
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name="dt_fim")
	private Date dtFim;
	
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name="data_cadastro")
	private Date dataCadastro;
	
	@Column(name="valor")
	private BigDecimal valor;
	
	@OneToMany(mappedBy="prestacaoDeContas", orphanRemoval=true, cascade={CascadeType.REMOVE}, fetch=FetchType.EAGER)
	@OrderBy("dataEnvio DESC")
	private List<HistoricoEnvioEmail> listaHistoricoEnvioEmail = new ArrayList<HistoricoEnvioEmail>();
	
	@OneToMany(mappedBy="prestacaoDeContas", orphanRemoval=true, cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
	@OrderBy("dataEvento DESC")
	private List<HistoricoViagem> listaHistoricoViagem = new ArrayList<HistoricoViagem>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="recursoDiaria")
	private RecursosDiaria recursoDiaria;
	
	@Enumerated(EnumType.STRING)
	@Column(name="atividade")
	private Atividade atividade;
	
	public Long getDiasUteisEmAtraso() {
		if (dtFim != null) {
			if (this.status == StatusViagem.ABERTA || this.status == StatusViagem.REABERTA) {
				Calendar c = Calendar.getInstance();
				c.setTime(dtFim);
				LocalDate dataFim = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));
				
				if (dataFim.isBefore(LocalDate.now()))
					return LocalDateUtil.diferencaEmDiasUteis(dataFim, LocalDate.now());
				else
					return 0L;
			} else
				return 0L;
		} else
			return 0L;
	}
	
	public Date getDataUltimoEnvioEmail() {
		if (listaHistoricoEnvioEmail != null && !listaHistoricoEnvioEmail.isEmpty()) {
			
			return listaHistoricoEnvioEmail.get(0).getDataEnvio().getTime();
		}
		
		return null;
	}
	
	public boolean isViagemEmAberto() {
		return this.status.equals(StatusViagem.ABERTA) || this.status.equals(StatusViagem.REABERTA);
	}
	
	public boolean isRelatorioEntregue() {
		return this.status.equals(StatusViagem.ENTREGUE);
	}
	
	public boolean isViagemReprovada() {
		return this.status.equals(StatusViagem.REPROVADA);
	}
	
	public boolean isViagemFinalizada() {
		return this.isRelatorioEntregue() || this.isViagemReprovada();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProtocolo() {
		return protocolo;
	}

	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public Servidor getServidor() {
		return servidor;
	}

	public Date getEntregueEm() {
		return entregueEm;
	}

	public void setEntregueEm(Date entregueEm) {
		this.entregueEm = entregueEm;
	}

	public String getNrBoletimUrs() {
		return nrBoletimUrs;
	}

	public void setNrBoletimUrs(String nrBoletimUrs) {
		this.nrBoletimUrs = nrBoletimUrs;
	}

	public Unidade getUnidadeOrigem() {
		return unidadeOrigem;
	}

	public void setUnidadeOrigem(Unidade unidadeOrigem) {
		if (unidadeOrigem != null)
			this.idUnidadeOrigem = unidadeOrigem.getId().toString();
		this.unidadeOrigem = unidadeOrigem;
	}

	public Unidade getUnidadeSolicitante() {
		return unidadeSolicitante;
	}

	public void setUnidadeSolicitante(Unidade unidadeSolicitante) {
		if (unidadeSolicitante != null)
			this.idUnidadeSolicitante = unidadeSolicitante.getId().toString();
		this.unidadeSolicitante = unidadeSolicitante;
	}

	public Unidade getUnidadeAutorizada() {
		return unidadeAutorizada;
	}

	public void setUnidadeAutorizada(Unidade unidadeAutorizada) {
		if (unidadeAutorizada != null)
			this.idUnidadeAutorizada = unidadeAutorizada.getId().toString();
		this.unidadeAutorizada = unidadeAutorizada;
	}

	public Date getDtInicio() {
		return dtInicio;
	}

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}

	public Date getDtFim() {
		return dtFim;
	}

	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getIdUnidadeOrigem() {
		return idUnidadeOrigem;
	}

	public void setIdUnidadeOrigem(String idUnidadeOrigem) {
		this.idUnidadeOrigem = idUnidadeOrigem;
	}

	public String getIdUnidadeSolicitante() {
		return idUnidadeSolicitante;
	}

	public void setIdUnidadeSolicitante(String idUnidadeSolicitante) {
		this.idUnidadeSolicitante = idUnidadeSolicitante;
	}

	public String getIdUnidadeAutorizada() {
		return idUnidadeAutorizada;
	}

	public void setIdUnidadeAutorizada(String idUnidadeAutorizada) {
		this.idUnidadeAutorizada = idUnidadeAutorizada;
	}

	public List<HistoricoEnvioEmail> getListaHistoricoEnvioEmail() {
		return listaHistoricoEnvioEmail;
	}

	public void setListaHistoricoEnvioEmail(List<HistoricoEnvioEmail> listaHistoricoEnvioEmail) {
		this.listaHistoricoEnvioEmail = listaHistoricoEnvioEmail;
	}

	public TipoEntregaRelatorioViagemFisico getTipoEntregaRelatorioViagemFisico() {
		return tipoEntregaRelatorioViagemFisico;
	}

	public void setTipoEntregaRelatorioViagemFisico(TipoEntregaRelatorioViagemFisico tipoEntregaRelatorioViagemFisico) {
		this.tipoEntregaRelatorioViagemFisico = tipoEntregaRelatorioViagemFisico;
	}

	public String getMotivoReprovacao() {
		return motivoReprovacao;
	}

	public void setMotivoReprovacao(String motivoReprovacao) {
		this.motivoReprovacao = motivoReprovacao;
	}

	public String getMotivoReabertura() {
		return motivoReabertura;
	}

	public void setMotivoReabertura(String motivoReabertura) {
		this.motivoReabertura = motivoReabertura;
	}

	public Date getDataReprovacao() {
		return dataReprovacao;
	}

	public void setDataReprovacao(Date dataReprovacao) {
		this.dataReprovacao = dataReprovacao;
	}

	public StatusViagem getStatus() {
		return status;
	}

	public void setStatus(StatusViagem status) {
		this.status = status;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public List<HistoricoViagem> getListaHistoricoViagem() {
		return listaHistoricoViagem;
	}

	public void setListaHistoricoViagem(List<HistoricoViagem> listaHistoricoViagem) {
		this.listaHistoricoViagem = listaHistoricoViagem;
	}

	public String getMotivoCancelamento() {
		return motivoCancelamento;
	}

	public void setMotivoCancelamento(String motivoCancelamento) {
		this.motivoCancelamento = motivoCancelamento;
	}

	public RecursosDiaria getRecursoDiaria() {
		return recursoDiaria;
	}

	public void setRecursoDiaria(RecursosDiaria recursoDiaria) {
		this.recursoDiaria = recursoDiaria;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrestacaoDeContas other = (PrestacaoDeContas) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
