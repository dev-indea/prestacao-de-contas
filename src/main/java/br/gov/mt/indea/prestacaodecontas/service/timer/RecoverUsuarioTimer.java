package br.gov.mt.indea.prestacaodecontas.service.timer;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timer;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.RecoverUsuario;
import br.gov.mt.indea.prestacaodecontas.service.RecoverUsuarioService;

@Singleton
@Startup
@LocalBean
public class RecoverUsuarioTimer {

	private static final Logger log = LoggerFactory.getLogger(RecoverUsuarioTimer.class);

	@Inject
	private RecoverUsuarioService recoverUsuarioService;

	@Schedule(minute = "0", second = "0")
	public void execute(Timer timer) {
        log.info("Iniciando timer"); 

		List<RecoverUsuario> lista = recoverUsuarioService.findAll();

		if (lista != null) {
			Instant now = Instant.now();
			Instant before = null;
			Duration duration = null;

			for (RecoverUsuario recoverUsuario : lista) {
				before = recoverUsuario.getData().toInstant();

				duration = Duration.between(before, now);

				if (duration.toHours() > 24) {
					recoverUsuarioService.delete(recoverUsuario);
				}
			}
		}
		
		log.info("Finalizando timer");
	}

}