package br.gov.mt.indea.prestacaodecontas.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.prestacaodecontas.entity.dbindea.Servidor;
import br.gov.mt.indea.prestacaodecontas.entity.dto.PrestacaoContasDTO;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.HistoricoViagem;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.PrestacaoDeContas;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.StatusViagem;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.TipoEntregaRelatorioViagemFisico;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.TipoEventoViagem;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.TipoServidor;
import br.gov.mt.indea.prestacaodecontas.service.HistoricoViagemService;
import br.gov.mt.indea.prestacaodecontas.service.LazyObjectDataModel;
import br.gov.mt.indea.prestacaodecontas.service.PrestacaoDeContasService;
import br.gov.mt.indea.prestacaodecontas.service.ServidorService;
import br.gov.mt.indea.prestacaodecontas.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("prestacaoDeContasManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarPrestacao", pattern = "/prestacaodecontas/pesquisar", viewId = "/pages/prestacaodecontas/lista.jsf"),
		@URLMapping(id = "incluirPrestacao", pattern = "/prestacaodecontas/incluir", viewId = "/pages/prestacaodecontas/novo.jsf"),
		@URLMapping(id = "alteraPrestacao", pattern = "/prestacaodecontas/alterar/#{prestacaoDeContasManagedBean.id}", viewId = "/pages/prestacaodecontas/novo.jsf"),
		@URLMapping(id = "alteraPrestacaoImportada", pattern = "/prestacaodecontas/alterarImportada/#{prestacaoDeContasManagedBean.id}", viewId = "/pages/prestacaodecontas/importada.jsf"),
		@URLMapping(id = "visualizarPrestacao", pattern = "/prestacaodecontas/visualizar/#{prestacaoDeContasManagedBean.id}", viewId = "/pages/prestacaodecontas/novo.jsf"),
		@URLMapping(id = "selfPrestacao", pattern = "/prestacaodecontas/incluir", viewId = "/pages/prestacaodecontas/novo.jsf")})
public class PrestacaoDeContasManagedBean implements Serializable {

	private static final long serialVersionUID = 3240358284250039945L;
	
	boolean editando = false;
	
	boolean editandoImportada = false;
	
	boolean visualizando = false;
	
	@Inject
	private PrestacaoDeContas prestacaoContas;
	
	@Inject
	private PrestacaoContasDTO prestacaoContasDTO;
	
	private Long id;

	private List<Servidor> listaServidores;
	
	private List<Servidor> listaServidoresCompleta;
	
	private LazyObjectDataModel<PrestacaoDeContas> listaPrestacaoContas;
	
	@Inject
	private ServidorService servidorService;
	
	@Inject
	private PrestacaoDeContasService prestacaoService;
	
	@Inject
	private HistoricoViagemService historicoViagemService;
	
	@Inject
	private ListsManagedBean listsManagedBean;
	
	@PostConstruct
	public void init(){
		this.listaServidores = servidorService.findAllOrderByNome();
	}
	
	@URLAction(mappingId = "incluirPrestacao", onPostback = false)
	public void novo(){
		this.limpar();
		
		this.editando = false;
		this.visualizando = false;
	}
	
	@URLAction(mappingId = "pesquisarPrestacao", onPostback = false)
	public void pesquisar(){
		limpar();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String protocolo = (String) request.getSession().getAttribute("protocolo");
		
		if (protocolo != null){
			this.prestacaoContasDTO = new PrestacaoContasDTO();
			this.prestacaoContasDTO.setProtocolo(protocolo);
			this.buscarPrestacao();
			
			request.getSession().setAttribute("protocolo", null);
		}
	}
	
	@URLAction(mappingId = "alteraPrestacao", onPostback = false)
	public void editar(){
		editando = true;
		editandoImportada = false;
		visualizando = false;
		
		this.prestacaoContas = prestacaoService.findById(this.getId());
		
		if (this.prestacaoContas.getServidor().getTipoServidor() != null)
			if (this.prestacaoContas.getServidor().getTipoServidor().equals(TipoServidor.INDEA))
				this.prestacaoContas.getServidor().setServidorIndeaWeb(servidorService.findByIdFetchAll(this.prestacaoContas.getServidor().getId()));
		
		if (this.prestacaoContas.getIdUnidadeAutorizada() != null)
			this.prestacaoContas.setUnidadeAutorizada(listsManagedBean.getUnidadeById(Long.valueOf(this.prestacaoContas.getIdUnidadeAutorizada())));
		if (this.prestacaoContas.getIdUnidadeOrigem() != null)
			this.prestacaoContas.setUnidadeOrigem(listsManagedBean.getUnidadeById(Long.valueOf(this.prestacaoContas.getIdUnidadeOrigem())));
		if (this.prestacaoContas.getIdUnidadeSolicitante() != null)
			this.prestacaoContas.setUnidadeSolicitante(listsManagedBean.getUnidadeById(Long.valueOf(this.prestacaoContas.getIdUnidadeSolicitante())));
	}
	
	@URLAction(mappingId = "alteraPrestacaoImportada", onPostback = false)
	public void editarImportada(){
		editando = false;
		editandoImportada = true;
		visualizando = false;
		
		this.prestacaoContas = prestacaoService.findById(this.getId());
		
		if (this.prestacaoContas.getServidor().getTipoServidor() != null)
			if (this.prestacaoContas.getServidor().getTipoServidor().equals(TipoServidor.INDEA))
				this.prestacaoContas.getServidor().setServidorIndeaWeb(servidorService.findByIdFetchAll(this.prestacaoContas.getServidor().getId()));
		
		if (this.prestacaoContas.getIdUnidadeAutorizada() != null)
			this.prestacaoContas.setUnidadeAutorizada(listsManagedBean.getUnidadeById(Long.valueOf(this.prestacaoContas.getIdUnidadeAutorizada())));
		if (this.prestacaoContas.getIdUnidadeOrigem() != null)
			this.prestacaoContas.setUnidadeOrigem(listsManagedBean.getUnidadeById(Long.valueOf(this.prestacaoContas.getIdUnidadeOrigem())));
		if (this.prestacaoContas.getIdUnidadeSolicitante() != null)
			this.prestacaoContas.setUnidadeSolicitante(listsManagedBean.getUnidadeById(Long.valueOf(this.prestacaoContas.getIdUnidadeSolicitante())));
	}
	
	@URLAction(mappingId = "visualizarPrestacao", onPostback = false)
	public void visualizar(){
		editando = false;
		editandoImportada = false;
		visualizando = true;
		
		this.prestacaoContas = prestacaoService.findById(this.getId());
		
		if (this.prestacaoContas.getServidor().isTipoServidorINDEA())
			this.prestacaoContas.getServidor().setServidorIndeaWeb(
					servidorService.findByIdFetchAll(this.prestacaoContas.getServidor().getId())
					);
		
		if (this.prestacaoContas.getIdUnidadeAutorizada() != null)
			this.prestacaoContas.setUnidadeAutorizada(listsManagedBean.getUnidadeById(Long.valueOf(this.prestacaoContas.getIdUnidadeAutorizada())));
		if (this.prestacaoContas.getIdUnidadeOrigem() != null)
			this.prestacaoContas.setUnidadeOrigem(listsManagedBean.getUnidadeById(Long.valueOf(this.prestacaoContas.getIdUnidadeOrigem())));
		if (this.prestacaoContas.getIdUnidadeSolicitante() != null)
			this.prestacaoContas.setUnidadeSolicitante(listsManagedBean.getUnidadeById(Long.valueOf(this.prestacaoContas.getIdUnidadeSolicitante())));
	}
	
	public void limpar() {
		this.prestacaoContas = new PrestacaoDeContas();
		this.listaPrestacaoContas = null;
		
		editando = false;
		visualizando = false;
	}
	
	public void reset() {
		this.prestacaoContasDTO.setEntregueEm(null);
		this.prestacaoContasDTO.setEntregueEmOut(null);
		this.prestacaoContasDTO.setDtFim(null);
		this.prestacaoContasDTO.setDtFimOut(null);
		this.prestacaoContasDTO.setDtInicio(null);
		this.prestacaoContasDTO.setDtInicioOut(null);
		this.prestacaoContasDTO.setRecursoDiaria(null);
		this.prestacaoContasDTO.setAtividade(null);
		RequestContext.getCurrentInstance().reset("formListaPrestacoes");
	}
	
	public List<Servidor> completaServidor(String query){
		
		listaServidoresCompleta = servidorService.findByNome(query);
		return listaServidoresCompleta;
	}
	
	public String adicionar() throws IOException{		

		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		boolean isPrestacaoOk = true;

		if(prestacaoService.checkExistOSProtocolo(prestacaoContas)) {			
			context.addMessage("cadastro:fieldProtocolo:protocolo", new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "J� existe uma viagem com este Protocolo e OS"));
			context.addMessage("cadastro:fieldOS:OS", new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "J� existe uma viagem com este Protocolo e OS"));
			
			isPrestacaoOk = false;
		}
		
		if(this.isDataFimBeforeDataInicio()) {
			context.addMessage("cadastro:fieldDtFim:formDtFim", new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "O Fim da viagem n�o pode ser anterior ao In�cio"));
			isPrestacaoOk = false;
		}
		
		if (!isPrestacaoOk)
			return "";
		
		if(this.prestacaoContas.getDataCadastro()== null){
			this.prestacaoContas.setDataCadastro(Calendar.getInstance().getTime());
		}
		this.prestacaoContas.setIdUnidadeOrigem(this.prestacaoContas.getUnidadeOrigem().getId().toString());
		this.prestacaoContas.setIdUnidadeAutorizada(this.prestacaoContas.getUnidadeAutorizada().getId().toString());
		this.prestacaoContas.setIdUnidadeSolicitante(this.prestacaoContas.getUnidadeSolicitante().getId().toString());

		if(this.prestacaoContas.getId() == null) {
			HistoricoViagem historicoViagem = new HistoricoViagem();
			historicoViagem.setTipoEventoViagem(TipoEventoViagem.ABERTURA);
			historicoViagem.setPrestacaoDeContas(this.prestacaoContas);
			Calendar data = Calendar.getInstance();
			data.setTime(this.prestacaoContas.getDtInicio());
			historicoViagem.setDataEvento(data);
			
			prestacaoContas.getListaHistoricoViagem().add(historicoViagem);
		} 
		
		this.prestacaoService.saveOrUpdate(this.prestacaoContas);

		String msg = " adicionado!";
		if(this.prestacaoContas.getId() != null) {
			msg =  " atualizado!";
		}
		
		FacesMessageUtil.addInfoContextFacesMessage("Viagem: \nProtocolo:" + prestacaoContas.getProtocolo() +" e OS:"+ prestacaoContas.getOs() + msg, "");

		request.getSession().setAttribute("protocolo", prestacaoContas.getProtocolo());

		
		limpar();

		return "pretty:pesquisarPrestacao";
	}
	
	public void selecionarTipoServidor() {
		
		if (this.prestacaoContas != null) {
			if (this.prestacaoContas.getServidor() != null) {
				TipoServidor tipoServidor = prestacaoContas.getServidor().getTipoServidor();
				this.prestacaoContas.setServidor(new br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.Servidor());
				this.prestacaoContas.getServidor().setTipoServidor(tipoServidor);
			}
		}
	}
	
	public void abrirTelaHistoricoViagem(PrestacaoDeContas prestacaoDeContas) {
		this.setPrestacaoContas(prestacaoDeContas);
		this.prestacaoContas.setListaHistoricoViagem(this.historicoViagemService.findAll(prestacaoDeContas));
	}
	
	public void remover(PrestacaoDeContas prestacaoDeContas) throws IOException{
		this.prestacaoService.delete(prestacaoDeContas);
		FacesMessageUtil.addInfoContextFacesMessage("Viagem exclu�da com sucesso", "");
	}
	
	
	public void buscarPrestacao() {
		this.limpar();
		this.listaPrestacaoContas = new LazyObjectDataModel<PrestacaoDeContas>(this.prestacaoService, this.prestacaoContasDTO);
	}
	
	public boolean isTipoEntregaRelatorioViagemFisicoIgualAMalote() {   
		if (this.prestacaoContas == null)
			return false;
		if (this.prestacaoContas.getTipoEntregaRelatorioViagemFisico() == null)
			return false;
		if (this.prestacaoContas.getTipoEntregaRelatorioViagemFisico().equals(TipoEntregaRelatorioViagemFisico.MALOTE))
			return true;
		
		return false;
	}
	
	private boolean isDataFimBeforeDataInicio() {
		if (this.prestacaoContas != null)
			if (this.prestacaoContas.getDtFim().before(prestacaoContas.getDtInicio())) {
				return true;
			}
		return false;
	}

	public void entregarRelatorioFisico() {
		this.prestacaoContas.setStatus(StatusViagem.ENTREGUE);
		this.prestacaoService.aprovar(this.prestacaoContas);
		
		FacesMessageUtil.addInfoContextFacesMessage("Relat�rio f�sico da viagem n�" + prestacaoContas.getOs() + " entregue", "");
	}
	
	public void reprovar() {
		this.prestacaoContas.setStatus(StatusViagem.REPROVADA);
		this.prestacaoContas.setDataReprovacao(Calendar.getInstance().getTime());
		this.prestacaoService.reprovar(this.prestacaoContas);
		
		FacesMessageUtil.addInfoContextFacesMessage("Relat�rio f�sico da viagem n�" + prestacaoContas.getOs() + " reprovado", "");
	}
	
	public void cancelar() {
		this.prestacaoContas.setStatus(StatusViagem.NAO_REALIZADA);
		this.prestacaoContas.setDataReprovacao(Calendar.getInstance().getTime());
		this.prestacaoService.cancelar(this.prestacaoContas);
		
		FacesMessageUtil.addInfoContextFacesMessage("Relat�rio f�sico da viagem n�" + prestacaoContas.getOs() + " reprovado", "");
	}
	
	public void reabrir() {
		this.prestacaoContas.setStatus(StatusViagem.REABERTA);
		this.prestacaoContas.setDataReprovacao(null);
		this.prestacaoContas.setEntregueEm(null);
		this.prestacaoContas.setMotivoReprovacao(null);
		this.prestacaoService.reabrir(this.prestacaoContas);
		
		FacesMessageUtil.addInfoContextFacesMessage("Viagem n�" + prestacaoContas.getOs() + " reaberta", "");
	}
	
	public List<Servidor> getListaServidores() {
		return listaServidores;
	}

	public void setListaServidores(List<Servidor> listaServidores) {
		this.listaServidores = listaServidores;
	}

	public PrestacaoDeContas getPrestacaoContas() {
		return prestacaoContas;
	}


	public void setPrestacaoContas(PrestacaoDeContas prestacaoContas) {
		this.prestacaoContas = prestacaoContas;
	}


	public PrestacaoContasDTO getPrestacaoContasDTO() {
		return prestacaoContasDTO;
	}


	public void setPrestacaoContasDTO(PrestacaoContasDTO prestacaoContasDTO) {
		this.prestacaoContasDTO = prestacaoContasDTO;
	}


	public LazyObjectDataModel<PrestacaoDeContas> getListaPrestacaoContas() {
		return listaPrestacaoContas;
	}


	public void setListaPrestacaoContas(LazyObjectDataModel<PrestacaoDeContas> listaPrestacaoContas) {
		this.listaPrestacaoContas = listaPrestacaoContas;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public List<Servidor> getListaServidoresCompleta() {
		return listaServidoresCompleta;
	}

	public void setListaServidoresCompleta(List<Servidor> listaServidoresCompleta) {
		this.listaServidoresCompleta = listaServidoresCompleta;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}

	public boolean isVisualizando() {
		return visualizando;
	}

	public void setVisualizando(boolean visualizando) {
		this.visualizando = visualizando;
	}

	public boolean isEditandoImportada() {
		return editandoImportada;
	}

	public void setEditandoImportada(boolean editandoImportada) {
		this.editandoImportada = editandoImportada;
	}

}
