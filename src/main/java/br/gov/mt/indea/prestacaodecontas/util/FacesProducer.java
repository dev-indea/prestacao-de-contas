package br.gov.mt.indea.prestacaodecontas.util;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class FacesProducer { 
	 
    @Produces 
    @RequestScoped 
    public FacesContext getFacesContext() { 
    	FacesContext context = FacesContext.getCurrentInstance(); 
    	
    	if (context == null)
			context = CDIServiceLocator.getBean(FacesContext.class);

    	return context;
    } 
 
    @Produces 
    @RequestScoped 
    public ExternalContext getExternalContext() {
    	FacesContext context = getFacesContext();
    	
    	if (context == null)
			context = CDIServiceLocator.getBean(FacesContext.class);
		
        return context.getExternalContext(); 
    } 
 
    @Produces 
    @RequestScoped 
    public HttpServletResponse getHttpServletResponse() { 
        return ((HttpServletResponse) getExternalContext().getResponse()); 
    }
    
    @Produces 
    @RequestScoped 
    public HttpSession getHttpSession() { 
        return (HttpSession) getExternalContext().getSession(false); 
    }
}
