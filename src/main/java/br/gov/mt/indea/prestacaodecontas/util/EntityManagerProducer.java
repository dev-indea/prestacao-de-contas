package br.gov.mt.indea.prestacaodecontas.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.hibernate.Session;

import br.gov.mt.indea.prestacaodecontas.annotation.IndeaWeb;

@ApplicationScoped
public class EntityManagerProducer {
	
    @PersistenceUnit(name="prestacao-de-contas", unitName="prestacao-de-contas")
    private EntityManagerFactory emfPrestacaoDeContas;
    
    @PersistenceUnit(name="indeaweb", unitName="indeaweb")
    private EntityManagerFactory emfIndeaWeb;

    @Produces
    @Dependent
	@Default
	private EntityManager getEntityManager() {
		return emfPrestacaoDeContas.createEntityManager();
	}
    
    @Produces
    @Dependent
	@IndeaWeb
	private EntityManager getEntityManagerIndeaWeb() {
		return emfIndeaWeb.createEntityManager();
	}
	
	public void closeEntityManager(@Disposes @Any EntityManager em){
		try {
			em.close();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
