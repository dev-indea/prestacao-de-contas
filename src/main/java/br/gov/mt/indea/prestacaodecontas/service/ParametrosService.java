package br.gov.mt.indea.prestacaodecontas.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.Parametros;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ParametrosService extends PaginableService<Parametros, Long> {

	private static final Logger log = LoggerFactory.getLogger(Parametros.class);
	
	protected ParametrosService() {
		super(Parametros.class);
	}

	public Parametros findById(Long id){
		Parametros parametros;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select parametros ")
		   .append("  from Parametros parametros ")
		   .append(" where parametros.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		parametros = (Parametros) query.uniqueResult();
		
		return parametros;
	}
	
	@Override
	public void saveOrUpdate(Parametros parametros) {
		parametros = (Parametros) this.getSession().merge(parametros);
		
		super.saveOrUpdate(parametros);
		
		log.info("Salvando Parametros {}");
	}
	
	@Override
	public void delete(Parametros parametros) {
		super.delete(parametros);
		
		log.info("Removendo Parametros {}");
	}
	
	@Override
	public void validar(Parametros model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarPersist(Parametros model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarMerge(Parametros model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarDelete(Parametros model) {
		// TODO Auto-generated method stub
		
	}

}
