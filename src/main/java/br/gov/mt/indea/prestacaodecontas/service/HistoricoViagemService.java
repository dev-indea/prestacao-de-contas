package br.gov.mt.indea.prestacaodecontas.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.HistoricoViagem;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.PrestacaoDeContas;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class HistoricoViagemService extends PaginableService<HistoricoViagem, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(HistoricoViagemService.class);

	protected HistoricoViagemService() {
		super(HistoricoViagem.class);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void saveOrUpdate(HistoricoViagem historicoViagem) {
		historicoViagem = (HistoricoViagem) this.getSession().merge(historicoViagem);
		
		super.saveOrUpdate(historicoViagem);
		
		log.info("Salvando HistoricoViagem {}", historicoViagem.getId());
	}
	
	@SuppressWarnings("unchecked")
	public List<HistoricoViagem> findAll(PrestacaoDeContas prestacaoDeContas){
		List<HistoricoViagem> lista = null;
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("select historicoViagem ")
		   .append("  from " + this.getType().getSimpleName() + " as historicoViagem ")
		   .append(" where historicoViagem.prestacaoDeContas = :prestacaoDeContas ")
		   .append(" order by historicoViagem.dataEvento DESC");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("prestacaoDeContas", prestacaoDeContas);
		lista = query.list();
		
		return lista;
	}
	
	@Override
	public void delete(HistoricoViagem HistoricoViagem) {
		super.delete(HistoricoViagem);
		
		log.info("Removendo HistoricoViagem {}", HistoricoViagem.getId());
	}
	
	@Override
	public void validar(HistoricoViagem HistoricoViagem) {

	}

	@Override
	public void validarPersist(HistoricoViagem HistoricoViagem) {

	}

	@Override
	public void validarMerge(HistoricoViagem HistoricoViagem) {

	}

	@Override
	public void validarDelete(HistoricoViagem HistoricoViagem) {

	}

}
