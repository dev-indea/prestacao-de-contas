package br.gov.mt.indea.prestacaodecontas.entity.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.inject.Inject;

import br.gov.mt.indea.prestacaodecontas.entity.dbindea.Servidor;
import br.gov.mt.indea.prestacaodecontas.entity.dbindea.Unidade;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.Atividade;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.RecursosDiaria;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.StatusViagem;
import br.gov.mt.indea.prestacaodecontas.service.ServidorService;
import br.gov.mt.indea.prestacaodecontas.service.UnidadeService;

public class PrestacaoContasDTO  implements AbstractDTO, Serializable{

	private static final long serialVersionUID = 5465505183619430177L;

	private String protocolo;
	private String os;
	private Servidor servidor;
	private String idServidor;
	private Date entregueEm;
	private Date entregueEmOut;
	private String nrBoletimUrs;
	private String cargo;
	private String origem;
	private String solicitante;
	private String autorizada;
	private Date dtInicio;
	private Date dtInicioOut;
	private Date dtFim;
	private Date dtFimOut;
	private Date dtReprovacao;
	private Date dtReprovacaoOut;
	private BigDecimal valor;
	private StatusViagem status;
	private RecursosDiaria recursoDiaria;
	private Atividade atividade;
	
	private Unidade unidadeOrigemEntity;
	private Unidade unidadeSolicitanteEntity;
	private Unidade unidadeAutorizadaEntity;
	
	@Inject
	ServidorService servidorService;
	
	@Inject
	UnidadeService unidadeService;
	
	@Override
	public boolean isNull() {
		// TODO Auto-generated method stub
		return false;
	}


	public String getProtocolo() {
		return protocolo;
	}


	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}


	public String getOs() {
		return os;
	}


	public void setOs(String os) {
		this.os = os;
	}


	public Servidor getServidor() {
		return servidor;
	}


	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}


	public Date getEntregueEm() {
		return entregueEm;
	}


	public void setEntregueEm(Date dataEntrega) {
		this.entregueEm = dataEntrega;
	}


	public String getNrBoletimUrs() {
		return nrBoletimUrs;
	}


	public void setNrBoletimUrs(String nrBoletimUrs) {
		this.nrBoletimUrs = nrBoletimUrs;
	}


	public Date getDtInicio() {
		return dtInicio;
	}


	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}


	public Date getDtFim() {
		return dtFim;
	}


	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}


	public BigDecimal getValor() {
		return valor;
	}


	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}


	public String getCargo() {
		return cargo;
	}


	public void setCargo(String cargo) {
		this.cargo = cargo;
	}


	public String getOrigem() {
		return origem;
	}


	public void setOrigem(String origem) {
		this.origem = origem;
	}


	public String getSolicitante() {
		return solicitante;
	}


	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}


	public String getAutorizada() {
		return autorizada;
	}


	public void setAutorizada(String autorizada) {
		this.autorizada = autorizada;
	}
	
	public Unidade getUnidadeOrigemEntity() {
		return this.unidadeOrigemEntity;
	}

	public void setUnidadeOrigemEntity(Unidade unidade) {
		this.unidadeOrigemEntity = unidade;
	}
	
	public Unidade getUnidadeSolicitanteEntity() {
		return this.unidadeSolicitanteEntity;
	}

	public void setUnidadeSolicitanteEntity(Unidade unidade) {
		this.unidadeSolicitanteEntity = unidade;
	}
	
	public Unidade getUnidadeAutorizadaEntity() {
		return this.unidadeAutorizadaEntity;
	}

	public void setUnidadeAutorizadaEntity(Unidade unidade) {
		this.unidadeAutorizadaEntity = unidade;
	}


	public String getIdServidor() {
		return idServidor;
	}


	public void setIdServidor(String idServidor) {
		this.idServidor = idServidor;
	}


	public Date getDtInicioOut() {
		return dtInicioOut;
	}


	public void setDtInicioOut(Date dtInicioOut) {
		this.dtInicioOut = dtInicioOut;
	}


	public Date getDtFimOut() {
		return dtFimOut;
	}


	public void setDtFimOut(Date dtFimOut) {
		this.dtFimOut = dtFimOut;
	}


	public Date getEntregueEmOut() {
		return entregueEmOut;
	}


	public void setEntregueEmOut(Date entregueEmOut) {
		this.entregueEmOut = entregueEmOut;
	}


	public StatusViagem getStatus() {
		return status;
	}


	public void setStatus(StatusViagem status) {
		this.status = status;
	}


	public Date getDtReprovacao() {
		return dtReprovacao;
	}


	public void setDtReprovacao(Date dtReprovacao) {
		this.dtReprovacao = dtReprovacao;
	}


	public Date getDtReprovacaoOut() {
		return dtReprovacaoOut;
	}


	public void setDtReprovacaoOut(Date dtReprovacaoOut) {
		this.dtReprovacaoOut = dtReprovacaoOut;
	}


	public RecursosDiaria getRecursoDiaria() {
		return recursoDiaria;
	}


	public void setRecursoDiaria(RecursosDiaria recursoDiaria) {
		this.recursoDiaria = recursoDiaria;
	}


	public Atividade getAtividade() {
		return atividade;
	}


	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
}
