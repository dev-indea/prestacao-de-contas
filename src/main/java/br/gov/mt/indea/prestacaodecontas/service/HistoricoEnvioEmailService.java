package br.gov.mt.indea.prestacaodecontas.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.HistoricoEnvioEmail;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class HistoricoEnvioEmailService extends PaginableService<HistoricoEnvioEmail, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(HistoricoEnvioEmailService.class);

	protected HistoricoEnvioEmailService() {
		super(HistoricoEnvioEmail.class);
	}
	
	public HistoricoEnvioEmail findById(String id){
		HistoricoEnvioEmail HistoricoEnvioEmail;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select historicoEnvioEmail ")
		   .append("  from HistoricoEnvioEmail historicoEnvioEmail ")
		   .append(" where historicoEnvioEmail.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setString("id", id);
		HistoricoEnvioEmail = (HistoricoEnvioEmail) query.uniqueResult();
		
		if (HistoricoEnvioEmail == null){
			return null;
		}
		
		return HistoricoEnvioEmail;
	}
	
	@Override
	public void saveOrUpdate(HistoricoEnvioEmail historicoEnvioEmail) {
		historicoEnvioEmail = (HistoricoEnvioEmail) this.getSession().merge(historicoEnvioEmail);
		
		super.saveOrUpdate(historicoEnvioEmail);
		
		log.info("Salvando HistoricoEnvioEmail {}", historicoEnvioEmail.getId());
	}
	
	@Override
	public void delete(HistoricoEnvioEmail HistoricoEnvioEmail) {
		super.delete(HistoricoEnvioEmail);
		
		log.info("Removendo HistoricoEnvioEmail {}", HistoricoEnvioEmail.getId());
	}
	
	@Override
	public void validar(HistoricoEnvioEmail HistoricoEnvioEmail) {

	}

	@Override
	public void validarPersist(HistoricoEnvioEmail HistoricoEnvioEmail) {

	}

	@Override
	public void validarMerge(HistoricoEnvioEmail HistoricoEnvioEmail) {

	}

	@Override
	public void validarDelete(HistoricoEnvioEmail HistoricoEnvioEmail) {

	}

}
