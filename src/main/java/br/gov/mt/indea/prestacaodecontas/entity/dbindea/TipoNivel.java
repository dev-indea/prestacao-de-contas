package br.gov.mt.indea.prestacaodecontas.entity.dbindea;

public enum TipoNivel {

    NAO_ESPECIFICADO("N�oo Especificado"),
    FUNDAMENTAL("Fundamental"),
    MEDIO("M�dio"),
    SUPERIO("Superior");

    String descricao;

    TipoNivel(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
