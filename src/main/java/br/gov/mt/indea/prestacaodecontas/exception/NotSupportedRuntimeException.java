package br.gov.mt.indea.prestacaodecontas.exception;

public class NotSupportedRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -6376888203540635430L;

	private String mensagem;
	
	private Throwable throwable;
	
	public NotSupportedRuntimeException(){
		super();
	}
	
	public NotSupportedRuntimeException(String mensagem){
		super(mensagem);
		this.mensagem = mensagem;
	}
	
	public NotSupportedRuntimeException(Throwable throwable){
		super(throwable);
		this.throwable = throwable;
	}
	
	public NotSupportedRuntimeException(String mensagem, Throwable throwable){
		super(mensagem, throwable);
		this.mensagem = mensagem;
		this.throwable = throwable;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

}
