package br.gov.mt.indea.prestacaodecontas.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.Usuario;
import br.gov.mt.indea.prestacaodecontas.entity.seguranca.Permissao;
import br.gov.mt.indea.prestacaodecontas.service.UsuarioService;
import br.gov.mt.indea.prestacaodecontas.util.CDIServiceLocator;

public class SIZUserDetailsService implements UserDetailsService{

	@Override
	public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
		UsuarioService usuarioService = CDIServiceLocator.getBean(UsuarioService.class);
		Usuario usuario = usuarioService.findById(arg0);
		
		User user = null;
		
		if (usuario != null){
			user = new UserSecurity(usuario, getRoles(usuario));
		} else
			throw new UsernameNotFoundException("Usu�rio ou senha inv�lidos");
		
		
		return user;
	}

	private Collection<? extends GrantedAuthority> getRoles(Usuario usuario) {
		List<GrantedAuthority> lista = new ArrayList<GrantedAuthority>();
		
		if (usuario.getListaPermissao() != null)
			for (Permissao permissao : usuario.getListaPermissao()) {
				lista.add(new SimpleGrantedAuthority(permissao.getNome()));
			}
		
		return lista;
	}

	

}
