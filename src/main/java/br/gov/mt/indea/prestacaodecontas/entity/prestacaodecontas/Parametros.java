package br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

@Audited
@Entity
public class Parametros extends BaseEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1887056915759535995L;

	@Id
	@SequenceGenerator(name="parametros_seq", sequenceName="parametros_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="parametros_seq")
	private Long id;
	
	@Column(name = "prazo_relatorio", nullable = false)
	private Integer prazoRelatorio;
	

	@Column(name = "dias_cobranca", nullable = false)
	private Integer diasCobranca;

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return this.id;
	}
	
	public Integer getPrazoRelatorio() {
		return prazoRelatorio;
	}

	public void setPrazoRelatorio(Integer prazoRelatorio) {
		this.prazoRelatorio = prazoRelatorio;
	}

	public Integer getDiasCobranca() {
		return diasCobranca;
	}

	public void setDiasCobranca(Integer diasCobranca) {
		this.diasCobranca = diasCobranca;
	}
}
