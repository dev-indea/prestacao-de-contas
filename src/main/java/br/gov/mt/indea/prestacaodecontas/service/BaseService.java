package br.gov.mt.indea.prestacaodecontas.service;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.prestacaodecontas.annotation.IndeaWeb;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.BaseEntity;
import br.gov.mt.indea.prestacaodecontas.util.ReflectionUtil;

@SuppressWarnings({"rawtypes", "unchecked", "hiding"})
public abstract class BaseService<T extends BaseEntity<PK>, PK extends Serializable> {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Inject
	protected EntityManager emSIZ;
	
	@Inject
	@IndeaWeb
	protected EntityManager emIndeaWeb;
	
	protected BaseService(Class<T> type){
		this.type = type; 
	}
	
	protected BaseService(Class<T> type, Order... orders) {
        this.type = type;
        this.orders = orders;
    }
	
    private final Class<T> type;
    
    private Order[] orders;
    
    public T find(Class<T> type, PK pk) {
        return (T) getSession().get(type, pk);
    }

    public List<T> findAll() {
        return findAll(orders);
    }

    public List<T> findAll(Order... orders) {

        Criteria criteria = getSession().createCriteria(type);
        
        if (orders != null) {
        	for (Order order : orders) {
        		criteria.addOrder(order);
        	}
        }

        return criteria.list();
    }
    
    protected Session getSession() {
    	return this.getEntityManager().unwrap(Session.class);
    }
    
    protected EntityManager getEntityManager(){
		Annotation[] declaredAnnotations = type.getAnnotations();
		for (Annotation annotation : declaredAnnotations) {
			if (annotation instanceof IndeaWeb)
				return this.emIndeaWeb;
		}
    	
    	return emSIZ;
    }

    /**
     * Conulta uma entidade no banco de dados utilizando a chave prim�ria do mesmo.
     * 
     * @param pk
     *            chave prim�ria.
     * @return objeto retornado do banco de dados.
     */
    public T findByIdFetchAll(PK pk) {

        LOGGER.debug("FindById: (#{}) {}", pk, type.getSimpleName());

        T entityLoaded = (T) getSession().get(type, pk);

        return entityLoaded;
    }

    public T loadByProperty(String propertyName, Object value) {

        Criteria criteria = getSession().createCriteria(type);

        criteria.add(Restrictions.eq(propertyName, value));

        List<?> results = criteria.list();
        T entity = null;
        if (!results.isEmpty()) {
            entity = (T) results.get(0);
        }

        return entity;
    }

    public T loadByProperty(String[] propertyName, Object[] value) {

        Criteria criteria = getSession().createCriteria(type);

        for (int i = 0; i < propertyName.length; i++) {
            criteria.add(Restrictions.eq(propertyName[i], value[i]));
        }

        List<?> results = criteria.list();
        T entity = null;
        if (!results.isEmpty()) {
            entity = (T) results.get(0);
        }

        return entity;
    }

    public void merge(T entity) {
        LOGGER.debug("Merge: (#{}) {}", entity.getId(), entity.getClass().getSimpleName());
        getSession().merge(entity);
    }

    public void persist(T entity) {
        LOGGER.debug("Persist: {}", entity.getClass().getSimpleName());
        getSession().persist(entity);
    }

    /**
     * Salva ou atualiza uma entidade no banco de dados.
     * 
     * @param entity
     */
    public void saveOrUpdate(T entity) {
        if (entity.getId() == null) {
            persist(entity);
        } else {
            merge(entity);
        }
    }

    /**
     * Salva ou atualiza uma lista de entidades no banco de dados.
     * 
     * @param entity
     */
    public void saveOrUpdate(Collection<T> objs) {
        for (T t : objs) {
            saveOrUpdate(t);
        }
    }

    /**
     * Salva ou atualiza uma lista de entidades no banco de dados.
     * 
     * @param entity
     */
    public void saveOrUpdate(T... objs) {
        for (T t : objs) {
            saveOrUpdate(t);
        }
    }

    /**
     * Atualiza a entidade passada por par�metro com as informa��es do banco de dados.
     * 
     * @param entity
     *            Entidade a ser aualizada.
     * @return Entidade com os dados do banco de dados.
     */
    public T refresh(T entity) {
        LOGGER.debug("Refresh: (#{}) {}", entity.getId(), entity.getClass().getSimpleName());
        getSession().refresh(entity);
        return entity;
    }

    public T evict(T entity) {
        LOGGER.debug("Evict: (#{}) {}", entity.getId(), entity.getClass().getSimpleName());
        getSession().evict(entity);
        return entity;
    }

    /**
     * Excluir um registro do baco de dados.
     * 
     * @param entity
     */
    public void delete(T entity) {
        LOGGER.debug("Remove: (#{}) {}", entity.getId(), entity.getClass().getSimpleName());

        Session session = getSession();
        Object entityToRemove = session.get(entity.getClass(), entity.getId());

        if (entityToRemove != null) {
        	session.delete(entityToRemove);
        }
    }

    /**
     * Excluir um grupo de registro do banco de dados utilizando a lista de chaves prim�ria.
     * 
     * @param entity
     */
    public void delete(PK... ids) {
        for (PK pk : ids) {
            T entityToRemove = this.findByIdFetchAll(pk);

            if (entityToRemove != null) {
                getSession().delete(entityToRemove);
            }
        }
    }

    public void delete(Collection<T> objs) {
        for (T t : objs) {
            this.delete(t);
        }
    }

    public void flush() {
        LOGGER.debug("Flushing statements...");
        getSession().flush();

    }

    public void clear() {
        LOGGER.debug("Clearing session...");
        getSession().clear();
    }

    /*
     * protected Query createNativeQuery(String sql) {
     * 
     * LOGGER.debug("Creating Query from SQL: {}", sql); return getEntityManager().createNativeQuery(sql);
     * 
     * }
     * 
     * protected Query createNativeQuery(String sql, Class<? extends BaseModel<?>> entityClass) {
     * 
     * LOGGER.debug("Creating Query from SQL: {}", sql); return getEntityManager().createNativeQuery(sql, entityClass);
     * 
     * }
     * 
     * protected Query createQuery(String jpql) {
     * 
     * LOGGER.debug("Creating Query from JPQL: {}", jpql); return getEntityManager().createQuery(jpql);
     * 
     * }
     * 
     * protected Query createQuery(String jpql, boolean cacheable) {
     * 
     * LOGGER.debug("Creating from JPQL: {} / Cacheable: {}", jpql, cacheable); return getEntityManager().createQuery(jpql).setHint(
     * "org.hibernate.cacheable", cacheable);
     * 
     * }
     * 
     * protected TypedQuery<T> createTypedQuery(String jpql) {
     * 
     * LOGGER.debug("Creating Typed Query from JPQL: {}", jpql); return getEntityManager().createQuery(jpql, getType());
     * 
     * }
     * 
     * protected TypedQuery<T> createTypedQuery(String jpql, boolean cacheable) {
     * 
     * LOGGER.debug("Creating from JPQL: {} / Cacheable: {}", jpql, cacheable); return getEntityManager().createQuery(jpql, getType()).setHint(
     * "org.hibernate.cacheable", cacheable);
     * 
     * }
     */

    public Class<T> getType() {
        return type;
    }

    public List<T> fildAllCriteria() {
        Criteria criteria = getSession().createCriteria(type);
        return criteria.list();
    }

    /**
     * Obter todos os registros do banco de dados ordenados pelo atributo passado por par�metro.
     * 
     * @param orderBy
     *            Nome do atributo de ordena��o.
     * @return Lista de entidades ordenada.
     */
    
    public List<T> findAll(String orderBy) {
        Criteria criteria = getSession().createCriteria(type);
        criteria.addOrder(Order.asc(orderBy));
        return criteria.list();
    }

    public List<T> findByCriteria(Criterion... criterion) {
        Criteria criteria = getSession().createCriteria(type);
        for (int i = 0; i < criterion.length; i++) {
            criteria.add(criterion[i]);
        }
        return criteria.list();
    }

    
    public List<T> findByCriteria(String orderField, Criterion... criterion) {
        Criteria criteria = getSession().createCriteria(type);
        for (int i = 0; i < criterion.length; i++) {
            criteria.add(criterion[i]);
        }
        criteria.addOrder(Order.asc(orderField));
        return criteria.list();
    }

    
    public List<T> findByExample(final T exampleInstance) {
        Criteria criteria = getSession().createCriteria(type);
        Example ex = Example.create(exampleInstance);
        criteria.add(ex);
        return criteria.list();
    }

    public List<T> findByExample(final T exampleInstance, String[] excludeProperty) {
        return findByExample(exampleInstance, MatchMode.START, excludeProperty);
    }

    public List<T> findByExample(final T exampleInstance, final String[] fetchAssociations, final String... excludeProperty) {
        return findByExample(exampleInstance, MatchMode.START, fetchAssociations, excludeProperty);
    }

    public List<T> findByExample(final T exampleInstance, final String[] fetchAssociations, final Map<String, Object> associationCriteria,
            final String... excludeProperty) {
        return findByExample(exampleInstance, MatchMode.START, fetchAssociations, associationCriteria, excludeProperty);
    }

    
    public List<T> findByExample(final T exampleInstance, final MatchMode mode, final String... excludeProperty) {
        Criteria crit = getSession().createCriteria(type);
        final org.hibernate.criterion.Example example = org.hibernate.criterion.Example.create(exampleInstance);
        for (String exclude : excludeProperty) {
            example.excludeProperty(exclude);
        }
        example.ignoreCase();
        example.enableLike(mode);
        crit.add(example);
        final List<T> result = crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        return result;
    }

    
    public List<T> findByExample(final T exampleInstance, final MatchMode mode, final String[] fetchAssociations, final String... excludeProperty) {

        Criteria crit = getSession().createCriteria(type);

        final org.hibernate.criterion.Example example = org.hibernate.criterion.Example.create(exampleInstance);
        for (String exclude : excludeProperty) {
            example.excludeProperty(exclude);
        }
        example.ignoreCase();
        example.enableLike(mode);

        crit.add(example);

        if (fetchAssociations != null && fetchAssociations.length > 0) {
            for (String association : fetchAssociations) {
                crit.setFetchMode(association, FetchMode.JOIN);
            }
        }

        final List<T> result = crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        return result;
    }

    
    public List<T> findByExample(final T exampleInstance, final MatchMode mode, final String[] fetchAssociations,
            final Map<String, Object> associationCriteria, final String... excludeProperty) {

        Criteria crit = getSession().createCriteria(type);

        final org.hibernate.criterion.Example example = org.hibernate.criterion.Example.create(exampleInstance);
        for (String exclude : excludeProperty) {
            example.excludeProperty(exclude);
        }
        example.ignoreCase();
        example.enableLike(mode);

        crit.add(example);

        if (fetchAssociations != null && fetchAssociations.length > 0) {
            for (String association : fetchAssociations) {
                crit.setFetchMode(association, FetchMode.JOIN);
            }
        }

        if (associationCriteria != null && !associationCriteria.isEmpty()) {
            for (String criterionName : associationCriteria.keySet()) {
                final Criteria criterion = crit.createCriteria(criterionName);
                final Example criteriaExample = Example.create(associationCriteria.get(criterionName));
                criteriaExample.ignoreCase().enableLike(mode);
                criterion.add(criteriaExample);
            }
        }

        final List<T> result = crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

        return result;
    }

    
    public List<T> findByExample(final T exampleInstance, MatchMode matchMode, boolean ignoreCase) {

        Criteria crit = getSession().createCriteria(type);

        final org.hibernate.criterion.Example example = org.hibernate.criterion.Example.create(exampleInstance);

        if (ignoreCase) {
            example.ignoreCase();
        }

        example.enableLike(matchMode);

        crit.add(example);
        final List<T> result = crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        return result;
    }

    
    public List<T> findByProperty(String propertyName, Object value) {

        Criteria criteria = getSession().createCriteria(type);
        criteria.add(Restrictions.eq(propertyName, value));

        return criteria.list();
    }

    
    public List<T> findByProperty(String[] propertyName, Object[] value) {

        Criteria criteria = getSession().createCriteria(type);

        for (int i = 0; i < propertyName.length; i++) {
            criteria.add(Restrictions.eq(propertyName[i], value[i]));
        }

        return criteria.list();
    }

    
    public List<T> findByProperty(String propertyName, String value, MatchMode matchMode) {

        Criteria criteria = getSession().createCriteria(type);
        criteria.add(Restrictions.like(propertyName, value, matchMode));

        return criteria.list();
    }

    
    public List<T> findByPropertyBetween(String propertyName, Object valueInit, Object valueEnd) {

        Criteria criteria = getSession().createCriteria(type);

        criteria.add(Restrictions.between(propertyName, valueInit, valueEnd));

        return criteria.list();
    }

    
    public List<T> findByPropertyInList(String propertyName, List<Object> values) {

        Criteria criteria = getSession().createCriteria(type);

        criteria.add(Restrictions.in(propertyName, values));

        return criteria.list();
    }

    
    public List<T> findByPropertyLike(String propertyName, String value) {

        Criteria criteria = getSession().createCriteria(type);

        criteria.add(Restrictions.eq(propertyName, value));

        return criteria.list();
    }

    
    public List<T> findByPropertyLike(String[] propertyName, Object[] value) {

        Criteria criteria = getSession().createCriteria(type);

        for (int i = 0; i < propertyName.length; i++) {
            criteria.add(Restrictions.like(propertyName[i], String.valueOf(value[i]), MatchMode.ANYWHERE));
        }

        return criteria.list();
    }

    
    public List<T> findByPropertyLikeOrdered(String propertyName, String value, String orderBy) {

        Criteria criteria = getSession().createCriteria(type);

        criteria.add(Restrictions.like(propertyName, value, MatchMode.ANYWHERE));

        criteria.addOrder(Order.asc(orderBy));

        return criteria.list();
    }

    
    public List<T> findByPropertyOrdered(String propertyName, Object value, String orderBy) {

        Criteria criteria = getSession().createCriteria(type);

        criteria.add(Restrictions.eq(propertyName, value));

        criteria.addOrder(Order.asc(orderBy));

        return criteria.list();
    }
    
    public <T extends BaseEntity> void atualizarListaObjetos(Collection<T> dadosPersistidos, List<T> dadosSerializados) throws Exception{
    	this.atualizarListaObjetos(dadosPersistidos, dadosSerializados, null);
    }
    
	public <T extends BaseEntity> void atualizarListaObjetos(Collection<T> dadosPersistidos, List<T> dadosSerializados, String[] camposExclusao)throws Exception{
		if(dadosPersistidos==null ){
			dadosPersistidos = new HashSet<T>();
		}
		if(dadosSerializados==null){
			return;
		}
		if(dadosPersistidos.isEmpty()){
			dadosPersistidos.addAll(dadosSerializados);
			return;
		}
		
		Iterator<T> itPersistidos = dadosPersistidos.iterator();
		//primeiro removemos os excluidos
		while(itPersistidos.hasNext()){
			T obj = itPersistidos.next();
			if(!dadosSerializados.contains(obj)){
				itPersistidos.remove();
			}
		}		
		
		//Alteramos os alterados
		itPersistidos = dadosPersistidos.iterator();		
		while(itPersistidos.hasNext()){
			T objPersistido = itPersistidos.next();
			if(dadosSerializados.contains(objPersistido)){
				Object objSerializado = dadosSerializados.get(dadosSerializados.indexOf(objPersistido));
				if(camposExclusao != null){
					cloneObj(objPersistido, objSerializado, camposExclusao);
				} else {
					cloneObj(objPersistido, objSerializado);					
				}
			}
		}		
		
		
		//Adicionamos os novos
		Iterator<T> itSerializados =dadosSerializados.iterator();
		while(itSerializados.hasNext()){
			T obj = itSerializados.next();
			if(!dadosPersistidos.contains(obj)){
				dadosPersistidos.add(obj);
			}
		}
	}
	
	public void cloneObj(Object destino, Object origem ) throws Exception{		
		cloneObj(destino, origem, new String[]{});
	}
	
	public boolean isEmpty(T entity, String collectionName){
		getSession().refresh(entity);		
		Collection collection = null;
		try {
			Method m = entity.getClass().getMethod("get" + StringUtils.capitalize(collectionName));
			collection = (Collection) m.invoke(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (collection != null) ? collection.isEmpty():true;
	}
	
	public void cloneObj(Object destino, Object origem, String... camposExclusao) throws Exception{
		Class<?> classe =  destino.getClass();
		Field[] campos = classe.getDeclaredFields();
		List<String> listaCamposExclusaoCopia = camposExclusao != null ? Arrays.asList(camposExclusao) : new ArrayList<String>();
		for (Field campo : campos) {
			if(java.lang.reflect.Modifier.isStatic(campo.getModifiers())){
				continue;
			}
			if(listaCamposExclusaoCopia.contains(campo.getName())){
				continue;
			}
			Method get = classe.getMethod(ReflectionUtil.obterNomeMetodoGET(campo.getName(), campo.getType()), new Class[] {});
			Method set = classe.getMethod(ReflectionUtil.obterNomeMetodoSET(campo.getName()), new Class[] { campo.getType() });
			if (origem != null) {				
				if (get.invoke(origem, new Object[] {}) != null) {					
						set.invoke(destino, new Object[] { get.invoke(origem,new Object[] {}) });
				}
			}
		}
	}
}