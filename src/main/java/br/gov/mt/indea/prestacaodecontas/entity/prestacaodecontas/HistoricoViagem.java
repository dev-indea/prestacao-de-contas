package br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.prestacaodecontas.enums.Dominio.TipoEventoViagem;

@Audited
@Entity
@Table(name="historico_viagem")
public class HistoricoViagem extends BaseEntity<Long> implements Serializable, Comparable<HistoricoViagem>{

	private static final long serialVersionUID = -3972600345667980049L;

	@Id
	@SequenceGenerator(name="historico_viagem_seq", sequenceName="historico_viagem_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="historico_viagem_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro = Calendar.getInstance();
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_evento")
	private Calendar dataEvento = Calendar.getInstance();
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_prestacao_de_contas")
	private PrestacaoDeContas prestacaoDeContas;
	
	@Column(name="observacao", length=500)
	private String observacao;

	@Enumerated(EnumType.STRING)
	@Column(name="tipo_evento_viagem")
	private TipoEventoViagem tipoEventoViagem;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataEvento() {
		return dataEvento;
	}

	public void setDataEvento(Calendar dataEvento) {
		this.dataEvento = dataEvento;
	}

	public PrestacaoDeContas getPrestacaoDeContas() {
		return prestacaoDeContas;
	}

	public void setPrestacaoDeContas(PrestacaoDeContas prestacaoDeContas) {
		this.prestacaoDeContas = prestacaoDeContas;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public TipoEventoViagem getTipoEventoViagem() {
		return tipoEventoViagem;
	}

	public void setTipoEventoViagem(TipoEventoViagem tipoEventoViagem) {
		this.tipoEventoViagem = tipoEventoViagem;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoViagem other = (HistoricoViagem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	@Override
	public int compareTo(HistoricoViagem o) {
		if (this.getDataEvento().after(o.getDataEvento()))
			return -1;
		else if (this.getDataEvento().before(o.getDataEvento()))
			return 1;
		else
			return 0;
	}
	
}
