package br.gov.mt.indea.prestacaodecontas.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;

import br.gov.mt.indea.prestacaodecontas.entity.dbindea.Unidade;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class UnidadeService extends PaginableService<Unidade, Long> {

	protected UnidadeService() {
		super(Unidade.class);
	}

	@SuppressWarnings("unchecked")
	public List<Unidade> findAllOrderByNome() {
		List<Unidade> listaUnidade = null;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select new Unidade(unidade.id, unidade.nome, unidade.sigla, unidade.email, unidade.unidadePai, unidade.tipoUnidade, unidade.municipio)")
		   .append("  from Unidade unidade ")
		   .append("  left join unidade.unidadePai ")
		   .append("  left join unidade.municipio ")
		   .append(" order by unidade.nome");
		
		Query query = getSession().createQuery(sql.toString());
		listaUnidade = query.list();
		
		return listaUnidade;
	}
	
	@Override
	public void validar(Unidade model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarPersist(Unidade model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarMerge(Unidade model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarDelete(Unidade model) {
		// TODO Auto-generated method stub
		
	}

}
