package br.gov.mt.indea.prestacaodecontas.listener;

import org.hibernate.envers.RevisionListener;

import br.gov.mt.indea.prestacaodecontas.entity.envers.SIZRevisionEntity;
import br.gov.mt.indea.prestacaodecontas.security.UserSecurity;
import br.gov.mt.indea.prestacaodecontas.util.CDIServiceLocator;

public class SIZEnversListener implements RevisionListener{
	
	@Override
	public void newRevision(Object revisionEntity) {
		SIZRevisionEntity entity = (SIZRevisionEntity) revisionEntity;
		
		UserSecurity userSecurity = CDIServiceLocator.getBean(UserSecurity.class);
		
		if (userSecurity != null){
			entity.setUsername(userSecurity.getUsuario().getId());
			entity.setUsuario(userSecurity.getUsuario().getNome());
			entity.setIp(userSecurity.getIp());
		} else{
			entity.setUsername("anonymous");
			entity.setUsuario("anonymous");
		}
		
	}

}
