package br.gov.mt.indea.prestacaodecontas.util;

import java.text.Normalizer;

public class StringUtil {

	public static String removeAcentos(String str) {
		str = Normalizer.normalize(str, Normalizer.Form.NFD);
		str = str.replaceAll("[^\\p{ASCII}]", "");
		return str;
	}
}
