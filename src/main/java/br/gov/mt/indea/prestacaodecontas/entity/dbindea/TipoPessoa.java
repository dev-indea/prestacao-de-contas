package br.gov.mt.indea.prestacaodecontas.entity.dbindea;

import br.gov.mt.indea.prestacaodecontas.entity.dbindea.validation.FisicaGroup;
import br.gov.mt.indea.prestacaodecontas.entity.dbindea.validation.JuridicaGroup;

public enum TipoPessoa {
    FISICA("F�sica", "CPF", "999.999.999-99", FisicaGroup.class),
    JURIDICA("Jur�dica", "CNPJ", "99.999.999/9999-99", JuridicaGroup.class);

    private String descricao;
    private String rotulo;
    private String mascara;
    private Class<?> grupoValidacao;

    private TipoPessoa(String descricao, String rotulo, String mascara, Class<?> grupoValidacao) {
        this.descricao = descricao;
        this.rotulo = rotulo;
        this.mascara = mascara;
        this.grupoValidacao = grupoValidacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getRotulo() {
        return rotulo;
    }

    public String getMascara() {
        return mascara;
    }

    public String getGrupoValidacao() {
        return grupoValidacao.getCanonicalName();
    }
}
