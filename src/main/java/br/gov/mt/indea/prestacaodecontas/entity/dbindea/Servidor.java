package br.gov.mt.indea.prestacaodecontas.entity.dbindea;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CPF;

import br.gov.mt.indea.prestacaodecontas.annotation.IndeaWeb;
import br.gov.mt.indea.prestacaodecontas.entity.dbindea.validation.FisicaGroup;

@Entity
@DiscriminatorValue("1")
@Table(name = "servidor", schema = "cogesp")
@IndeaWeb
public class Servidor extends Pessoa {

	private static final long serialVersionUID = 7811664327889819479L;

	@NotBlank
    @Size(max = 10)
    @Column(nullable = false, length = 10)
    private String matricula;
    
	@Past
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(nullable = false, name = "data_nascimento")
    private Date dataNascimento;
    
	@NotNull
    @Temporal(TemporalType.DATE)
    @Column(nullable = false, name = "data_exercicio")
    private Date dataExercicio;
    
	@NotBlank
    @CPF(groups = FisicaGroup.class)
    @Size(max = 14)
    @Column(nullable = false, length = 14)
    private String cpf;
    
	private boolean responsavel = false;
    
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unidade_id")
    private Unidade unidade;
    
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cargo_id")
    private Cargo cargo;
    
    @Transient
    public TipoPessoa getTipoPessoa(){
        return TipoPessoa.FISICA;
    }

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Date getDataExercicio() {
		return dataExercicio;
	}

	public void setDataExercicio(Date dataExercicio) {
		this.dataExercicio = dataExercicio;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public boolean isResponsavel() {
		return responsavel;
	}

	public void setResponsavel(boolean responsavel) {
		this.responsavel = responsavel;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Servidor other = (Servidor) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		return true;
	}
    
}
