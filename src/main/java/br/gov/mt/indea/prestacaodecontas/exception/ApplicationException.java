package br.gov.mt.indea.prestacaodecontas.exception;

@javax.ejb.ApplicationException(rollback=true)
public class ApplicationException extends Exception {

	private static final long serialVersionUID = -5849615990643923188L;
	
	private String mensagem;
	
	private Throwable throwable;
	
	public ApplicationException(){
		super();
	}
	
	public ApplicationException(String mensagem){
		super(mensagem);
		this.mensagem = mensagem;
	}
	
	public ApplicationException(Throwable throwable){
		super(throwable);
		this.throwable = throwable;
	}
	
	public ApplicationException(String mensagem, Throwable throwable){
		super(mensagem, throwable);
		this.mensagem = mensagem;
		this.throwable = throwable;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

}
