package br.gov.mt.indea.prestacaodecontas.exception.handlers;

import java.io.IOException;
import java.sql.BatchUpdateException;
import java.time.LocalDate;
import java.util.Iterator;

import javax.faces.FacesException;
import javax.faces.FactoryFinder;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialResponseWriter;
import javax.faces.context.ResponseWriter;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import javax.faces.render.RenderKit;
import javax.faces.render.RenderKitFactory;
import javax.faces.view.facelets.FaceletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.lang.StringUtils;
import org.postgresql.util.PSQLException;
import org.postgresql.util.ServerErrorMessage;
import org.primefaces.application.exceptionhandler.PrimeExceptionHandler;
import org.primefaces.expression.ComponentNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocpsoft.pretty.PrettyException;

import br.gov.mt.indea.prestacaodecontas.enums.PostgreSQLErrorCodes;
import br.gov.mt.indea.prestacaodecontas.exception.ApplicationErrorException;
import br.gov.mt.indea.prestacaodecontas.exception.ApplicationException;
import br.gov.mt.indea.prestacaodecontas.exception.ApplicationRuntimeException;
import br.gov.mt.indea.prestacaodecontas.managedbeans.ErroManagedBean;
import br.gov.mt.indea.prestacaodecontas.util.CDIServiceLocator;
import br.gov.mt.indea.prestacaodecontas.util.ExceptionUtil;
import br.gov.mt.indea.prestacaodecontas.util.FacesMessageUtil;
import br.gov.mt.indea.prestacaodecontas.util.TokenGenerator;

public class SIZExceptionHandler extends PrimeExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(SIZExceptionHandler.class);
	
	private ErroManagedBean erroManagedBean = CDIServiceLocator.getBean(ErroManagedBean.class);

	public SIZExceptionHandler(ExceptionHandler wrapped) {
		super(wrapped);
	}

	@Override
	protected void logException(Throwable rootCause) {
		LocalDate date = LocalDate.now();
		String token = TokenGenerator.csRandomAlphaNumericString(30);
		token = date.getDayOfMonth() + "" + String.format("%02d", date.getMonthValue()) + "" + date.getYear() + token;
		erroManagedBean.setCodigo(token);

		log.error("Erro: {}", token, rootCause);
	}
	
	public void doRedirect(FacesContext context, String redirectPage) throws FacesException {
		ExternalContext externalContext = context.getExternalContext();

		try {

			if ((context.getPartialViewContext().isAjaxRequest() || context.getPartialViewContext().isPartialRequest())
					&& (context.getResponseWriter() == null) && (context.getRenderKit() == null)) {

				ServletResponse response = (ServletResponse) externalContext.getResponse();
				ServletRequest request = (ServletRequest) externalContext.getRequest();

				RenderKitFactory factory = (RenderKitFactory) FactoryFinder
						.getFactory(FactoryFinder.RENDER_KIT_FACTORY);
				RenderKit renderKit = factory.getRenderKit(context,
						context.getApplication().getViewHandler().calculateRenderKitId(context));
				ResponseWriter responseWriter = renderKit.createResponseWriter(response.getWriter(), null,
						request.getCharacterEncoding());
				responseWriter = new PartialResponseWriter(responseWriter);
				context.setResponseWriter(responseWriter);

			}

			externalContext.redirect(externalContext.getRequestContextPath()
					+ (redirectPage != null ? redirectPage : StringUtils.EMPTY));

		} catch (IOException e) {
			log.error("IOException");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handle() throws FacesException {
		for (Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator(); i.hasNext();) {

			ExceptionQueuedEvent event = i.next();
			ExceptionQueuedEventContext eventContext = (ExceptionQueuedEventContext) event.getSource();
			Throwable exception = ExceptionUtil.findException(eventContext.getException(), ApplicationException.class,
					ApplicationErrorException.class, BatchUpdateException.class, IOException.class, ApplicationRuntimeException.class);

			if (exception instanceof ApplicationErrorException) {
				try{
					handleApplicationErrorException((ApplicationErrorException) exception);
				} finally {
					i.remove();
				}
			} else if (exception instanceof ApplicationException) {
				try{
					handleApplicationException((ApplicationException) exception);
				} finally {
					i.remove();
				}
			} else if (exception instanceof ViewExpiredException) {
				try{
					handleViewExpiredException((ViewExpiredException) exception);
				} finally {
					i.remove();
				}
			} else if (exception instanceof BatchUpdateException) {
				try{
					handleBatchUpdateException(	(BatchUpdateException) exception);
				} finally {
					i.remove();
				}
			} else if (exception instanceof FaceletException){
				try{
					handleFaceletException((FaceletException) exception);
				} finally {
					i.remove();
				}
			} else if (exception instanceof PrettyException){
				try{
					handlePrettyException((PrettyException) exception);
				} finally {
					i.remove();
				}
			} else if (exception instanceof ApplicationRuntimeException){
				try{
					handleApplicationRuntimeException((ApplicationRuntimeException) exception);
				} finally {
					i.remove();
				}
			} else if (exception instanceof IOException) {
				try{
					log.warn("::BROKEN PIPE::", exception);
					handleSystemException(exception);
				} finally {
					i.remove();
				}
			} else if (exception instanceof ComponentNotFoundException) {
				try{
					handleFaceletException((FaceletException) exception);
				} finally {
					i.remove();
				}
			} else {
				Throwable psqlException = ExceptionUtil.findException(exception, PSQLException.class);
				
				if (psqlException != null && psqlException instanceof PSQLException)
					try{
						handlePSQLException((PSQLException) psqlException);
					} catch (ClassCastException e) {
						handleSystemException(exception);
					} finally {
						i.remove();
					}
				else{
					super.handle();
				}
			}
		}

		getWrapped().handle();

	}

	private void handleApplicationErrorException(ApplicationErrorException exception) {
		LocalDate date = LocalDate.now();
		String token = TokenGenerator.csRandomAlphaNumericString(30);
		token = date.getDayOfMonth() + "" + String.format("%02d", date.getMonthValue()) + "" + date.getYear() + token;
		erroManagedBean.setCodigo(token);

		log.error("Erro desconhecido: {}", token, exception);
		
		FacesContext context = FacesContext.getCurrentInstance();
		doRedirect(context, "/erro.jsf");
		context.renderResponse();
	}

	private void handleApplicationException(ApplicationException e) {
		log.warn("Erro desconhecido: {}", e.getLocalizedMessage(), e);
		
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessageUtil.addErrorContextFacesMessage(e.getMensagem(), e.getLocalizedMessage());
		context.renderResponse();
	}
	
	private void handleApplicationRuntimeException(ApplicationRuntimeException e) {
		log.warn("Warning: {}", e.getLocalizedMessage(), e);
		
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessageUtil.addErrorContextFacesMessage(e.getMensagem(), e.getLocalizedMessage());
		context.renderResponse();
	}
	
	private void handleFaceletException(FaceletException exception) {
		LocalDate date = LocalDate.now();
		String token = TokenGenerator.csRandomAlphaNumericString(30);
		token = date.getDayOfMonth() + "" + String.format("%02d", date.getMonthValue()) + "" + date.getYear() + token;
		erroManagedBean.setCodigo(token);

		log.error("Ocorreu um erro de sintaxe no xhtml: {}", token, exception);
		
		FacesContext context = FacesContext.getCurrentInstance();
		doRedirect(context, "/erro.jsf");
		context.renderResponse();
	}
	
	private void handlePrettyException(PrettyException exception) {
		LocalDate date = LocalDate.now();
		String token = TokenGenerator.csRandomAlphaNumericString(30);
		token = date.getDayOfMonth() + "" + String.format("%02d", date.getMonthValue()) + "" + date.getYear() + token;
		erroManagedBean.setCodigo(token);

		log.error("Erro desconhecido: {}", token, exception);
		
		FacesContext context = FacesContext.getCurrentInstance();
		doRedirect(context, "/erro.jsf");
		context.renderResponse();
	}

	private void handleSystemException(Throwable exception) {
		LocalDate date = LocalDate.now();
		String token = TokenGenerator.csRandomAlphaNumericString(30);
		token = date.getDayOfMonth() + "" + String.format("%02d", date.getMonthValue()) + "" + date.getYear() + token;
		erroManagedBean.setCodigo(token);
		
		log.error("Erro desconhecido: {}",  token, exception);
		
		FacesContext context = FacesContext.getCurrentInstance();
		doRedirect(context, "/erro.jsf");
		context.renderResponse();
	}

	private void handleViewExpiredException(ViewExpiredException exception) {
		FacesContext context = FacesContext.getCurrentInstance();
		doRedirect(context, "/dashboard.jsf");
		context.renderResponse();
	}

	private void handleBatchUpdateException(BatchUpdateException exception) {
		PSQLException psqlException = (PSQLException) exception.getNextException();
		this.handlePSQLException(psqlException);
	}
	
	private void handlePSQLException(PSQLException exception){
		
		ServerErrorMessage serverErrorMessage = exception.getServerErrorMessage();
		
		String sqlState = exception.getSQLState();
		
		PostgreSQLErrorCodes errorCode = PostgreSQLErrorCodes.getPostgreSQLErrorCodesByVendorCode(sqlState);

		String message;
		if (errorCode.equals(PostgreSQLErrorCodes.UNIQUE_VIOLATION)){
			String[] detalhe = serverErrorMessage.getDetail().split(" ")[1].split("=");
			
			message = "Valor \"" + detalhe[1].replace("(", "").replace(")", "") + "\" j� est� cadastrado";
		} else if (errorCode.equals(PostgreSQLErrorCodes.FOREIGN_KEY_VIOLATION)){
			String table = serverErrorMessage.getTable();
			
			message = "Registro filho n� encontrado na tabela " + table;
		} else if (errorCode.equals(PostgreSQLErrorCodes.CONNECTION_DOES_NOT_EXIST) ||
				   errorCode.equals(PostgreSQLErrorCodes.CONNECTION_FAILURE) ||
				   errorCode.equals(PostgreSQLErrorCodes.SQLCLIENT_UNABLE_TO_ESTABLISH_SQLCONNECTION) ||
				   errorCode.equals(PostgreSQLErrorCodes.SQLSERVER_REJECTED_ESTABLISHMENT_OF_SQLCONNECTION)){
		
			erroManagedBean.setMensagem("N�o foi poss�vel conectar com o banco de dados. Contate o administrador");
		    
			FacesContext context = FacesContext.getCurrentInstance();
			doRedirect(context, "/erro.jsf");
			context.renderResponse();
			
			return;
		} else{
			message = PostgreSQLErrorCodes.getMessage(exception.getSQLState() + "");
			
			if (message.isEmpty()) {
				message = exception.getMessage();
			}
		}
		
		log.error("Erro inesperado no banco de dados: {}", exception.getLocalizedMessage());
		
		FacesMessageUtil.addErrorContextFacesMessage("Ocorreu um erro", message);
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.renderResponse();
	}
	
}
