package br.gov.mt.indea.prestacaodecontas.managedbeans.seguranca;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.prestacaodecontas.entity.seguranca.Permissao;
import br.gov.mt.indea.prestacaodecontas.service.LazyObjectDataModel;
import br.gov.mt.indea.prestacaodecontas.service.seguranca.PermissaoService;
import br.gov.mt.indea.prestacaodecontas.util.FacesMessageUtil;


@Named
@ViewScoped
@URLBeanName("permissaoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarPermissao", pattern = "/permissao/pesquisar", viewId = "/pages/security/permissao/lista.jsf"),
		@URLMapping(id = "incluirPermissao", pattern = "/permissao/incluir", viewId = "/pages/security/permissao/novo.jsf"),
		@URLMapping(id = "alterarPermissao", pattern = "/permissao/alterar/#{permissaoManagedBean.id}", viewId = "/pages/security/permissao/novo.jsf")})
public class PermissaoManagedBean implements Serializable{

	private static final long serialVersionUID = -107693789607909430L;

	private Long id;
	
	private Permissao permissao;
	
	private LazyObjectDataModel<Permissao> listaPermissao;
	
	@Inject
	private PermissaoService permissaoService;
	
	@PostConstruct
	private void init(){
		this.listaPermissao = new LazyObjectDataModel<Permissao>(this.permissaoService);
	}
	
	private void limpar() {
		this.permissao = new Permissao();
	}
	
	@URLAction(mappingId = "incluirPermissao", onPostback = false)
	public void novo(){
		this.limpar();
	}
	
	@URLAction(mappingId = "alterarPermissao", onPostback = false)
	public void editar(){
		this.permissao = permissaoService.findByIdFetchAll(this.getId());
	}
	
	public String adicionar() throws IOException{
		
		if (permissao.getId() != null){
			this.permissaoService.saveOrUpdate(permissao);
			FacesMessageUtil.addInfoContextFacesMessage("Permissao atualizada", "");
		}else{
			this.permissao.setDataCadastro(Calendar.getInstance());
			this.permissaoService.saveOrUpdate(permissao);
			FacesMessageUtil.addInfoContextFacesMessage("Permissao adicionada", "");
		}
		
		limpar();
	    return "pretty:pesquisarPermissao";
	}
	
	public void remover(Permissao permissao){
		this.permissaoService.delete(permissao);
		FacesMessageUtil.addInfoContextFacesMessage("Permissao exclu�do com sucesso", "");
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}

	public LazyObjectDataModel<Permissao> getListaPermissao() {
		return listaPermissao;
	}

	public void setListaPermissao(
			LazyObjectDataModel<Permissao> listaPermissao) {
		this.listaPermissao = listaPermissao;
	}
	
	
}
