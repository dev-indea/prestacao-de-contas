package br.gov.mt.indea.prestacaodecontas.managedbeans;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.gov.mt.indea.prestacaodecontas.entity.dbindea.Uf;
import br.gov.mt.indea.prestacaodecontas.entity.dbindea.Unidade;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.*;
import br.gov.mt.indea.prestacaodecontas.service.UfService;
import br.gov.mt.indea.prestacaodecontas.service.UnidadeService;

@Named
@ApplicationScoped
public class ListsManagedBean implements Serializable{

	private static final long serialVersionUID = 1096028368132582154L;

	private List<Uf> listaUFs;
	
	private Map<Long, Unidade> mapUnidades;
	
	private List<Unidade> listaUnidades;
	
	@Inject
	private UfService ufService;
	
	@Inject
	private UnidadeService unidadeService;
	
	public ListsManagedBean(){
	}
	
	@PostConstruct
	public void init(){
		this.listaUFs = ufService.findAll("nome");
		this.listaUnidades = unidadeService.findAllOrderByNome();
		
		mapUnidades = new HashMap<>();
		for (Unidade unidade : listaUnidades) {
			mapUnidades.put(unidade.getId(), unidade);
		}
	}
	
	public List<Uf> getListaUFs(){	
		return listaUFs;
	}
	
	public List<Unidade> getListaUnidades() {
		return listaUnidades;
	}
	
	public Unidade getUnidadeById(Long id) {
		return mapUnidades.get(id);
	}

	public TipoServidor[] getListaTipoServidor(){
		return TipoServidor.values();
	}
	
	public TipoEventoViagem[] getListaTipoEventoViagem(){
		return TipoEventoViagem.values();
	}

	public AtivoInativo[] getListaAtivoInativo(){
		return AtivoInativo.values();
	}
	
	public AtivoPassivo[] getListaAtivoPassivo(){
		return AtivoPassivo.values();
	}
	
	public AtivaPassiva[] getListaAtivaPassiva(){
		return AtivaPassiva.values();
	}
	
	public SimNao[] getListaSimNao(){
		return SimNao.values();
	}
	
	public SimNaoSI[] getListaSimNaoSI(){
		return SimNaoSI.values();
	}
	
	public SimNaoNA[] getListaSimNaoNA(){
		return SimNaoNA.values();
	}
	
	public FonteDaNotificacao[] getListaFonteDaNotificacao(){
		return FonteDaNotificacao.values();
	}
	
	public MotivoInicialDaInvestigacao[] getListaMotivoInicialDaInvestigacao(){
		return MotivoInicialDaInvestigacao.values();
	}
	
	public TipoPropriedade[] getListaTipoPropriedadeFormIN(){
		return TipoPropriedade.getTipoPropriedadeFormIN();
	}
	
	public TipoPropriedade[] getListaTipoPropriedadeEquinos(){
		return TipoPropriedade.getTipoPropriedadeEquinos();
	}
	
	public SistemaDeCriacaoPropriedade[] getListaSistemaDeCriacaoPropriedade(){
		return SistemaDeCriacaoPropriedade.values();
	}
	
	public TipoCoordenadaGeografica[] getListaTipoCoordenadaGeografica(){
		return TipoCoordenadaGeografica.values();
	}
	
	public TipoCoordenadaGeografica[] getListaTipoCoordenadaGeograficaFormSN(){
		return TipoCoordenadaGeografica.getListaTipoCoordenadaGeograficaFormSN();
	}
	
	public FuncaoNoEstabelecimento[] getListaFuncaoNoEstabelecimento(){
		return FuncaoNoEstabelecimento.values();
	}
	
	public TipoTelefone[] getListaTipoTelefone(){
		return TipoTelefone.values();
	}
	
	public VigilanciaSindromica[] getListaVigilanciaSindromica(){
		return VigilanciaSindromica.values();
	}
	
	public TipoTransitoDeAnimais[] getListaTipoTransitoDeAnimais(){
		return TipoTransitoDeAnimais.values();
	}
	
	public MedidasAdotadasNoEstabelecimento[] getListaMedidasAdotadasNoEstabelecimentos_FormIN(){
		return MedidasAdotadasNoEstabelecimento.getFormINValues();
	}
	
	public MedidasAdotadasNoEstabelecimento[] getListaMedidasAdotadasNoEstabelecimentos_FormCOM(){
		return MedidasAdotadasNoEstabelecimento.getFormCOMValues();
	}
	
	public TipoDestinoExploracao[] getListaTipoDestinoExploracao(){
		return TipoDestinoExploracao.values();
	}
	
	public FinalidadeExploracaoBovinosEBubalinos[] getListaFinalidadeExploracaoBovinosEBubalinos(){
		return FinalidadeExploracaoBovinosEBubalinos.getFinalidadeExploracaoBovinosEBubalinos();
	}
	
	public FaseExploracaoBovinosEBubalinos[] getListaFaseExploracaoBovinosEBubalinos_FormIN(){
		return FaseExploracaoBovinosEBubalinos.getListaFaseExploracaoBovinosEBubalinos_FormIN();
	}
	
	public FaseExploracaoBovinosEBubalinos[] getListaFaseExploracaoBovinosEBubalinos_Vigilancia(){
		return FaseExploracaoBovinosEBubalinos.getListaFaseExploracaoBovinosEBubalinos_Vigilancia();
	}
	
	public FinalidadeExploracaoCaprinos[] getListaFinalidadeExploracaoCaprinos(){
		return FinalidadeExploracaoCaprinos.values();
	}
	
	public SistemaDeCriacaoDeRuminantes[] getListaSistemaDeCriacaoDeRuminantes(){
		return SistemaDeCriacaoDeRuminantes.values();
	}
	
	public FaseExploracaoCaprinos[] getListaFaseExploracaoCaprinos(){
		return FaseExploracaoCaprinos.values();
	}

	public FinalidadeExploracaoOvinos[] getListaFinalidadeExploracaoOvinos(){
		return FinalidadeExploracaoOvinos.values();
	}
	
	public FaseExploracaoOvinos[] getListaFaseExploracaoOvinos(){
		return FaseExploracaoOvinos.values();
	}
	
	public TipoExploracaoSuinos[] getListaTipoExploracaoSuinos(){
		return TipoExploracaoSuinos.values();
	}
	
	public TipoExploracaoSuinos[] getListaTipoExploracaoSuinos_Subsistencia(){
		return TipoExploracaoSuinos.getTiposSubsistencia();
	}
	
	public TipoExploracaoSuinos[] getListaTipoExploracaoSuinos_Granja(){
		return TipoExploracaoSuinos.getTiposGranja();
	}
	
	public TipoExploracaoEquideos[] getListaTipoExploracaoEquideos(){
		return TipoExploracaoEquideos.values();
	}
	
	public TipoExploracaoAves[] getListaTipoExploracaoAves(){
		return TipoExploracaoAves.values();
	}
	
	public TipoExploracaoAbelhas[] getListaTipoExploracaoAbelhas(){
		return TipoExploracaoAbelhas.values();
	}
	
	public TipoExploracaoCoelhos[] getListaTipoExploracaoCoelhos(){
		return TipoExploracaoCoelhos.values();
	}
	
	public TipoInvestigacaoFormCOM[] getListaTipoInvestigacaoFormCOM(){
		return TipoInvestigacaoFormCOM.values();
	}
	
	public ProvavelOrigemFormCOM[] getListaProvavelOrigemFormCOM(){
		return ProvavelOrigemFormCOM.values();
	}
	
	public TipoOcorrenciaObservadaFormCOM[] getListaTipoOcorrenciaObservadaFormCOM(){
		return TipoOcorrenciaObservadaFormCOM.values();
	}
	
	public MomentoMedidasAdotadasNoEstabelecimento[] getListaMomentoMedidasAdotadasNoEstabelecimento(){
		return MomentoMedidasAdotadasNoEstabelecimento.values();
	}
	
	public TipoVinculoEpidemiologico[] getListaTipoVinculoEpidemiologico(){
		return TipoVinculoEpidemiologico.values();
	}
	
	public Especie[] getListaEspecies(){
		return Especie.values();
	}
	
	public Especie[] getListaEspeciesAfetadasFormIN(){
		return Especie.getEspeciesAfetadasFormIN();
	}
	public Especie[] getListaEspeciesLAB(){
		return Especie.getEspeciesLAB();
	}
	
	public Especie[] getListaEspeciesSV(){
		return Especie.getEspeciesSV();
	}
	
	public Especie[] getListaEspeciesSN(){
		return Especie.getEspeciesSN();
	}
	
	public Especie[] getListaEspeciesDeEquinos(){
		return Especie.getEspeciesDeEquinos();
	}
	
	public Especie[] getListaEspeciesDeAves(){
		return Especie.getEspeciesDeAves();
	}
	
	public Especie[] getListaEspeciesFormularioDeColheitaEET(){
		return Especie.getEspeciesFormularioDeColheitaEET();
	}
	
	public Especie[] getListaEspeciesInvestigacaoEpidemiologica(){
		return Especie.getEspeciesInvestigacaoEpidemiologica();
	}
	
	public Sexo[] getListaSexo_FormLAB(){
		return Sexo.getSexo_FormLAB();
	}
	
	public Sexo[] getListaSexo_FormEQ(){
		return Sexo.getSexo_FormEQ();
	}
	
	public Sexo[] getListaSexoSimples(){
		return Sexo.getSexoSimples();
	}
	
	public MachoFemea[] getListaMachoFemea(){
		return MachoFemea.values();
	}
	
	public Sexo[] getListaPeriodoGestacao(){
		return Sexo.getSexo_PeriodoGestacao();
	}
	
	public TipoPeriodo[] getListaTipoPeriodos(){
		return TipoPeriodo.values();
	}
	
	public TipoPeriodo[] getListaTipoPeriodo_AnoMes(){
		return TipoPeriodo.getListaTipoPeriodos_AnoMes();
	}
	
	public TipoPeriodo[] getListaTipoPeriodo_DiaAno(){
		return TipoPeriodo.getListaTipoPeriodos_DiaAno();
	}
	
	public TipoPeriodo[] getListaTipoPeriodo_DiaMes(){
		return TipoPeriodo.getListaTipoPeriodos_DiaMes();
	}
	
	public TipoCriacaoAnimais[] getListaTipoCriacaoAnimais(){
		return TipoCriacaoAnimais.values();
	}
	
	public TipoReposicaoAnimais[] getListaTipoReposicaoAnimais(){
		return TipoReposicaoAnimais.values();
	}
	
	public TipoOrigemRacaoAnimais[] getListaTipoOrigemRacaoAnimais(){
		return TipoOrigemRacaoAnimais.values();
	}
	
	public TipoOrigemRestosDeComida[] getListaTipoOrigemRestosDeComida(){
		return TipoOrigemRestosDeComida.values();
	}
	
	public TipoOrigemSoroOuRestosDeLavoura[] getListaTipoOrigemSoroOuRestosDeLavouras(){
		return TipoOrigemSoroOuRestosDeLavoura.values();
	}
	
	public SinaisClinicosSindromeVesicular[] getListaSinaisClinicosSindromeVesicular(){
		return SinaisClinicosSindromeVesicular.values();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_EstadoGeral(){
		return SinaisClinicosSindromeHemorragica.getLesoesEstadoGeral();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaRespiratorio(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaRespiratorio();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaNervoso(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaNervoso();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaDigestorio(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaDigestorio();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaReprodutivo(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaReprodutivo();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaTegumentar(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaTegumentar();
	}
	
	public SinaisClinicosSindromeHemorragica[] getListaSinaisClinicosSindromeHemorragica_SistemaLinfatico(){
		return SinaisClinicosSindromeHemorragica.getLesoesSistemaLinfatico();
	}
	
	public SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getListaSinaisClinicosSindromeNervosasSindromeNervosaERespiratoriaDasAves_EstadoGeral(){
		return SinaisClinicosSindromeNervosaERespiratoriaDasAves.getSinaisClinicosSindromeNervosa_EstadoGeral();
	}
	
	public SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getListaSinaisClinicosSindromeNervosasSindromeNervosaERespiratoriaDasAves_SistemaRespiratorio(){
		return SinaisClinicosSindromeNervosaERespiratoriaDasAves.getSinaisClinicosSindromeNervosa_SistemaRespiratorio();
	}
	
	public SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getListaSinaisClinicosSindromeNervosasSindromeNervosaERespiratoriaDasAves_SistemaNervoso(){
		return SinaisClinicosSindromeNervosaERespiratoriaDasAves.getSinaisClinicosSindromeNervosa_SistemaNervoso();
	}
	
	public SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getListaSinaisClinicosSindromeNervosasSindromeNervosaERespiratoriaDasAves_SistemaDigestorio(){
		return SinaisClinicosSindromeNervosaERespiratoriaDasAves.getSinaisClinicosSindromeNervosa_SistemaDigestorio();
	}
	
	public SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getListaSinaisClinicosSindromeNervosasSindromeNervosaERespiratoriaDasAves_SistemaCirculatorio(){
		return SinaisClinicosSindromeNervosaERespiratoriaDasAves.getSinaisClinicosSindromeNervosa_SistemaCirculatorio();
	}
	
	public SinaisClinicosCavidadeNasal[] getListaSinaisClinicosCavidadeNasal(){
		return SinaisClinicosCavidadeNasal.values();
	}
	
	public SinaisClinicosPele[] getListaSinaisClinicosPele(){
		return SinaisClinicosPele.values();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_EstadoGeral(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_EstadoGeral();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_SistemaRespiratorio(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_SistemaRespiratorio();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_SistemaUrinarioEReprodutor(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_SistemaUrinarioEReprodutor();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_SistemaCirculatorioHematopoteicoELinfatico(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_SistemaCirculatorioHematopoteicoELinfatico();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_SistemaDigestivo(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_SistemaDigestivo();
	}
	
	public ResultadoNecropsiaDeAves[] getListaResultadoNecropsiaDeAves_SistemaNervoso(){
		return ResultadoNecropsiaDeAves.getResultadoNecropsiaDeAves_SistemaNervoso();
	}
	
	public LocalDeColhimentoDeAmostra[] getListaLocalDeColhimentoDeAmostras(){
		return LocalDeColhimentoDeAmostra.values();
	}
	
	public MetodoParaEstipularIdade[] getListaMetodoParaEstipularIdade(){
		return MetodoParaEstipularIdade.values();
	}
	
	public CategoriaDoAnimalSubmetidoAVigilancia[] getListaCategoriaDoAnimalSubmetidoAVigilancia(){
		return CategoriaDoAnimalSubmetidoAVigilancia.values();
	}
	
	public TipoAmostra[] getListaTipoAmostras(){
		return TipoAmostra.values();
	}
	
	public MeioConservacaoAmostra[] getListaMeioConservacaoAmostras(){
		return MeioConservacaoAmostra.values();
	}
	
	public TipoAlimentoAves[] getListaTipoAlimentoAves(){
		return TipoAlimentoAves.values();
	}
	
	public TipoLaboratorio[] getListaTipoLaboratorio(){
		return TipoLaboratorio.values();
	}
	
	public FinalidadeDoTesteDeLaboratorio[] getListaFinalidadeDoTesteDeLaboratorio_LaboratorioCredenciado(){
		return FinalidadeDoTesteDeLaboratorio.getFinalidadeDoTesteDeLaboratorio_LaboratorioCredenciado();
	}
	
	public FinalidadeDoTesteDeLaboratorio[] getListaFinalidadeDoTesteDeLaboratorio_LaboratorioCredenciadoPublico(){
		return FinalidadeDoTesteDeLaboratorio.getFinalidadeDoTesteDeLaboratorio_LaboratorioCredenciadoPublico();
	}

	public FinalidadeDoTesteDeLaboratorio[] getListaFinalidadeDoTesteDeLaboratorio_LaboratorioOficial(){
		return FinalidadeDoTesteDeLaboratorio.getFinalidadeDoTesteDeLaboratorio_LaboratorioOficial();
	}
	
	public TipoDeTesteParaMormo[] getListaTipoDeTesteParaMormo(){
		return TipoDeTesteParaMormo.values();
	}
	
	public DireitaEsquerda[] getListaDireitaEsquerda(){
		return DireitaEsquerda.values();
	}
	
	public DireitoEsquerdo[] getListaDireitoEsquerdo(){
		return DireitoEsquerdo.values();
	}
	
	public PositivoNegativo[] getListaPositivoNegativoSimple(){
		return PositivoNegativo.getPositivoNegativoSimples();
	}
	
	public PositivoNegativo[] getListaPositivoNegativo(){
		return PositivoNegativo.values();
	}
	
	public SinaisClinicosExameMaleina[] getListaSinaisClinicosExameMaleina(){
		return SinaisClinicosExameMaleina.values();
	}
	
	public LocalizacaoCorrimentoNasal[] getListaLocalizacaoCorrimentoNasal(){
		return LocalizacaoCorrimentoNasal.values();
	}
	
	public TipoCorrimentoNasal[] getListaTipoCorrimentoNasal(){
		return TipoCorrimentoNasal.values();
	}
	
	public OrigemDoAnimal[] getListaOrigemDoAnimal(){
		return OrigemDoAnimal.values();
	}
	
	public DoencasRespiratorias[] getListaDoencasRespiratorias(){
		return DoencasRespiratorias.values();
	}
	
	public ManejoDosAnimais[] getListaManejoDosAnimais(){
		return ManejoDosAnimais.values();
	}

	public CategoriaAves[] getListaCategoriaAves(){
		return CategoriaAves.values();
	}
	
	public TipoDeAgrupamento[] getListaTipoDeAgrupamento(){
		return TipoDeAgrupamento.values();
	}
	
	public FaixaEtariaOuEspecie[] getListaFaixaEtariaFormSN(){
		return FaixaEtariaOuEspecie.getFaixaEtariaFormSN();
	}
	
	public FaixaEtariaOuEspecie[] getListaFaixaOutrosTiposDeAves(){
		return FaixaEtariaOuEspecie.getOutrasAves();
	}
	
	public UsoDeMedicacaoFormCOM[] getListaUsoDeMedicacaoFormCOMs(){
		return UsoDeMedicacaoFormCOM.values();
	}
	
	public PontosDeRisco[] getListaPontosDeRiscos(){
		return PontosDeRisco.values();
	}
	
	public TipoAlimentoSuideos[] getListaTipoAlimentoSuideos(){
		return TipoAlimentoSuideos.values();
	}
	
	public TipoOrigemSuideos[] getListaTipoOrigemSuideos(){
		return TipoOrigemSuideos.values();
	}
	
	public TipoDestinoSuideos[] getListaTipoDestinoSuideos(){
		return TipoDestinoSuideos.values();
	}
	
	public TipoDestinoCadaveres[] getListaTipoDestinoCadaveres(){
		return TipoDestinoCadaveres.values();
	}
	
	public TipoEstabelecimento[] getListaTipoEstabelecimento(){
		return TipoEstabelecimento.values();
	}
	
	public TipoEstabelecimento[] getListaTipoEstabelecimento_FormIN(){
		return TipoEstabelecimento.getListaTipoEstabelecimento_FormIN();
	}
	
	public TipoResponsavelColheitaDeAmostra[] getListaTipoResponsavelColheitaDeAmostra(){
		return TipoResponsavelColheitaDeAmostra.values();
	}
	
	public TipoAlteracaoSindromeNeurologica[] getListaTipoAlteracaoSindromeNeurologica(){
		return TipoAlteracaoSindromeNeurologica.values();
	}
	
	public TipoExploracaoSuinosSistemaIndustrial[] getListaTipoExploracaoSuinosSistemaIndustrial(){
		return TipoExploracaoSuinosSistemaIndustrial.values();
	}
	
	public TipoExploracaoAvicolaSistemaIndustrial[] getListaTipoExploracaoAvicolaSistemaIndustrial(){
		return TipoExploracaoAvicolaSistemaIndustrial.values();
	}
	
	public FinalidadeExploracaoSuinos[] getListaFinalidadeExploracaoSuinos(){
		return FinalidadeExploracaoSuinos.values();
	}

	public TipoExploracaoOutrosRuminantes[] getListaTipoExploracaoOutrosRuminantes(){
		return TipoExploracaoOutrosRuminantes.values();
	}
	
	public TipoDiagnostico[] getListaTipoDiagnostico_ProvavelConclusivo(){
		return TipoDiagnostico.getListTipoDiagnostico_ProvavelConclusivo();
	}
	
	public TipoDiagnostico[] getListaTipoDiagnostico_ConclusivoLaboratorial(){
		return TipoDiagnostico.getListTipoDiagnostico_ConclusivoLaboratorial();
	}
	
	public TipoTesteLaboratorial[] getListaTipoTesteLaboratorial(){
		return TipoTesteLaboratorial.values();
	}
	
	public AreaAtuacaoMedicoVeterinario[] getListaAreaAtuacaoMedicoVeterinario(){
		return AreaAtuacaoMedicoVeterinario.values();
	}
	
	public TipoUsuario[] getListaTipoUsuario(){
		return TipoUsuario.values();
	}
	
	public CondicaoMaterial[] getListaCondicaoMaterial(){
		return CondicaoMaterial.values();
	}
	
	public TipoSaida[] getListaTipoSaida(){
		return TipoSaida.values();
	}
	
	public AcompanhaNaoAcompanha[] getListaAcompanhaNaoAcompanha(){
		return AcompanhaNaoAcompanha.values();
	}
	
	public Mes[] getListaMes(){
		return Mes.values();
	}

	public TipoServicoDeInspecao[] getListaTipoServicoDeInspecao(){
		return TipoServicoDeInspecao.values();
	}
	
	public TipoDeMorteEmFrigorifico[] getListaTipoDeMorteEmFrigorifico(){
		return TipoDeMorteEmFrigorifico.values();
	}
	
	public MotivacaoParaAbateDeEmergenciaEmFrigorifico[] getListaMotivacaoParaAbateDeEmergenciaEmFrigorifico(){
		return MotivacaoParaAbateDeEmergenciaEmFrigorifico.values();
	}
	
	public SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel[] getListaSinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel(){
		return SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel.values();
	}
	
	public CategoriaAnimal[] getListaCategoriaAnimal(){
		return CategoriaAnimal.values();
	}
	
	public TipoEntregaRelatorioViagemFisico[] getListaTipoEntregaRelatorioViagemFisico() {
		return TipoEntregaRelatorioViagemFisico.values();
	}
	
	public RecursosDiaria[] getListaRecursosDiaria() {
		return RecursosDiaria.values();
	}
	
	public Atividade[] getListaAtividade() {
		return Atividade.values();
	}

	public int[] getListaAnosFrom(int inicio){
		int[] anos;
		LocalDate data = LocalDate.now();
		int anoAtual = data.getYear();
		
		anos = new int[anoAtual - inicio + 1];
		int i = 0;
		
		
		do {
			anos[i] = anoAtual;
			i++;
			anoAtual--;
		} while (anoAtual >= inicio);
		
		return anos;
	}
	
}
