package br.gov.mt.indea.prestacaodecontas.managedbeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.prestacaodecontas.entity.dto.PrestacaoContasDTO;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.PrestacaoDeContas;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.StatusViagem;
import br.gov.mt.indea.prestacaodecontas.service.LazyObjectDataModel;
import br.gov.mt.indea.prestacaodecontas.service.PrestacaoDeContasService;
@Named
@ViewScoped
@URLBeanName("relatoriosBean")
@URLMappings(mappings = {
		@URLMapping(id = "exibirRelatorioGeral", pattern = "/relatorios/geral", viewId = "/pages/relatorios/lista.jsf"),
		@URLMapping(id = "exibirRelatorioEmAberto", pattern = "/relatorios/emaberto", viewId = "/pages/relatorios/lista.jsf"),
		@URLMapping(id = "exibirRelatorioReprovadas", pattern = "/relatorios/reprovadas", viewId = "/pages/relatorios/lista.jsf")})
public class RelatoriosBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private PrestacaoContasDTO prestacaoContasDTO;
	
	@Inject
	private PrestacaoDeContas prestacaoContas;
	
	@Inject
	private PrestacaoDeContasService prestacaoService;
	
	private Boolean showDatatable;
	
	private LazyObjectDataModel<PrestacaoDeContas> listaPrestacaoContas;
	
	@PostConstruct
	public void init() {
		
	}
	
	public void limpar() {
		this.showDatatable = false;
		this.prestacaoContas = new PrestacaoDeContas();
		this.listaPrestacaoContas = null;
	}
	
	@URLAction(mappingId = "exibirRelatorioGeral", onPostback = false)
	public void exibirGeral() {
		limpar();
	}
	
	@URLAction(mappingId = "exibirRelatorioEmAberto", onPostback = false)
	public void exibirEmAberto() {
		limpar();
			this.prestacaoContasDTO = new PrestacaoContasDTO();
			this.prestacaoContasDTO.setStatus(StatusViagem.ABERTA);
		
	}
	
	@URLAction(mappingId = "exibirRelatorioReprovadas", onPostback = false)
	public void exibirReprovadas() {
		limpar();
			this.prestacaoContasDTO = new PrestacaoContasDTO();
			this.prestacaoContasDTO.setStatus(StatusViagem.REPROVADA);
	}
	
	public boolean statusViagem(StatusViagem sts) {
		if(this.prestacaoContasDTO.getStatus()==sts) {
			return true;
		}
		return false;
	}
	
	
	public void exibir() {
			this.showDatatable = true;
			this.buscarPrestacao();
	}

	public void buscarPrestacao() {
		this.listaPrestacaoContas = new LazyObjectDataModel<PrestacaoDeContas>(this.prestacaoService, this.prestacaoContasDTO);
	}
	
	public void reset() {
		this.prestacaoContasDTO.setDtFim(null);
		this.prestacaoContasDTO.setDtFimOut(null);
		this.prestacaoContasDTO.setDtInicio(null);
		this.prestacaoContasDTO.setDtInicioOut(null);
		this.prestacaoContasDTO.setDtReprovacao(null);
		this.prestacaoContasDTO.setDtReprovacaoOut(null);
		RequestContext.getCurrentInstance().reset("formListaPrestacoes");
	}

	public PrestacaoContasDTO getPrestacaoContasDTO() {
		return prestacaoContasDTO;
	}

	public void setPrestacaoContasDTO(PrestacaoContasDTO prestacaoContasDTO) {
		this.prestacaoContasDTO = prestacaoContasDTO;
	}

	public PrestacaoDeContas getPrestacaoContas() {
		return prestacaoContas;
	}

	public void setPrestacaoContas(PrestacaoDeContas prestacaoContas) {
		this.prestacaoContas = prestacaoContas;
	}

	public PrestacaoDeContasService getPrestacaoService() {
		return prestacaoService;
	}

	public void setPrestacaoService(PrestacaoDeContasService prestacaoService) {
		this.prestacaoService = prestacaoService;
	}

	public LazyObjectDataModel<PrestacaoDeContas> getListaPrestacaoContas() {
		return listaPrestacaoContas;
	}

	public void setListaPrestacaoContas(LazyObjectDataModel<PrestacaoDeContas> listaPrestacaoContas) {
		this.listaPrestacaoContas = listaPrestacaoContas;
	}

	public Boolean getShowDatatable() {
		return showDatatable;
	}

	public void setShowDatatable(Boolean showDatatable) {
		this.showDatatable = showDatatable;
	}
}
