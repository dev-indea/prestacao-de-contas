package br.gov.mt.indea.prestacaodecontas.entity.view;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class RelatorioVigilanciaVeterinariaGeral implements Serializable{

	private static final long serialVersionUID = 1667324840277718104L;
	
	private String urs;
	
	private String ule;
	
	private String numero;
	
	private Date data_vigilancia;
	
	private Date data_cadastro;
	
	private String responsavel;
	
	private String propriedade;
	
	private String codigo_propriedade;
	
	private String proprietario;
	
	private Integer latitude_grau;
	
	private Integer latitude_minuto;
	
	private Float latitude_segundo;
	
	private Integer longitude_grau;
	
	private Integer longitude_minuto;
	
	private Float longitude_segundo;
	
	private String principais_motivos_visita;
	
	private String houve_invest_alim_rum;
	
	private String ha_pontos_risco;
	
	private String houve_invest_bov_bub;
	
	private String houve_invest_suideos;
	
	private String houve_invest_aves;
	
	private String houve_invest_outra_esp;
	
	private String houve_invest_epidem;

	private String tipo_exploracao_outros_ruminantes;

	private String sistema_criacao_ruminantes;

	private Integer quantidade_bovinos;
	
	private Integer quantidade_caprinos;
	
	private Integer quantidade_ovinos;
	
	private Integer quantidade_outra_esp_rumin;
	
	private String idade_rumin_alim_com_racao;
	
	private Integer quantidade_bovinos_exp;
	
	private Integer quantidade_caprinos_exp;
	
	private Integer quantidade_ovinos_exp;
	
	private String tipo_alimentacao_ruminantes;
	
	private String tipo_exp_avicola_sist_indust;
	
	private String epoca_ano_suplementacao;
	
	private String presenca_cama_aviario;
	
	private String observacao_cama_aviario;
	
	private String cama_aviario_alimentacao_rum;
	
	private String tipo_exp_suino_sist_indust;
	
	private String piscicultura_aliment_base_racao;
	
	private String contaminacao_racao_peixe_rumin;
	
	private String colheita_amostra_alimento_rum;
	
	private String tipo_fiscalizacao;
	
	private Integer numero_denuncia;
	
	private String descricao_racoes;
	
	private String outras_observacoes;

	private String tipos_risco;
	
	private Integer quantidade_ruminantes_inspec;

	private Integer quantidade_bovinos_vist;
	
	private Integer quantidade_leiteiras;
	
	private Integer quantidade_reprodutoras;
	
	private Integer porcentagem_natalidade;

	private Integer quantidade_suideos_vist;

	private Integer quantidade_suideos_inspec;
			
	private String presenca_suinos_asselvajados;
	
	private String contato_suinos_domest_e_assel;
	
	private String prop_prox_comunidade_carente;
	
	private String prop_prox_quarentenario;
	
	private String prop_suideos_criados_extensiv;
	
	private String prop_com_lavagem_de_terceiros;
	
	private String dono_tem_prop_em_area_endem;
	
	private String finalidade_explocarao_suinos;
	
	private String tipo_alimento_suideos;
	
	private String tipo_origem_suideos;
	
	private String tipo_destino_suideos;
	
	private String tipo_destino_cadaveres;
	
	private String hipertermia;
	
	private Integer porc_hipertermia;
	
	private String incoordenacao_motora;
	
	private Integer porc_incoordenacao_motora;
	
	private String extremidades_cianoticas;
	
	private Integer porc_extremidades_cianoticas;
	
	private String vomitos;
	
	private Integer porc_vomitos;
	
	private String diarreia;
	
	private Integer porc_diarreia;
	
	private String anorexia;
	
	private Integer porc_anorexia;
	
	private String abortos;
	
	private Integer porc_abortos;
	
	private String leitoes_natimortos;
	
	private Integer porc_leitoes_natimortos;
	
	private String agrupamentos_nas_pocilgas;
	
	private Integer porc_agrupamentos_nas_pocilgas;
	
	private String hemorragias_e_petequias;
	
	private Integer porc_hemorragias_e_petequias;
	
	private String cresc_retardado_de_leitoes;
	
	private Integer porc_cresc_retardado_de_leitoes;
	
	private String mortalidade_mensal;
	
	private Integer porc_mortalidade_mensal;
		
	private BigInteger qtdade_aves_inspecionados;
	
	private BigInteger qtdade_aves_vistoriados;

	private Integer ovinos_inspecionados;
	
	private Integer ovinos_vistoriados;
	
	private Integer caprinos_inspecionados;
	
	private Integer caprinos_vistoriados;
	
	private Integer equideos_inspecionados;
	
	private Integer equideos_vistoriados;
	
	private Integer peixes_inspecionados;
	
	private Integer peixes_vistoriados;

	private String doencas;

	private String historico_sugadura_morcegos;
	
	private String observacao;
	
	private String emissao_form_in;

	public String getUrs() {
		return urs;
	}

	public void setUrs(String urs) {
		this.urs = urs;
	}

	public String getUle() {
		return ule;
	}

	public void setUle(String ule) {
		this.ule = ule;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getData_vigilancia() {
		return data_vigilancia;
	}

	public void setData_vigilancia(Date data_vigilancia) {
		this.data_vigilancia = data_vigilancia;
	}

	public Date getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(Date data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public String getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}

	public String getCodigo_propriedade() {
		return codigo_propriedade;
	}

	public void setCodigo_propriedade(String codigo_propriedade) {
		this.codigo_propriedade = codigo_propriedade;
	}

	public String getProprietario() {
		return proprietario;
	}

	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

	public Integer getLatitude_grau() {
		return latitude_grau;
	}

	public void setLatitude_grau(Integer latitude_grau) {
		this.latitude_grau = latitude_grau;
	}

	public Integer getLatitude_minuto() {
		return latitude_minuto;
	}

	public void setLatitude_minuto(Integer latitude_minuto) {
		this.latitude_minuto = latitude_minuto;
	}

	public Float getLatitude_segundo() {
		return latitude_segundo;
	}

	public void setLatitude_segundo(Float latitude_segundo) {
		this.latitude_segundo = latitude_segundo;
	}

	public Integer getLongitude_grau() {
		return longitude_grau;
	}

	public void setLongitude_grau(Integer longitude_grau) {
		this.longitude_grau = longitude_grau;
	}

	public Integer getLongitude_minuto() {
		return longitude_minuto;
	}

	public void setLongitude_minuto(Integer longitude_minuto) {
		this.longitude_minuto = longitude_minuto;
	}

	public Float getLongitude_segundo() {
		return longitude_segundo;
	}

	public void setLongitude_segundo(Float longitude_segundo) {
		this.longitude_segundo = longitude_segundo;
	}

	public String getPrincipais_motivos_visita() {
		return principais_motivos_visita;
	}

	public void setPrincipais_motivos_visita(String principais_motivos_visita) {
		this.principais_motivos_visita = principais_motivos_visita;
	}

	public String getTipo_exploracao_outros_ruminantes() {
		if (tipo_exploracao_outros_ruminantes != null)
			tipo_exploracao_outros_ruminantes = tipo_exploracao_outros_ruminantes.replace("_", " ");
		return tipo_exploracao_outros_ruminantes;
	}

	public void setTipo_exploracao_outros_ruminantes(String tipo_exploracao_outros_ruminantes) {
		this.tipo_exploracao_outros_ruminantes = tipo_exploracao_outros_ruminantes;
	}

	public String getSistema_criacao_ruminantes() {
		if (sistema_criacao_ruminantes != null)
			sistema_criacao_ruminantes = sistema_criacao_ruminantes.replace("_", " ");
		return sistema_criacao_ruminantes;
	}

	public void setSistema_criacao_ruminantes(String sistema_criacao_ruminantes) {
		this.sistema_criacao_ruminantes = sistema_criacao_ruminantes;
	}

	public Integer getQuantidade_bovinos() {
		return quantidade_bovinos;
	}

	public void setQuantidade_bovinos(Integer quantidade_bovinos) {
		this.quantidade_bovinos = quantidade_bovinos;
	}

	public Integer getQuantidade_caprinos() {
		return quantidade_caprinos;
	}

	public void setQuantidade_caprinos(Integer quantidade_caprinos) {
		this.quantidade_caprinos = quantidade_caprinos;
	}

	public Integer getQuantidade_ovinos() {
		return quantidade_ovinos;
	}

	public void setQuantidade_ovinos(Integer quantidade_ovinos) {
		this.quantidade_ovinos = quantidade_ovinos;
	}

	public Integer getQuantidade_outra_esp_rumin() {
		return quantidade_outra_esp_rumin;
	}

	public void setQuantidade_outra_esp_rumin(Integer quantidade_outra_esp_rumin) {
		this.quantidade_outra_esp_rumin = quantidade_outra_esp_rumin;
	}

	public String getIdade_rumin_alim_com_racao() {
		return idade_rumin_alim_com_racao;
	}

	public void setIdade_rumin_alim_com_racao(String idade_rumin_alim_com_racao) {
		this.idade_rumin_alim_com_racao = idade_rumin_alim_com_racao;
	}

	public Integer getQuantidade_bovinos_exp() {
		return quantidade_bovinos_exp;
	}

	public void setQuantidade_bovinos_exp(Integer quantidade_bovinos_exp) {
		this.quantidade_bovinos_exp = quantidade_bovinos_exp;
	}

	public Integer getQuantidade_caprinos_exp() {
		return quantidade_caprinos_exp;
	}

	public void setQuantidade_caprinos_exp(Integer quantidade_caprinos_exp) {
		this.quantidade_caprinos_exp = quantidade_caprinos_exp;
	}

	public Integer getQuantidade_ovinos_exp() {
		return quantidade_ovinos_exp;
	}

	public void setQuantidade_ovinos_exp(Integer quantidade_ovinos_exp) {
		this.quantidade_ovinos_exp = quantidade_ovinos_exp;
	}

	public String getTipo_alimentacao_ruminantes() {
		return tipo_alimentacao_ruminantes;
	}

	public void setTipo_alimentacao_ruminantes(String tipo_alimentacao_ruminantes) {
		this.tipo_alimentacao_ruminantes = tipo_alimentacao_ruminantes;
	}

	public String getTipo_exp_avicola_sist_indust() {
		if (tipo_exp_avicola_sist_indust != null)
			tipo_exp_avicola_sist_indust = tipo_exp_avicola_sist_indust.replace("_", " ");
		return tipo_exp_avicola_sist_indust;
	}

	public void setTipo_exp_avicola_sist_indust(String tipo_exp_avicola_sist_indust) {
		this.tipo_exp_avicola_sist_indust = tipo_exp_avicola_sist_indust;
	}

	public String getEpoca_ano_suplementacao() {
		return epoca_ano_suplementacao;
	}

	public void setEpoca_ano_suplementacao(String epoca_ano_suplementacao) {
		this.epoca_ano_suplementacao = epoca_ano_suplementacao;
	}

	public String getPresenca_cama_aviario() {
		return presenca_cama_aviario;
	}

	public void setPresenca_cama_aviario(String presenca_cama_aviario) {
		this.presenca_cama_aviario = presenca_cama_aviario;
	}

	public String getObservacao_cama_aviario() {
		return observacao_cama_aviario;
	}

	public void setObservacao_cama_aviario(String observacao_cama_aviario) {
		this.observacao_cama_aviario = observacao_cama_aviario;
	}

	public String getCama_aviario_alimentacao_rum() {
		return cama_aviario_alimentacao_rum;
	}

	public void setCama_aviario_alimentacao_rum(String cama_aviario_alimentacao_rum) {
		this.cama_aviario_alimentacao_rum = cama_aviario_alimentacao_rum;
	}

	public String getTipo_exp_suino_sist_indust() {
		if (tipo_exp_suino_sist_indust != null)
			tipo_exp_suino_sist_indust = tipo_exp_suino_sist_indust.replace("_", " ");
		return tipo_exp_suino_sist_indust;
	}

	public void setTipo_exp_suino_sist_indust(String tipo_exp_suino_sist_indust) {
		this.tipo_exp_suino_sist_indust = tipo_exp_suino_sist_indust;
	}

	public String getPiscicultura_aliment_base_racao() {
		return piscicultura_aliment_base_racao;
	}

	public void setPiscicultura_aliment_base_racao(String piscicultura_aliment_base_racao) {
		this.piscicultura_aliment_base_racao = piscicultura_aliment_base_racao;
	}

	public String getContaminacao_racao_peixe_rumin() {
		return contaminacao_racao_peixe_rumin;
	}

	public void setContaminacao_racao_peixe_rumin(String contaminacao_racao_peixe_rumin) {
		this.contaminacao_racao_peixe_rumin = contaminacao_racao_peixe_rumin;
	}

	public String getColheita_amostra_alimento_rum() {
		return colheita_amostra_alimento_rum;
	}

	public void setColheita_amostra_alimento_rum(String colheita_amostra_alimento_rum) {
		this.colheita_amostra_alimento_rum = colheita_amostra_alimento_rum;
	}

	public String getTipo_fiscalizacao() {
		return tipo_fiscalizacao;
	}

	public void setTipo_fiscalizacao(String tipo_fiscalizacao) {
		this.tipo_fiscalizacao = tipo_fiscalizacao;
	}

	public Integer getNumero_denuncia() {
		return numero_denuncia;
	}

	public void setNumero_denuncia(Integer numero_denuncia) {
		this.numero_denuncia = numero_denuncia;
	}

	public String getDescricao_racoes() {
		return descricao_racoes;
	}

	public void setDescricao_racoes(String descricao_racoes) {
		this.descricao_racoes = descricao_racoes;
	}

	public String getOutras_observacoes() {
		return outras_observacoes;
	}

	public void setOutras_observacoes(String outras_observacoes) {
		this.outras_observacoes = outras_observacoes;
	}

	public String getTipos_risco() {
		if (tipos_risco != null)
			tipos_risco = tipos_risco.replace("_", " ");
		return tipos_risco;
	}

	public void setTipos_risco(String tipos_risco) {
		this.tipos_risco = tipos_risco;
	}

	public Integer getQuantidade_ruminantes_inspec() {
		return quantidade_ruminantes_inspec;
	}

	public void setQuantidade_ruminantes_inspec(Integer quantidade_ruminantes_inspec) {
		this.quantidade_ruminantes_inspec = quantidade_ruminantes_inspec;
	}

	public Integer getQuantidade_bovinos_vist() {
		return quantidade_bovinos_vist;
	}

	public void setQuantidade_bovinos_vist(Integer quantidade_bovinos_vist) {
		this.quantidade_bovinos_vist = quantidade_bovinos_vist;
	}

	public Integer getQuantidade_leiteiras() {
		return quantidade_leiteiras;
	}

	public void setQuantidade_leiteiras(Integer quantidade_leiteiras) {
		this.quantidade_leiteiras = quantidade_leiteiras;
	}

	public Integer getQuantidade_reprodutoras() {
		return quantidade_reprodutoras;
	}

	public void setQuantidade_reprodutoras(Integer quantidade_reprodutoras) {
		this.quantidade_reprodutoras = quantidade_reprodutoras;
	}

	public Integer getPorcentagem_natalidade() {
		return porcentagem_natalidade;
	}

	public void setPorcentagem_natalidade(Integer porcentagem_natalidade) {
		this.porcentagem_natalidade = porcentagem_natalidade;
	}

	public Integer getQuantidade_suideos_vist() {
		return quantidade_suideos_vist;
	}

	public void setQuantidade_suideos_vist(Integer quantidade_suideos_vist) {
		this.quantidade_suideos_vist = quantidade_suideos_vist;
	}

	public Integer getQuantidade_suideos_inspec() {
		return quantidade_suideos_inspec;
	}

	public void setQuantidade_suideos_inspec(Integer quantidade_suideos_inspec) {
		this.quantidade_suideos_inspec = quantidade_suideos_inspec;
	}

	public String getPresenca_suinos_asselvajados() {
		return presenca_suinos_asselvajados;
	}

	public void setPresenca_suinos_asselvajados(String presenca_suinos_asselvajados) {
		this.presenca_suinos_asselvajados = presenca_suinos_asselvajados;
	}

	public String getContato_suinos_domest_e_assel() {
		return contato_suinos_domest_e_assel;
	}

	public void setContato_suinos_domest_e_assel(String contato_suinos_domest_e_assel) {
		this.contato_suinos_domest_e_assel = contato_suinos_domest_e_assel;
	}

	public String getProp_prox_comunidade_carente() {
		return prop_prox_comunidade_carente;
	}

	public void setProp_prox_comunidade_carente(String prop_prox_comunidade_carente) {
		this.prop_prox_comunidade_carente = prop_prox_comunidade_carente;
	}

	public String getProp_prox_quarentenario() {
		return prop_prox_quarentenario;
	}

	public void setProp_prox_quarentenario(String prop_prox_quarentenario) {
		this.prop_prox_quarentenario = prop_prox_quarentenario;
	}

	public String getProp_suideos_criados_extensiv() {
		return prop_suideos_criados_extensiv;
	}

	public void setProp_suideos_criados_extensiv(String prop_suideos_criados_extensiv) {
		this.prop_suideos_criados_extensiv = prop_suideos_criados_extensiv;
	}

	public String getProp_com_lavagem_de_terceiros() {
		return prop_com_lavagem_de_terceiros;
	}

	public void setProp_com_lavagem_de_terceiros(String prop_com_lavagem_de_terceiros) {
		this.prop_com_lavagem_de_terceiros = prop_com_lavagem_de_terceiros;
	}

	public String getDono_tem_prop_em_area_endem() {
		return dono_tem_prop_em_area_endem;
	}

	public void setDono_tem_prop_em_area_endem(String dono_tem_prop_em_area_endem) {
		this.dono_tem_prop_em_area_endem = dono_tem_prop_em_area_endem;
	}

	public String getFinalidade_explocarao_suinos() {
		if (finalidade_explocarao_suinos != null)
			finalidade_explocarao_suinos = finalidade_explocarao_suinos.replace("_", " ");
		return finalidade_explocarao_suinos;
	}

	public void setFinalidade_explocarao_suinos(String finalidade_explocarao_suinos) {
		this.finalidade_explocarao_suinos = finalidade_explocarao_suinos;
	}

	public String getTipo_alimento_suideos() {
		return tipo_alimento_suideos;
	}

	public void setTipo_alimento_suideos(String tipo_alimento_suideos) {
		this.tipo_alimento_suideos = tipo_alimento_suideos;
	}

	public String getTipo_origem_suideos() {
		if (tipo_origem_suideos != null)
			tipo_origem_suideos = tipo_origem_suideos.replace("_", " ");
		return tipo_origem_suideos;
	}

	public void setTipo_origem_suideos(String tipo_origem_suideos) {
		this.tipo_origem_suideos = tipo_origem_suideos;
	}

	public String getTipo_destino_suideos() {
		if (tipo_destino_suideos != null)
			tipo_destino_suideos = tipo_destino_suideos.replace("_", " ");
		return tipo_destino_suideos;
	}

	public void setTipo_destino_suideos(String tipo_destino_suideos) {
		this.tipo_destino_suideos = tipo_destino_suideos;
	}

	public String getTipo_destino_cadaveres() {
		if (tipo_destino_cadaveres != null)
			tipo_destino_cadaveres = tipo_destino_cadaveres.replace("_", " ");
		return tipo_destino_cadaveres;
	}

	public void setTipo_destino_cadaveres(String tipo_destino_cadaveres) {
		this.tipo_destino_cadaveres = tipo_destino_cadaveres;
	}

	public String getHipertermia() {
		return hipertermia;
	}

	public void setHipertermia(String hipertermia) {
		this.hipertermia = hipertermia;
	}

	public Integer getPorc_hipertermia() {
		return porc_hipertermia;
	}

	public void setPorc_hipertermia(Integer porc_hipertermia) {
		this.porc_hipertermia = porc_hipertermia;
	}

	public String getIncoordenacao_motora() {
		return incoordenacao_motora;
	}

	public void setIncoordenacao_motora(String incoordenacao_motora) {
		this.incoordenacao_motora = incoordenacao_motora;
	}

	public Integer getPorc_incoordenacao_motora() {
		return porc_incoordenacao_motora;
	}

	public void setPorc_incoordenacao_motora(Integer porc_incoordenacao_motora) {
		this.porc_incoordenacao_motora = porc_incoordenacao_motora;
	}

	public String getExtremidades_cianoticas() {
		return extremidades_cianoticas;
	}

	public void setExtremidades_cianoticas(String extremidades_cianoticas) {
		this.extremidades_cianoticas = extremidades_cianoticas;
	}

	public Integer getPorc_extremidades_cianoticas() {
		return porc_extremidades_cianoticas;
	}

	public void setPorc_extremidades_cianoticas(Integer porc_extremidades_cianoticas) {
		this.porc_extremidades_cianoticas = porc_extremidades_cianoticas;
	}

	public String getVomitos() {
		return vomitos;
	}

	public void setVomitos(String vomitos) {
		this.vomitos = vomitos;
	}

	public Integer getPorc_vomitos() {
		return porc_vomitos;
	}

	public void setPorc_vomitos(Integer porc_vomitos) {
		this.porc_vomitos = porc_vomitos;
	}

	public String getDiarreia() {
		return diarreia;
	}

	public void setDiarreia(String diarreia) {
		this.diarreia = diarreia;
	}

	public Integer getPorc_diarreia() {
		return porc_diarreia;
	}

	public void setPorc_diarreia(Integer porc_diarreia) {
		this.porc_diarreia = porc_diarreia;
	}

	public String getAnorexia() {
		return anorexia;
	}

	public void setAnorexia(String anorexia) {
		this.anorexia = anorexia;
	}

	public Integer getPorc_anorexia() {
		return porc_anorexia;
	}

	public void setPorc_anorexia(Integer porc_anorexia) {
		this.porc_anorexia = porc_anorexia;
	}

	public String getAbortos() {
		return abortos;
	}

	public void setAbortos(String abortos) {
		this.abortos = abortos;
	}

	public Integer getPorc_abortos() {
		return porc_abortos;
	}

	public void setPorc_abortos(Integer porc_abortos) {
		this.porc_abortos = porc_abortos;
	}

	public String getLeitoes_natimortos() {
		return leitoes_natimortos;
	}

	public void setLeitoes_natimortos(String leitoes_natimortos) {
		this.leitoes_natimortos = leitoes_natimortos;
	}

	public Integer getPorc_leitoes_natimortos() {
		return porc_leitoes_natimortos;
	}

	public void setPorc_leitoes_natimortos(Integer porc_leitoes_natimortos) {
		this.porc_leitoes_natimortos = porc_leitoes_natimortos;
	}

	public String getAgrupamentos_nas_pocilgas() {
		return agrupamentos_nas_pocilgas;
	}

	public void setAgrupamentos_nas_pocilgas(String agrupamentos_nas_pocilgas) {
		this.agrupamentos_nas_pocilgas = agrupamentos_nas_pocilgas;
	}

	public Integer getPorc_agrupamentos_nas_pocilgas() {
		return porc_agrupamentos_nas_pocilgas;
	}

	public void setPorc_agrupamentos_nas_pocilgas(Integer porc_agrupamentos_nas_pocilgas) {
		this.porc_agrupamentos_nas_pocilgas = porc_agrupamentos_nas_pocilgas;
	}

	public String getHemorragias_e_petequias() {
		return hemorragias_e_petequias;
	}

	public void setHemorragias_e_petequias(String hemorragias_e_petequias) {
		this.hemorragias_e_petequias = hemorragias_e_petequias;
	}

	public Integer getPorc_hemorragias_e_petequias() {
		return porc_hemorragias_e_petequias;
	}

	public void setPorc_hemorragias_e_petequias(Integer porc_hemorragias_e_petequias) {
		this.porc_hemorragias_e_petequias = porc_hemorragias_e_petequias;
	}

	public String getCresc_retardado_de_leitoes() {
		return cresc_retardado_de_leitoes;
	}

	public void setCresc_retardado_de_leitoes(String cresc_retardado_de_leitoes) {
		this.cresc_retardado_de_leitoes = cresc_retardado_de_leitoes;
	}

	public Integer getPorc_cresc_retardado_de_leitoes() {
		return porc_cresc_retardado_de_leitoes;
	}

	public void setPorc_cresc_retardado_de_leitoes(Integer porc_cresc_retardado_de_leitoes) {
		this.porc_cresc_retardado_de_leitoes = porc_cresc_retardado_de_leitoes;
	}

	public String getMortalidade_mensal() {
		return mortalidade_mensal;
	}

	public void setMortalidade_mensal(String mortalidade_mensal) {
		this.mortalidade_mensal = mortalidade_mensal;
	}

	public Integer getPorc_mortalidade_mensal() {
		return porc_mortalidade_mensal;
	}

	public void setPorc_mortalidade_mensal(Integer porc_mortalidade_mensal) {
		this.porc_mortalidade_mensal = porc_mortalidade_mensal;
	}

	public BigInteger getQtdade_aves_inspecionados() {
		return qtdade_aves_inspecionados;
	}

	public void setQtdade_aves_inspecionados(BigInteger qtdade_aves_inspecionados) {
		this.qtdade_aves_inspecionados = qtdade_aves_inspecionados;
	}

	public BigInteger getQtdade_aves_vistoriados() {
		return qtdade_aves_vistoriados;
	}

	public void setQtdade_aves_vistoriados(BigInteger qtdade_aves_vistoriados) {
		this.qtdade_aves_vistoriados = qtdade_aves_vistoriados;
	}

	public Integer getOvinos_inspecionados() {
		return ovinos_inspecionados;
	}

	public void setOvinos_inspecionados(Integer ovinos_inspecionados) {
		this.ovinos_inspecionados = ovinos_inspecionados;
	}

	public Integer getOvinos_vistoriados() {
		return ovinos_vistoriados;
	}

	public void setOvinos_vistoriados(Integer ovinos_vistoriados) {
		this.ovinos_vistoriados = ovinos_vistoriados;
	}

	public Integer getCaprinos_inspecionados() {
		return caprinos_inspecionados;
	}

	public void setCaprinos_inspecionados(Integer caprinos_inspecionados) {
		this.caprinos_inspecionados = caprinos_inspecionados;
	}

	public Integer getCaprinos_vistoriados() {
		return caprinos_vistoriados;
	}

	public void setCaprinos_vistoriados(Integer caprinos_vistoriados) {
		this.caprinos_vistoriados = caprinos_vistoriados;
	}

	public Integer getEquideos_inspecionados() {
		return equideos_inspecionados;
	}

	public void setEquideos_inspecionados(Integer equideos_inspecionados) {
		this.equideos_inspecionados = equideos_inspecionados;
	}

	public Integer getEquideos_vistoriados() {
		return equideos_vistoriados;
	}

	public void setEquideos_vistoriados(Integer equideos_vistoriados) {
		this.equideos_vistoriados = equideos_vistoriados;
	}

	public Integer getPeixes_inspecionados() {
		return peixes_inspecionados;
	}

	public void setPeixes_inspecionados(Integer peixes_inspecionados) {
		this.peixes_inspecionados = peixes_inspecionados;
	}

	public Integer getPeixes_vistoriados() {
		return peixes_vistoriados;
	}

	public void setPeixes_vistoriados(Integer peixes_vistoriados) {
		this.peixes_vistoriados = peixes_vistoriados;
	}

	public String getDoencas() {
		return doencas;
	}

	public void setDoencas(String doencas) {
		this.doencas = doencas;
	}

	public String getHistorico_sugadura_morcegos() {
		return historico_sugadura_morcegos;
	}

	public void setHistorico_sugadura_morcegos(String historico_sugadura_morcegos) {
		this.historico_sugadura_morcegos = historico_sugadura_morcegos;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getEmissao_form_in() {
		return emissao_form_in;
	}

	public void setEmissao_form_in(String emissao_form_in) {
		this.emissao_form_in = emissao_form_in;
	}

	public String getHouve_invest_alim_rum() {
		return houve_invest_alim_rum;
	}

	public void setHouve_invest_alim_rum(String houve_invest_alim_rum) {
		this.houve_invest_alim_rum = houve_invest_alim_rum;
	}

	public String getHa_pontos_risco() {
		return ha_pontos_risco;
	}

	public void setHa_pontos_risco(String ha_pontos_risco) {
		this.ha_pontos_risco = ha_pontos_risco;
	}

	public String getHouve_invest_bov_bub() {
		return houve_invest_bov_bub;
	}

	public void setHouve_invest_bov_bub(String houve_invest_bov_bub) {
		this.houve_invest_bov_bub = houve_invest_bov_bub;
	}

	public String getHouve_invest_suideos() {
		return houve_invest_suideos;
	}

	public void setHouve_invest_suideos(String houve_invest_suideos) {
		this.houve_invest_suideos = houve_invest_suideos;
	}

	public String getHouve_invest_aves() {
		return houve_invest_aves;
	}

	public void setHouve_invest_aves(String houve_invest_aves) {
		this.houve_invest_aves = houve_invest_aves;
	}

	public String getHouve_invest_outra_esp() {
		return houve_invest_outra_esp;
	}

	public void setHouve_invest_outra_esp(String houve_invest_outra_esp) {
		this.houve_invest_outra_esp = houve_invest_outra_esp;
	}

	public String getHouve_invest_epidem() {
		return houve_invest_epidem;
	}

	public void setHouve_invest_epidem(String houve_invest_epidem) {
		this.houve_invest_epidem = houve_invest_epidem;
	}


}
