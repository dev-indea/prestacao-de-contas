package br.gov.mt.indea.prestacaodecontas.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.validation.ValidationException;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.prestacaodecontas.entity.dbindea.Servidor;
import br.gov.mt.indea.prestacaodecontas.entity.dto.AbstractDTO;
import br.gov.mt.indea.prestacaodecontas.entity.dto.PrestacaoContasDTO;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.HistoricoViagem;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.PrestacaoDeContas;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.TipoEventoViagem;
import br.gov.mt.indea.prestacaodecontas.managedbeans.ListsManagedBean;
import br.gov.mt.indea.prestacaodecontas.util.Email;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PrestacaoDeContasService extends PaginableService<PrestacaoDeContas, Long> {

	private static final Logger log = LoggerFactory.getLogger(PrestacaoDeContas.class);
	
	protected PrestacaoDeContasService() {
		super(PrestacaoDeContas.class);
	}
	
	@Inject
	ServidorService servidorService;
	
	@Inject
	HistoricoViagemService historicoViagemService;
	
	@Inject
	private ListsManagedBean listsManagedBean;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long countAll(AbstractDTO dto){
    	StringBuilder sql = new StringBuilder();
		sql.append("select count(entity) ")
		   .append( gerarSqlDatatable(dto).toString() );
		
		Query query = gerarQueryDatatable(dto, sql);
    	
		return (Long) query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<PrestacaoDeContas> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select entity")
		   .append(gerarSqlDatatable(dto));
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by entity." + sortField + " " + sortOrder);

		Query query = gerarQueryDatatable(dto, sql);

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<PrestacaoDeContas> lista = query.list();
		
		if(!lista.isEmpty()) {
			for(PrestacaoDeContas pdc:lista) {
				if(dto != null) {
					PrestacaoContasDTO prestacaoContasDTO = (PrestacaoContasDTO) dto;
					if(prestacaoContasDTO.getServidor()!=null) {
						pdc.getServidor().setServidorIndeaWeb(prestacaoContasDTO.getServidor());
					}else {
						if (pdc.getServidor().isTipoServidorINDEA())
							pdc.getServidor().setServidorIndeaWeb(servidorService.findByIdFetchAll(Long.valueOf(pdc.getServidor().getId())));
					}
				}
				
				if (pdc.getIdUnidadeAutorizada() != null)
					pdc.setUnidadeAutorizada(listsManagedBean.getUnidadeById(Long.valueOf(pdc.getIdUnidadeAutorizada())));
				if (pdc.getIdUnidadeOrigem() != null)
					pdc.setUnidadeOrigem(listsManagedBean.getUnidadeById(Long.valueOf(pdc.getIdUnidadeOrigem())));
				if (pdc.getIdUnidadeSolicitante() != null)
					pdc.setUnidadeSolicitante(listsManagedBean.getUnidadeById(Long.valueOf(pdc.getIdUnidadeSolicitante())));
			}
		}
		
		return lista;
    }
	
	private Query gerarQueryDatatable(AbstractDTO dto, StringBuilder sql) {
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			PrestacaoContasDTO prestacaoContasDTO = (PrestacaoContasDTO) dto;
			Calendar cal = Calendar.getInstance();
			if(prestacaoContasDTO.getProtocolo() != null && !prestacaoContasDTO.getProtocolo().equals("")) 
				query.setString("protocolo", prestacaoContasDTO.getProtocolo());			
			if(prestacaoContasDTO.getOs() != null && !prestacaoContasDTO.getOs().equals("")) 
				query.setString("os", prestacaoContasDTO.getOs());	
			if(prestacaoContasDTO.getNrBoletimUrs() != null && !prestacaoContasDTO.getNrBoletimUrs().equals(""))
				query.setString("nrBoletimUrs", prestacaoContasDTO.getNrBoletimUrs());
			if(prestacaoContasDTO.getEntregueEm() != null ) {
				query.setDate("entregueEm", prestacaoContasDTO.getEntregueEm()!=null?prestacaoContasDTO.getEntregueEm():cal.getTime());
				query.setDate("entregueEmOut", prestacaoContasDTO.getEntregueEmOut()!=null?prestacaoContasDTO.getEntregueEmOut():cal.getTime());
			}
			if(prestacaoContasDTO.getDtInicio() != null ) { 
				query.setDate("dtInicio", prestacaoContasDTO.getDtInicio()!=null?prestacaoContasDTO.getDtInicio():cal.getTime());
				query.setDate("dtInicioOut", prestacaoContasDTO.getDtInicioOut()!=null?prestacaoContasDTO.getDtInicioOut():cal.getTime());
			}
			if(prestacaoContasDTO.getDtFim() != null ) {
				query.setDate("dtFim", prestacaoContasDTO.getDtFim()!=null?prestacaoContasDTO.getDtFim():cal.getTime());
				query.setDate("dtFimOut", prestacaoContasDTO.getDtFimOut()!=null?prestacaoContasDTO.getDtFimOut():cal.getTime());
			}
			if(prestacaoContasDTO.getDtReprovacao() != null ) {
				query.setDate("dtReprovacao", prestacaoContasDTO.getDtReprovacao()!=null?prestacaoContasDTO.getDtReprovacao():cal.getTime());
				query.setDate("dtReprovacaoOut", prestacaoContasDTO.getDtReprovacaoOut()!=null?prestacaoContasDTO.getDtReprovacaoOut():cal.getTime());
			}
			
			if(prestacaoContasDTO.getServidor() != null ) {
				query.setString("nome", prestacaoContasDTO.getServidor().getNome());
			}if(prestacaoContasDTO.getUnidadeOrigemEntity() != null)
				query.setString("idUnidadeOrigem", prestacaoContasDTO.getUnidadeOrigemEntity().getId().toString());
			if(prestacaoContasDTO.getUnidadeSolicitanteEntity() != null)
				query.setString("idUnidadeSolicitante", prestacaoContasDTO.getUnidadeSolicitanteEntity().getId().toString());
			if(prestacaoContasDTO.getUnidadeAutorizadaEntity() != null)
				query.setString("idUnidadeAutorizada", prestacaoContasDTO.getUnidadeAutorizadaEntity().getId().toString());
			if(prestacaoContasDTO.getStatus() != null)
				query.setString("status", prestacaoContasDTO.getStatus().getDescricao().toUpperCase());
			if(prestacaoContasDTO.getRecursoDiaria() != null)
				query.setString("recursoDiaria", prestacaoContasDTO.getRecursoDiaria().getDescricao().toUpperCase());
			if(prestacaoContasDTO.getAtividade() != null)
				query.setString("atividade", prestacaoContasDTO.getAtividade().getDescricao().toUpperCase());
		}
		
		return query;
	}

	private StringBuilder gerarSqlDatatable(AbstractDTO dto) {
		StringBuilder sqlFrom = new StringBuilder();
		StringBuilder sql = new StringBuilder();
		sqlFrom.append(" ")
		   .append("  from " + this.getType().getSimpleName() + " as entity ")
		   .append("  left join entity.servidor servidor ");
		sql.append(" where 1 = 1 ");
		
		if (dto != null && !dto.isNull()){
			PrestacaoContasDTO prestacaoContasDTO = (PrestacaoContasDTO) dto;
			
			if(prestacaoContasDTO.getProtocolo() != null && !prestacaoContasDTO.getProtocolo().equals("")) { 
				sql.append("  and entity.protocolo = :protocolo");
			}				
			if(prestacaoContasDTO.getOs() != null && !prestacaoContasDTO.getOs().equals("")) {
				sql.append("  and entity.os = :os");
			}
			if(prestacaoContasDTO.getNrBoletimUrs() != null && !prestacaoContasDTO.getNrBoletimUrs().equals("")) {
				sql.append("  and entity.nrBoletimUrs = :nrBoletimUrs");
			}
			if(prestacaoContasDTO.getEntregueEm() != null || prestacaoContasDTO.getEntregueEmOut() != null) {
				sql.append("  and entity.entregueEm BETWEEN :entregueEm AND :entregueEmOut ");
			}
			if(prestacaoContasDTO.getDtInicio() != null || prestacaoContasDTO.getDtInicioOut() != null) 
				sql.append(" and entity.dtInicio between :dtInicio AND :dtInicioOut ");
			if(prestacaoContasDTO.getDtFim() != null) 
				sql.append(" and entity.dtFim between :dtFim AND :dtFimOut ");
			if(prestacaoContasDTO.getDtReprovacao() != null) 
				sql.append(" and entity.dataReprovacao between :dtReprovacao AND :dtReprovacaoOut ");
			if(prestacaoContasDTO.getServidor() != null) {
				sql.append(" and servidor.nome = :nome ");
			}
			if(prestacaoContasDTO.getUnidadeOrigemEntity()!=null) {
				sql.append(" and entity.idUnidadeOrigem = :idUnidadeOrigem ");
			}
			if(prestacaoContasDTO.getUnidadeSolicitanteEntity()!=null) {
				sql.append(" and entity.idUnidadeSolicitante = :idUnidadeSolicitante ");
			}
			if(prestacaoContasDTO.getUnidadeAutorizadaEntity()!=null) {
				sql.append(" and entity.idUnidadeAutorizada = :idUnidadeAutorizada ");
			}
			if(prestacaoContasDTO.getStatus()!=null) {
				sql.append(" and entity.status = :status ");
			}
			if(prestacaoContasDTO.getRecursoDiaria()!=null) {
				sql.append(" and entity.recursoDiaria = :recursoDiaria ");
			}
			if(prestacaoContasDTO.getAtividade()!=null) {
				sql.append(" and entity.atividade = :atividade ");
			}
		}
		
		sqlFrom.append(sql.toString());
		sql = new StringBuilder(sqlFrom.toString());
		
		return sql;
	}
	
	@SuppressWarnings("unchecked")
	public List<PrestacaoDeContas> findByNome(String nome){
		List<Servidor> servidores = servidorService.findByNome(nome);
		
		StringBuilder sql = new StringBuilder();
		sql.append("select pc ")
		   .append("  from PrestacaoDeContas pc, Pessoa p ")
		   .append(" where pc.idServidor = :id ")
		   .append(" AND pc.id = p.id ");
		
		Query query = getSession().createQuery(sql.toString()).setParameterList("id", servidores);
		
		List<PrestacaoDeContas> listaPrestacaoContas = query.list();
		
		return listaPrestacaoContas.isEmpty()?new ArrayList<PrestacaoDeContas>():listaPrestacaoContas;
	}
	
	public PrestacaoDeContas findById(Long id){
		PrestacaoDeContas prestacao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select prestacao ")
		   .append("  from PrestacaoDeContas as prestacao ")
		   .append("  left join prestacao.listaHistoricoViagem ")
		   .append(" where prestacao.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		prestacao = (PrestacaoDeContas) query.uniqueResult();
		
		if (prestacao == null){
			return null;
		}
		
		return prestacao;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<PrestacaoDeContas> findAllComAtraso() {
		List<PrestacaoDeContas> lista;
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("select entity ")
		   .append("  from " + this.getType().getSimpleName() + " as entity ")
		   .append(" where entity.status = 'ABERTA' ")
		   .append("    or entity.status = 'REABERTA' ")
		   .append(" order by entity.os ");
		
		Query query = getSession().createQuery(sql.toString());
		lista = query.list();
		
		if (lista != null)
			for (PrestacaoDeContas prestacaoDeContas : lista) {
				Hibernate.initialize(prestacaoDeContas.getListaHistoricoEnvioEmail());
				
				if (prestacaoDeContas.getServidor().isTipoServidorINDEA())
					prestacaoDeContas.getServidor().setServidorIndeaWeb(servidorService.findByIdFetchAll(Long.valueOf(prestacaoDeContas.getServidor().getId())));
				
				if (prestacaoDeContas.getIdUnidadeOrigem() != null)
					prestacaoDeContas.setUnidadeOrigem(listsManagedBean.getUnidadeById(Long.valueOf(prestacaoDeContas.getIdUnidadeOrigem())));
				if (prestacaoDeContas.getIdUnidadeSolicitante() != null)
					prestacaoDeContas.setUnidadeSolicitante(listsManagedBean.getUnidadeById(Long.valueOf(prestacaoDeContas.getIdUnidadeSolicitante())));
			}

		return lista;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<PrestacaoDeContas> findAllComDefeitos() {
		List<PrestacaoDeContas> lista;
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("select prestacao ")
		   .append("  from " + this.getType().getSimpleName() + " as prestacao ")
		   .append("  left join fetch prestacao.servidor servidor ")
		   .append(" where servidor.tipoServidor is null ")
		   .append("    or prestacao.idUnidadeOrigem is null ")
		   .append("    or prestacao.idUnidadeSolicitante is null ")
		   .append("    or prestacao.idUnidadeAutorizada is null ")
		   .append("    or prestacao.dtInicio is null ")
		   .append("    or prestacao.dtFim is null ")
		   .append("    or prestacao.valor is null ")
		   .append(" order by prestacao.os ");
		   
		
		Query query = getSession().createQuery(sql.toString());
		lista = query.list();
		
		if (lista != null)
			for (PrestacaoDeContas prestacaoDeContas : lista) {
				if (prestacaoDeContas.getServidor().isTipoServidorINDEA())
					prestacaoDeContas.getServidor().setServidorIndeaWeb(servidorService.findByIdFetchAll(Long.valueOf(prestacaoDeContas.getServidor().getId())));
			}

		return lista;
	}
	
	public boolean checkExistOSProtocolo(PrestacaoDeContas prestacao) {
		if(prestacao.getId()==null) {
			StringBuilder sql = new StringBuilder();
			
			sql.append("select prestacao ")
			   .append("  from " + this.getType().getSimpleName() + " as prestacao ")
			   .append(" WHERE prestacao.os = :os ")
			   .append(" AND prestacao.protocolo = :protocolo ");
			
			Query query = getSession().createQuery(sql.toString());
			query.setString("os", prestacao.getOs());
			query.setString("protocolo", prestacao.getProtocolo());
			
			return query.list().size()>0;
		}
		return false;
	}
	
	@Override
	public void saveOrUpdate(PrestacaoDeContas prestacaoContas) {
		
		prestacaoContas = (PrestacaoDeContas) this.getSession().merge(prestacaoContas);
		
		//super.saveOrUpdate(prestacaoContas);
		
		log.info("Salvando Prestašao de Contas {}", prestacaoContas.getId());
	}
	
	@Override
	public void delete(PrestacaoDeContas prestacaoContas) {
		super.delete(prestacaoContas);
		
		log.info("Removendo Prestašao de Contas {}", prestacaoContas.getId());
	}
	
	public void aprovar(PrestacaoDeContas prestacaoDeContas) {
		HistoricoViagem historicoViagem = new HistoricoViagem();
		historicoViagem.setTipoEventoViagem(TipoEventoViagem.APROVACAO);
		historicoViagem.setPrestacaoDeContas(prestacaoDeContas);
		Calendar dataEntrega = Calendar.getInstance();
		dataEntrega.setTime(prestacaoDeContas.getEntregueEm());
		historicoViagem.setDataEvento(dataEntrega);

		log.info("Aprovando Prestašao de Contas {}", prestacaoDeContas.getId());
		
		historicoViagemService.saveOrUpdate(historicoViagem);
		this.saveOrUpdate(prestacaoDeContas);
	}
	
	public void reprovar(PrestacaoDeContas prestacaoDeContas) {
		HistoricoViagem historicoViagem = new HistoricoViagem();
		historicoViagem.setTipoEventoViagem(TipoEventoViagem.REPROVACAO);
		historicoViagem.setPrestacaoDeContas(prestacaoDeContas);
		Calendar data = Calendar.getInstance();
		data.setTime(prestacaoDeContas.getDataReprovacao());
		historicoViagem.setDataEvento(data);
		historicoViagem.setObservacao(prestacaoDeContas.getMotivoReprovacao());

		log.info("Aprovando Prestašao de Contas {}", prestacaoDeContas.getId());
		
		historicoViagemService.saveOrUpdate(historicoViagem);
		this.saveOrUpdate(prestacaoDeContas);
		
		Email.sendEmailServidorComContasReprovadas(prestacaoDeContas, prestacaoDeContas.getServidor().getEmail(), true);
	}
	
	public void cancelar(PrestacaoDeContas prestacaoDeContas) {
		HistoricoViagem historicoViagem = new HistoricoViagem();
		historicoViagem.setTipoEventoViagem(TipoEventoViagem.CANCELAMENTO);
		historicoViagem.setPrestacaoDeContas(prestacaoDeContas);
		Calendar data = Calendar.getInstance();
		data.setTime(prestacaoDeContas.getDataReprovacao());
		historicoViagem.setDataEvento(data);
		historicoViagem.setObservacao(prestacaoDeContas.getMotivoCancelamento());

		log.info("Aprovando Prestašao de Contas {}", prestacaoDeContas.getId());
		
		historicoViagemService.saveOrUpdate(historicoViagem);
		this.saveOrUpdate(prestacaoDeContas);
	}
	
	public void reabrir(PrestacaoDeContas prestacaoDeContas) {
		HistoricoViagem historicoViagem = new HistoricoViagem();
		historicoViagem.setTipoEventoViagem(TipoEventoViagem.REABERTURA);
		historicoViagem.setPrestacaoDeContas(prestacaoDeContas);
		Calendar data = Calendar.getInstance();
		historicoViagem.setDataEvento(data);
		historicoViagem.setObservacao(prestacaoDeContas.getMotivoReabertura());

		log.info("Aprovando Prestašao de Contas {}", prestacaoDeContas.getId());
		
		historicoViagemService.saveOrUpdate(historicoViagem);
		this.saveOrUpdate(prestacaoDeContas);
	}

	@Override
	public void validar(PrestacaoDeContas model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarPersist(PrestacaoDeContas model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarMerge(PrestacaoDeContas model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarDelete(PrestacaoDeContas model) {
		// TODO Auto-generated method stub
		
	}

}
