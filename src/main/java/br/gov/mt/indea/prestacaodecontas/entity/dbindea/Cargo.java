package br.gov.mt.indea.prestacaodecontas.entity.dbindea;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import br.gov.mt.indea.prestacaodecontas.annotation.IndeaWeb;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.BaseEntity;

@Entity
@Table(name = "cargo", schema = "cogesp")
@IndeaWeb
public class Cargo extends BaseEntity<Long> implements Serializable {

	private static final long serialVersionUID = -4553664195166282727L;
	
	@Id
	private Long id;
	
	@NotBlank
    @Size(max = 110)
    @Column(nullable = false, length = 110)
    private String nome;
    
	@NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private TipoNivel nivel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoNivel getNivel() {
		return nivel;
	}

	public void setNivel(TipoNivel nivel) {
		this.nivel = nivel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cargo other = (Cargo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
