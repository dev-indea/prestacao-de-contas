package br.gov.mt.indea.prestacaodecontas.util;

public class CoordenadaGeograficaUtil {

	public static Double convertToDecimal(Integer grau, Integer min, Float seg, String orientation){
		if (grau == null || min == null || seg == null || orientation == null)
			return null;
		
		double d = Math.signum(grau) * (Math.abs(grau) + (min / 60.0) + (seg / 3600.0));
		if (orientation != null && orientation.equals("S"))
			d = d * -1;
		return d;
	}

}
