package br.gov.mt.indea.prestacaodecontas.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.EmailException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.prestacaodecontas.entity.dto.UsuarioDTO;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.Usuario;
import br.gov.mt.indea.prestacaodecontas.entity.seguranca.Grupo;
import br.gov.mt.indea.prestacaodecontas.entity.seguranca.Permissao;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.prestacaodecontas.security.UserSecurity;
import br.gov.mt.indea.prestacaodecontas.service.LazyObjectDataModel;
import br.gov.mt.indea.prestacaodecontas.service.UsuarioService;
import br.gov.mt.indea.prestacaodecontas.service.seguranca.GrupoService;
import br.gov.mt.indea.prestacaodecontas.service.seguranca.PermissaoService;
import br.gov.mt.indea.prestacaodecontas.util.Email;
import br.gov.mt.indea.prestacaodecontas.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("usuarioManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarUsuario", pattern = "/usuario/pesquisar", viewId = "/pages/usuario/lista.jsf"),
		@URLMapping(id = "incluirUsuario", pattern = "/usuario/incluir", viewId = "/pages/usuario/novo.jsf"),
		@URLMapping(id = "alterarUsuario", pattern = "/usuario/alterar/#{usuarioManagedBean.id}", viewId = "/pages/usuario/novo.jsf"),
		@URLMapping(id = "selfUsuario", pattern = "/self/alterar", viewId = "/pages/usuario/novo.jsf"),
		@URLMapping(id = "permissoesUsuario", pattern = "/usuario/#{usuarioManagedBean.id}/permissoes", viewId = "/pages/usuario/permissoes.jsf")})
public class UsuarioManagedBean implements Serializable {

	private static final long serialVersionUID = 5401332308697941357L;
	
	private String id;
	
	@Inject
	private Usuario usuario;
	
	@Inject
	private UsuarioDTO usuarioDTO;
	
	private String senha;
	
	private String senhaRedigitada;
	
	boolean editando = false;
	
	boolean editandoSelf = false;
	
	private LazyObjectDataModel<Usuario> listaUsuario;
	
	private List<Grupo> listaGrupo;
	
	private List<Permissao> listaPermissao;
	
	@Inject
	private UsuarioService usuarioService;
	
	@Inject
	private GrupoService grupoService;
	
	@Inject
	private PermissaoService permissaoService;
	
	@Inject
	private UserSecurity usuarioLogado;
	
	@PostConstruct
	private void init(){
		this.listaPermissao = permissaoService.findAll("nome");
	}
	
	public void limpar(){
		this.usuario = new Usuario();
		
		this.editando = false;
		this.editandoSelf = false;
		this.senha = null;
		this.senhaRedigitada = null;
	}
	
	public void buscarUsuarios(){
		this.listaUsuario = new LazyObjectDataModel<Usuario>(this.usuarioService, this.usuarioDTO);
		
	}
	
	@URLAction(mappingId = "incluirUsuario", onPostback = false)
	public void novo(){
		this.limpar();
	}
	
	@URLAction(mappingId = "pesquisarUsuario", onPostback = false)
	public void pesquisar(){
		limpar();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String usernameUsuario = (String) request.getSession().getAttribute("usernameUsuario");
		
		if (usernameUsuario != null){
			this.usuarioDTO = new UsuarioDTO();
			this.usuarioDTO.setUsername(usernameUsuario);
			this.buscarUsuarios();
			
			request.getSession().setAttribute("usernameUsuario", null);
		}
	}

	@URLAction(mappingId = "alterarUsuario", onPostback = false)
	public void editar(){
		this.editando = true;
		this.usuario = usuarioService.findById(this.getId());
		
	}
	
	@URLAction(mappingId = "selfUsuario", onPostback = false)
	public void editarSelf(){
		this.editando = true;
		this.editandoSelf = true;
		
		this.usuario = usuarioService.findById(this.usuarioLogado.getUsuario().getId());
		
	}
	
	@URLAction(mappingId = "permissoesUsuario", onPostback = false)
	public void permissoes(){
		this.usuario = usuarioService.findById(this.getId());
		
		this.listaGrupo = grupoService.findAll("nome");
	}
	
	public String adicionar() throws IOException{
		if (!senha.equals(senhaRedigitada)){
			FacesMessageUtil.addWarnContextFacesMessage("As senhas digitadas devem ser iguais", "");
			return "";
		}
		
		if (usuario.getDataCadastro() != null){
			if (senha != null && !senha.equals("")){
				
				try {
					Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
					
					senha = passwordEncoder.encodePassword(senha, null);
					usuario.setPassword(senha);
				} catch (Exception e) {
					FacesMessageUtil.addErrorContextFacesMessage("N�o foi poss�vel criptografar a senha digitada", e.getLocalizedMessage());
					e.printStackTrace();
				}
			}

			this.usuarioService.saveOrUpdate(usuario);
			
		}else{
			this.usuario.setDataCadastro(Calendar.getInstance());
			this.usuario.setUltimoLogin(Calendar.getInstance().getTime());
			
			try {
				Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
				
				senha = passwordEncoder.encodePassword(senha, null);
				
				this.usuario.setPassword(senha);
			} catch (Exception e1) {
				FacesMessageUtil.addErrorContextFacesMessage("N�o foi poss�vel criptografar a senha digitada", e1.getLocalizedMessage());
				return "";
			}
	        
			this.usuarioService.saveOrUpdate(usuario);
			
	        try {
				String mensagem = "Informamos que a sua conta foi criada com sucesso no Sistema de Presta��o de Contas do INDEA-MT.\n\n" + 
						"Voc� poder� acessar o sistema no endere�o http://web.indea.mt.gov.br/prestacao-de-contas com as seguintes informa��es:\n\n" + 
						"login: " +  this.usuario.getId() + "\n" +
						"senha: " +  this.senhaRedigitada;
				
				Email.send(this.usuario.getEmail(), this.usuario.getNome(), "Cria��o de usu�rio", mensagem);
			} catch (EmailException e) {
				FacesMessageUtil.addWarnContextFacesMessage("O usu�rio foi criado mas n�o foi poss�vel enviar email de confirma��o", e.getLocalizedMessage());
			}
			FacesMessageUtil.addInfoContextFacesMessage("Usu�rio " + usuario.getId() + " adicionado!", "");
		}
		
		boolean editandoSelf = this.editandoSelf;
		
		if (editandoSelf){
			limpar();
			FacesMessageUtil.addInfoContextFacesMessage("Usu�rio " + usuario.getId() + " atualizado", "As altera��es ter�o efeito na pr�xima vez que entrar no sistema");
			
			return "pretty:dashboard";
		}else {
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			request.getSession().setAttribute("usernameUsuario", usuario.getId());
			
			limpar();
			
			return "pretty:pesquisarUsuario";
		}
	}
	
	public void ativar(Usuario usuario){
		usuario.setStatus(AtivoInativo.ATIVO);
		
		if (usuario.isUsuarioExpirado())
			usuario.setUltimoLogin(new Date());
		
		this.usuarioService.saveOrUpdate(usuario);
		FacesMessageUtil.addInfoContextFacesMessage("Usu�rio ativado", "");
	}
	
	public void desativar(Usuario usuario){
		usuario.setStatus(AtivoInativo.INATIVO);
		
		this.usuarioService.saveOrUpdate(usuario);
		FacesMessageUtil.addInfoContextFacesMessage("Usu�rio desativado", "");
	}
	
	public void remover(Usuario usuario){
		this.usuarioService.delete(usuario);
		FacesMessageUtil.addInfoContextFacesMessage("Usu�rio exclu�do com sucesso", "");
	}
	
	public void adicionar(Grupo grupo){
		this.usuario = this.usuarioService.adicionarGrupo(usuario, grupo);
		
		FacesMessageUtil.addInfoContextFacesMessage("Grupo adicionado", "");
	}
	
	public void remover(Grupo grupo){
		this.usuario = this.usuarioService.removerGrupo(usuario, grupo);
		
		FacesMessageUtil.addInfoContextFacesMessage("Grupo removido", "");
	}
	
	public boolean usuarioHaveGrupo(Grupo grupo){
		return this.usuario.getListaGrupo().contains(grupo);
	}
	
	public void adicionar(Permissao permissao){
		this.usuario = this.usuarioService.adicionarPermissao(usuario, permissao);
		
		FacesMessageUtil.addInfoContextFacesMessage("Permiss�o adicionada", "");
	}
	
	public void remover(Permissao permissao){
		this.usuario = this.usuarioService.removerPermissao(usuario, permissao);
		
		FacesMessageUtil.addInfoContextFacesMessage("Permiss�o removida", "");
	}
	
	public boolean usuarioHavePermissao(Permissao permissao){
		return this.usuario.getListaPermissao().contains(permissao);
	}
	
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSenhaRedigitada() {
		return senhaRedigitada;
	}

	public void setSenhaRedigitada(String senhaRedigitada) {
		this.senhaRedigitada = senhaRedigitada;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}

	public LazyObjectDataModel<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(LazyObjectDataModel<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Grupo> getListaGrupo() {
		return listaGrupo;
	}

	public void setListaGrupo(List<Grupo> listaGrupo) {
		this.listaGrupo = listaGrupo;
	}

	public List<Permissao> getListaPermissao() {
		return listaPermissao;
	}

	public void setListaPermissao(List<Permissao> listaPermissao) {
		this.listaPermissao = listaPermissao;
	}

	public boolean isEditandoSelf() {
		return editandoSelf;
	}

	public void setEditandoSelf(boolean editandoSelf) {
		this.editandoSelf = editandoSelf;
	}

	public UsuarioDTO getUsuarioDTO() {
		return usuarioDTO;
	}

	public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
		this.usuarioDTO = usuarioDTO;
	}

}
