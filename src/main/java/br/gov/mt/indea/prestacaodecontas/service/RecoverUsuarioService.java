package br.gov.mt.indea.prestacaodecontas.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.RecoverUsuario;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RecoverUsuarioService extends PaginableService<RecoverUsuario, Long> {

	private static final Logger log = LoggerFactory.getLogger(RecoverUsuarioService.class);

	protected RecoverUsuarioService() {
		super(RecoverUsuario.class);
	}
	
	public RecoverUsuario findById(Long id){
		RecoverUsuario recoverUsuario;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select recoverUsuario ")
		   .append("  from RecoverUsuario recoverUsuario ")
		   .append(" where recoverUsuario.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		recoverUsuario = (RecoverUsuario) query.uniqueResult();
		
		return recoverUsuario;
	}
	
	public RecoverUsuario findByToken(String token){
		RecoverUsuario recoverUsuario;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select recoverUsuario ")
		   .append("  from RecoverUsuario recoverUsuario ")
		   .append("  join fetch recoverUsuario.usuario usuario")
		   .append(" where recoverUsuario.token = :token ");
		
		Query query = getSession().createQuery(sql.toString()).setString("token", token);
		recoverUsuario = (RecoverUsuario) query.uniqueResult();
		
		return recoverUsuario;
	}
	
	@Override
	public void saveOrUpdate(RecoverUsuario recoverUsuario) {
		super.saveOrUpdate(recoverUsuario);
		
		log.info("Salvando Recover {}", recoverUsuario.getId());
	}
	
	@Override
	public void delete(RecoverUsuario recoverUsuario) {
		super.delete(recoverUsuario);
		
		log.info("Removendo Recover {}", recoverUsuario.getId());
	}
	
	@Override
	public void validar(RecoverUsuario RecoverUsuario) {

	}

	@Override
	public void validarPersist(RecoverUsuario RecoverUsuario) {

	}

	@Override
	public void validarMerge(RecoverUsuario RecoverUsuario) {

	}

	@Override
	public void validarDelete(RecoverUsuario RecoverUsuario) {

	}

}
