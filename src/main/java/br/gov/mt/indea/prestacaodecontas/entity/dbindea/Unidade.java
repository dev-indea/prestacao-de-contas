package br.gov.mt.indea.prestacaodecontas.entity.dbindea;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import br.gov.mt.indea.prestacaodecontas.annotation.IndeaWeb;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.BaseEntity;
import br.gov.mt.indea.prestacaodecontas.enums.Dominio.TipoUnidade;

@Entity
@Table(name = "unidade")
@IndeaWeb
public class Unidade extends BaseEntity<Long> implements Serializable {

	private static final long serialVersionUID = -7679964536402134480L;

	@Id
	private Long id;
	
	@NotBlank
    @Size(max = 110)
    @Column(nullable = false, length = 110)
    private String nome;
    
	@NotBlank
    @Size(max = 40)
    @Column(nullable = false, length = 40)
    private String sigla;
    
	@Size(max = 45)
    @Column(length = 45)
    private String email;
    
	@NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "data_atualizado")
    private Date dataAtualizado;
    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unidade_pai_id")
    private Unidade unidadePai;
    
	@NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "tipo_unidade", nullable = false)
    private TipoUnidade tipoUnidade;
    
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "municipio_id")
    private Municipio municipio;
    
	@Column(name = "cod_seap")
    private Integer codSeap;
    
    private boolean ativo = true;
    
    @OneToMany(mappedBy = "unidade", fetch = FetchType.LAZY)
    private List<Servidor> servidores = new ArrayList<>();

    public Unidade() {
    	
    }
    
    public Unidade(Long id, String nome, String sigla, String email, Unidade unidadePai, TipoUnidade tipoUnidade, Municipio municipio) {
    	this.id = id;
    	this.nome = nome;
    	this.sigla = sigla;
    	this.email = email;
    	this.unidadePai = unidadePai;
    	this.tipoUnidade = tipoUnidade;
    	this.municipio = municipio;
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDataAtualizado() {
		return dataAtualizado;
	}

	public void setDataAtualizado(Date dataAtualizado) {
		this.dataAtualizado = dataAtualizado;
	}

	public Unidade getUnidadePai() {
		return unidadePai;
	}

	public void setUnidadePai(Unidade unidadePai) {
		this.unidadePai = unidadePai;
	}

	public TipoUnidade getTipoUnidade() {
		return tipoUnidade;
	}

	public void setTipoUnidade(TipoUnidade tipoUnidade) {
		this.tipoUnidade = tipoUnidade;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Integer getCodSeap() {
		return codSeap;
	}

	public void setCodSeap(Integer codSeap) {
		this.codSeap = codSeap;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public List<Servidor> getServidores() {
		return servidores;
	}

	public void setServidores(List<Servidor> servidores) {
		this.servidores = servidores;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Unidade other = (Unidade) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

    
}
