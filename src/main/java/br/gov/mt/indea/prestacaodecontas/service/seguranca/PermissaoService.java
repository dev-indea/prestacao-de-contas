package br.gov.mt.indea.prestacaodecontas.service.seguranca;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;

import br.gov.mt.indea.prestacaodecontas.entity.seguranca.Permissao;
import br.gov.mt.indea.prestacaodecontas.service.PaginableService;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PermissaoService extends PaginableService<Permissao, Long> {

	protected PermissaoService() {
		super(Permissao.class);
	}
	
	public Permissao findByIdFetchAll(Long id){
		Permissao permissao;
		
		StringBuilder sql = new StringBuilder();
		sql.append("  from Permissao permissao ")
		   .append(" where permissao.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		permissao = (Permissao) query.uniqueResult();
		
		return permissao;
	}
	
	@Override
	public void saveOrUpdate(Permissao permissao) {
		permissao = (Permissao) this.getSession().merge(permissao);
		
		super.saveOrUpdate(permissao);
	}
	
	@Override
	public void validar(Permissao Permissao) {

	}

	@Override
	public void validarPersist(Permissao Permissao) {

	}

	@Override
	public void validarMerge(Permissao Permissao) {

	}

	@Override
	public void validarDelete(Permissao Permissao) {

	}

}
