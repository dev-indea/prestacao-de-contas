package br.gov.mt.indea.prestacaodecontas.service.timer;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timer;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.Parametros;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.PrestacaoDeContas;
import br.gov.mt.indea.prestacaodecontas.service.ParametrosService;
import br.gov.mt.indea.prestacaodecontas.service.PrestacaoDeContasService;
import br.gov.mt.indea.prestacaodecontas.util.Email;

@Singleton
@Startup
@LocalBean
public class ServidoresComDebitoTimer {

	private static final Logger log = LoggerFactory.getLogger(ServidoresComDebitoTimer.class);

	@Inject
	private PrestacaoDeContasService prestacaoDeContasService;
	
	@Inject
	private ParametrosService parametrosService;
	
	
	@Schedule(hour = "0", minute = "1", second = "0")
	public void execute(Timer timer) {
        log.info("Iniciando timer"); 

		List<PrestacaoDeContas> lista = prestacaoDeContasService.findAllComAtraso();
		List<PrestacaoDeContas> listaRefinada = new ArrayList<PrestacaoDeContas>();
		
		if (lista != null) {
			
			List<Parametros> listaParametros = (List<Parametros>)parametrosService.findAll();
			Parametros parametros = listaParametros.isEmpty()?null:listaParametros.get(0);
			
			Integer diasUteis = parametros!=null?parametros.getDiasCobranca():5;
			Integer prazoRelatorio = parametros!=null?parametros.getPrazoRelatorio():10;
			
			//Seleciona todos os servidores com d�bito maior do que 10 dias �teis
			for (PrestacaoDeContas prestacaoDeContas : lista) {
				if (prestacaoDeContas.getDiasUteisEmAtraso() >= prazoRelatorio)
					listaRefinada.add(prestacaoDeContas);
			}

			//Envia email para todos os servidores em atraso ap�s ter se passado os 10 dias �teis ou 5 dias da �ltima cobran�a
			//Para alterar para os par�metros, isso n�o vai valer mais, pois o parametro de dias �teis pode n�o ser m�ltiplo do par�metro de dias adicionais 
			//Portanto, deve ter dois ifs abaixo: um para verificar cada par�metro
			for (PrestacaoDeContas prestacaoDeContas : lista) {
				if (prestacaoDeContas.getDiasUteisEmAtraso() == (prazoRelatorio+1)
						|| (prestacaoDeContas.getDiasUteisEmAtraso() > prazoRelatorio && prestacaoDeContas.getDiasUteisEmAtraso() % diasUteis == 0)) {
//					Email.sendEmailServidorComAtraso(prestacaoDeContas, prestacaoDeContas.getServidor().getEmail(), false);
				}
			}
		}
		
		log.info("Finalizando timer");
	}

}