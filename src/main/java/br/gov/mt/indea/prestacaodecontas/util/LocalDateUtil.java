package br.gov.mt.indea.prestacaodecontas.util;

import java.time.LocalDate;

import com.opengamma.strata.basics.ReferenceData;
import com.opengamma.strata.basics.date.HolidayCalendar;
import com.opengamma.strata.basics.date.HolidayCalendarId;

public class LocalDateUtil {
	
	public static Long diferencaEmDiasUteis(LocalDate paramDataInicial, LocalDate paramDataFinal) {
		LocalDate dataInicial = null;
		LocalDate dataFinal = null;
		
		HolidayCalendarId holCalId = HolidayCalendarId.of("BRBD");
		HolidayCalendar holCal = holCalId.resolve(ReferenceData.standard());

		if (paramDataInicial.isBefore(paramDataFinal)){
			dataInicial = paramDataInicial;
			dataFinal = paramDataFinal;
		} else {
			dataInicial = paramDataFinal;
			dataFinal = paramDataInicial;
		}
		
		Long quantidade = 0L;
		LocalDate dataFor = dataInicial;
		
		
		while (!dataFor.isEqual(dataFinal)) {
			dataFor = dataFor.plusDays(1);
			
			if (holCal.isBusinessDay(dataFor))
				quantidade++;
		}
		
		return quantidade;
	}
		
}
