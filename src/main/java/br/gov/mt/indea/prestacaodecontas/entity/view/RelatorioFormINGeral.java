package br.gov.mt.indea.prestacaodecontas.entity.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class RelatorioFormINGeral implements Serializable{
	
	private static final long serialVersionUID = -1599212622409444851L;

	private String urs;

	private String ule;
	
	private String numero;
    
	private Integer latitude_grau;
    
	private Integer latitude_minuto;
    
	private Float latitude_segundo;
    
	private Integer longitude_grau;
    
	private Integer longitude_minuto;
    
	private Float longitude_segundo;
    
	private String tipo_notificacao;
    
	private String houve_colheita;
    
	private Date data_envio_material;
    
	private Date data_provavel_inicio;
    
	private Date data_notificacao;
    
	private Date data_cadastro;
    
	private Date data_visita_inicial_med_vet;
    
	private String origem_notificacao;
    
	private String especie_afetada;
    
	private String nome_proprietario;
    
	private String nome_propriedade;
    
	private String codigo_propriedade;
    
	private String medico_veterinario_responsavel;
    
	private String crmv;
    
	private String diagnostico_provavel;
    
	private String diagnostico_conclusivo;
    
	private String tipo_teste_confirmou_resultado;
    
	private Date data_resultado;
    
	private String diagnostico;
    
	private String investigação_encerrada;
    
	private Date data_form_com_encerramento;
    
	private BigInteger quantidade_form_com;
    
	private BigDecimal quantidade_equideos_destruidos;
	
	private BigDecimal quantidade_bovinos_destruidos;

	private BigDecimal quantidade_suinos_destruidos;

	private BigDecimal quantidade_aves_destruidos;

	private BigDecimal quantidade_ovinos_destruidos;

	private BigDecimal quantidade_caprinos_destruidos;

	public String getUrs() {
		return urs;
	}

	public void setUrs(String urs) {
		this.urs = urs;
	}

	public String getUle() {
		return ule;
	}

	public void setUle(String ule) {
		this.ule = ule;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getLatitude_grau() {
		return latitude_grau;
	}

	public void setLatitude_grau(Integer latitude_grau) {
		this.latitude_grau = latitude_grau;
	}

	public Integer getLatitude_minuto() {
		return latitude_minuto;
	}

	public void setLatitude_minuto(Integer latitude_minuto) {
		this.latitude_minuto = latitude_minuto;
	}

	public Float getLatitude_segundo() {
		return latitude_segundo;
	}

	public void setLatitude_segundo(Float latitude_segundo) {
		this.latitude_segundo = latitude_segundo;
	}

	public Integer getLongitude_grau() {
		return longitude_grau;
	}

	public void setLongitude_grau(Integer longitude_grau) {
		this.longitude_grau = longitude_grau;
	}

	public Integer getLongitude_minuto() {
		return longitude_minuto;
	}

	public void setLongitude_minuto(Integer longitude_minuto) {
		this.longitude_minuto = longitude_minuto;
	}

	public Float getLongitude_segundo() {
		return longitude_segundo;
	}

	public void setLongitude_segundo(Float longitude_segundo) {
		this.longitude_segundo = longitude_segundo;
	}

	public String getTipo_notificacao() {
		return tipo_notificacao.replace("_", " ");
	}

	public void setTipo_notificacao(String tipo_notificacao) {
		this.tipo_notificacao = tipo_notificacao;
	}

	public String getHouve_colheita() {
		return houve_colheita;
	}

	public void setHouve_colheita(String houve_colheita) {
		this.houve_colheita = houve_colheita;
	}

	public Date getData_envio_material() {
		return data_envio_material;
	}

	public void setData_envio_material(Date data_envio_material) {
		this.data_envio_material = data_envio_material;
	}

	public Date getData_provavel_inicio() {
		return data_provavel_inicio;
	}

	public void setData_provavel_inicio(Date data_provavel_inicio) {
		this.data_provavel_inicio = data_provavel_inicio;
	}

	public Date getData_notificacao() {
		return data_notificacao;
	}

	public void setData_notificacao(Date data_notificacao) {
		this.data_notificacao = data_notificacao;
	}

	public Date getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(Date data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	public Date getData_visita_inicial_med_vet() {
		return data_visita_inicial_med_vet;
	}

	public void setData_visita_inicial_med_vet(Date data_visita_inicial_med_vet) {
		this.data_visita_inicial_med_vet = data_visita_inicial_med_vet;
	}

	public String getOrigem_notificacao() {
		return origem_notificacao.replace("_", " ");
	}

	public void setOrigem_notificacao(String origem_notificacao) {
		this.origem_notificacao = origem_notificacao;
	}

	public String getEspecie_afetada() {
		if (especie_afetada != null){
			String[] especies = especie_afetada.split(",");
			for (String especie : especies) {
				especie_afetada = especie_afetada.replace(especie, especie.replaceFirst(especie.charAt(0) + "", (especie.charAt(0) + "").toUpperCase()));
			}
		}
		
		return especie_afetada;
	}

	public void setEspecie_afetada(String especie_afetada) {
		this.especie_afetada = especie_afetada;
	}

	public String getNome_proprietario() {
		return nome_proprietario;
	}

	public void setNome_proprietario(String nome_proprietario) {
		this.nome_proprietario = nome_proprietario;
	}

	public String getNome_propriedade() {
		return nome_propriedade;
	}

	public void setNome_propriedade(String nome_propriedade) {
		this.nome_propriedade = nome_propriedade;
	}

	public String getCodigo_propriedade() {
		return codigo_propriedade;
	}

	public void setCodigo_propriedade(String codigo_propriedade) {
		this.codigo_propriedade = codigo_propriedade;
	}

	public String getMedico_veterinario_responsavel() {
		return medico_veterinario_responsavel;
	}

	public void setMedico_veterinario_responsavel(String medico_veterinario_responsavel) {
		this.medico_veterinario_responsavel = medico_veterinario_responsavel;
	}

	public String getCrmv() {
		return crmv;
	}

	public void setCrmv(String crmv) {
		this.crmv = crmv;
	}

	public String getDiagnostico_provavel() {
		return diagnostico_provavel;
	}

	public void setDiagnostico_provavel(String diagnostico_provavel) {
		this.diagnostico_provavel = diagnostico_provavel;
	}

	public String getDiagnostico_conclusivo() {
		return diagnostico_conclusivo;
	}

	public void setDiagnostico_conclusivo(String diagnostico_conclusivo) {
		this.diagnostico_conclusivo = diagnostico_conclusivo;
	}

	public String getTipo_teste_confirmou_resultado() {
		return tipo_teste_confirmou_resultado;
	}

	public void setTipo_teste_confirmou_resultado(String tipo_teste_confirmou_resultado) {
		this.tipo_teste_confirmou_resultado = tipo_teste_confirmou_resultado;
	}

	public Date getData_resultado() {
		return data_resultado;
	}

	public void setData_resultado(Date data_resultado) {
		this.data_resultado = data_resultado;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getInvestigação_encerrada() {
		return investigação_encerrada;
	}

	public void setInvestigação_encerrada(String investigação_encerrada) {
		this.investigação_encerrada = investigação_encerrada;
	}

	public Date getData_form_com_encerramento() {
		return data_form_com_encerramento;
	}

	public void setData_form_com_encerramento(Date data_form_com_encerramento) {
		this.data_form_com_encerramento = data_form_com_encerramento;
	}

	public BigInteger getQuantidade_form_com() {
		return quantidade_form_com;
	}

	public void setQuantidade_form_com(BigInteger quantidade_form_com) {
		this.quantidade_form_com = quantidade_form_com;
	}

	public BigDecimal getQuantidade_equideos_destruidos() {
		return quantidade_equideos_destruidos;
	}

	public void setQuantidade_equideos_destruidos(BigDecimal quantidade_equideos_destruidos) {
		this.quantidade_equideos_destruidos = quantidade_equideos_destruidos;
	}

	public BigDecimal getQuantidade_suinos_destruidos() {
		return quantidade_suinos_destruidos;
	}

	public void setQuantidade_suinos_destruidos(BigDecimal quantidade_suinos_destruidos) {
		this.quantidade_suinos_destruidos = quantidade_suinos_destruidos;
	}

	public BigDecimal getQuantidade_aves_destruidos() {
		return quantidade_aves_destruidos;
	}

	public void setQuantidade_aves_destruidos(BigDecimal quantidade_aves_destruidos) {
		this.quantidade_aves_destruidos = quantidade_aves_destruidos;
	}

	public BigDecimal getQuantidade_ovinos_destruidos() {
		return quantidade_ovinos_destruidos;
	}

	public void setQuantidade_ovinos_destruidos(BigDecimal quantidade_ovinos_destruidos) {
		this.quantidade_ovinos_destruidos = quantidade_ovinos_destruidos;
	}

	public BigDecimal getQuantidade_caprinos_destruidos() {
		return quantidade_caprinos_destruidos;
	}

	public void setQuantidade_caprinos_destruidos(BigDecimal quantidade_caprinos_destruidos) {
		this.quantidade_caprinos_destruidos = quantidade_caprinos_destruidos;
	}

	public BigDecimal getQuantidade_bovinos_destruidos() {
		return quantidade_bovinos_destruidos;
	}

	public void setQuantidade_bovinos_destruidos(BigDecimal quantidade_bovinos_destruidos) {
		this.quantidade_bovinos_destruidos = quantidade_bovinos_destruidos;
	}
    
}
