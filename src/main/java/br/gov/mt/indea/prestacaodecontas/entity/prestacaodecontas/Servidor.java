package br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.prestacaodecontas.enums.Dominio.TipoServidor;

@Entity
@DiscriminatorValue("SERVIDOR")
@Audited
public class Servidor extends Pessoa {

	private static final long serialVersionUID = 7811664327889819479L;

	@Size(max = 10)
    @Column(length = 10)
    private String matricula;
    
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_servidor")
	private TipoServidor tipoServidor;
	
	@Transient
	private br.gov.mt.indea.prestacaodecontas.entity.dbindea.Servidor servidorIndeaWeb;
	
	@Column(name="id_servidor")
	private Long idServidor;
	
	public Long getId() {
		if (this.isTipoServidorINDEA())
			return this.idServidor;
		else if (this.isTipoServidorPM())
			return super.getId();
		else
			return null;
	}
	
	public String getNome() {
		if (this.isTipoServidorINDEA()) {
			if (this.servidorIndeaWeb != null)
				return this.servidorIndeaWeb.getNome();
		} else if (this.isTipoServidorPM() || this.isTipoServidorColaboradorEventual())
			return super.getNome();

		return null;
	}
	
	public String getCPf() {
		if (this.isTipoServidorINDEA()) {
			if (this.servidorIndeaWeb != null)
				return this.servidorIndeaWeb.getCpf();
		} else if (this.isTipoServidorPM() || this.isTipoServidorColaboradorEventual())
			return super.getCpf();

		return null;
	}
	
	public String getEmail() {
		if (this.isTipoServidorINDEA()) {
			if (this.servidorIndeaWeb != null)
				return this.servidorIndeaWeb.getEmail();
		} else if (this.isTipoServidorPM() || this.isTipoServidorColaboradorEventual())
			return super.getEmail();

		return null;
	}
	
	public String getCargo() {
		if (this.isTipoServidorINDEA()) {
			if (this.servidorIndeaWeb != null)
				return this.servidorIndeaWeb.getCargo().getNome();
		} else if (this.isTipoServidorPM() || this.isTipoServidorColaboradorEventual())
			return this.tipoServidor.getDescricao();
		

		return null;
	}
	
	public String getMatricula() {
		if (this.isTipoServidorINDEA()) {
			if (this.servidorIndeaWeb != null)
				return this.servidorIndeaWeb.getMatricula();
		} else if (this.isTipoServidorPM() || this.isTipoServidorColaboradorEventual())
			return this.matricula;

		return null;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public void setServidorIndeaWeb(br.gov.mt.indea.prestacaodecontas.entity.dbindea.Servidor servidorIndeaWeb) {
		if (servidorIndeaWeb != null) {
			this.setIdServidor(servidorIndeaWeb.getId());
			this.setNome(servidorIndeaWeb.getNome());
			this.setCpf(servidorIndeaWeb.getCpf());
		}
		this.servidorIndeaWeb = servidorIndeaWeb;
	}

	public br.gov.mt.indea.prestacaodecontas.entity.dbindea.Servidor getServidorIndeaWeb() {
		return servidorIndeaWeb;
	}

	private void setIdServidor(Long idServidor) {
		this.idServidor = idServidor;
	}

	public TipoServidor getTipoServidor() {
		return tipoServidor;
	}

	public void setTipoServidor(TipoServidor tipoServidor) {
		this.tipoServidor = tipoServidor;
	}
	
	public boolean isTipoServidorPM() {
		if (this.tipoServidor == null)
			return false;
		if (this.tipoServidor.equals(TipoServidor.PM))
			return true;
		return false;
	}
	
	public boolean isTipoServidorINDEA() {
		if (this.tipoServidor == null)
			return false;
		if (this.tipoServidor.equals(TipoServidor.INDEA))
			return true;
		return false;
	}
	
	public boolean isTipoServidorColaboradorEventual() {
		if (this.tipoServidor == null)
			return false;
		if (this.tipoServidor.equals(TipoServidor.COLABORADOR_EVENTUAL))
			return true;
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((super.getCpf() == null) ? 0 : super.getCpf().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Servidor other = (Servidor) obj;
		if (super.getCpf() == null) {
			if (other.getCpf() != null)
				return false;
		} else if (!getCpf().equals(other.getCpf()))
			return false;
		return true;
	}
    
}
