package br.gov.mt.indea.prestacaodecontas.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.persistence.Id;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("rawtypes")
public class ReflectionUtil {

	private static final Logger log = LoggerFactory.getLogger(ReflectionUtil.class);

	public static String obterNomeMetodoGET(String nomeAtributo){
		return obterNomeMetodoGET(nomeAtributo, Object.class);
	}
	
	private static String obterNomeMetodo(String nomeAtributo, String tipoMetodo){
		String nomeCampoCapital = nomeAtributo.substring(0,1).toUpperCase() + nomeAtributo.substring(1);
		return tipoMetodo + nomeCampoCapital;
	}
	
	public static String obterNomeMetodoGET(String nomeAtributo, Class<?> type){
		if(type.equals(boolean.class) || type.equals(Boolean.class)){
			return obterNomeMetodo(nomeAtributo, "is");
		}
		return obterNomeMetodo(nomeAtributo, "get");
	}
	
	public static String obterNomeMetodoSET(String nomeAtributo){
		return obterNomeMetodo(nomeAtributo, "set");
	}
	
	 /**
	 * Obtem o m�todo get do atributo passado como parametro
	 * 
	 * @param c
	 * @param nomeCampo
	 * @return
	 */
	public static Method getGetter(Class c, String nomeCampo) {
		Method[] methos = c.getMethods();
		for (Method met : methos) {
			String methodName = "get" + nomeCampo;

			if (met.getName().equalsIgnoreCase(methodName))
				return met;
		}
		return null;
	}
	
	/**
	 * Obtem o m�todo set do atributo passado como parametro
	 * 
	 * @param c
	 * @param nomeCampo
	 * @return
	 */
	public static Method getSetter(Class c, String nomeCampo) {
		Method[] methos = c.getMethods();
		for (Method met : methos) {
			String methodName = "set" + nomeCampo;

			if (met.getName().equalsIgnoreCase(methodName))
				return met;
		}
		return null;
	}
		
	/**
	 * Set Value
	 * @param object
	 * @param nomeCampo
	 * @param params
	 */
	public static void setValue(Object object, String nomeCampo, Object... params){
		if (object!=null && nomeCampo!=null){
			try{
				Method mS = getSetter(object.getClass(), nomeCampo);
				if (mS!=null){
					if (params!=null){
						mS.invoke(object,params);
					}
				}
				
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				log.error(e.getMessage());
			}
		}


	}
	
	/**
	 * M�todo utilizado para obter o atributo mapeado com @Id do Entity
	 * @param entityClass
	 * @return
	 */
	public static String getIdAnnotatedAttribute(Class<?> entityClass) {
		
        if (entityClass == null) return null;
        
        String idAnnotatedAttribute = null;

        for (Field f : entityClass.getDeclaredFields()) {
            Annotation[] annotations = f.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType() == Id.class){
                	idAnnotatedAttribute = f.getName();
                	break;
                }                    
            }
        }

        if (idAnnotatedAttribute == null && entityClass.getSuperclass() != Object.class){
            idAnnotatedAttribute = getIdAnnotatedAttribute(entityClass.getSuperclass());
        }

        return idAnnotatedAttribute;
    }
	
	/**
	 * Get Value
	 * @param object
	 * @param nomeCampo
	 * @param args
	 * @return
	 */
	public static Object getValue(Object object, String nomeCampo, Object... args){
		if (object==null || nomeCampo==null){
			return null;
		}else{
			Object value = null;
			try {
				String atributo = nomeCampo;
				boolean isPath = atributo.contains(".");
				if (isPath){
					atributo = nomeCampo.substring(0, nomeCampo.indexOf("."));
				}
				
				Method mG = getGetter(object.getClass(), atributo);
				if (mG!=null){
					value= mG.invoke(object, args);
				}
				
				if (isPath){
					atributo = nomeCampo.substring(nomeCampo.indexOf(".")+1);
					return getValue(value, atributo, args);
				}
				

			} catch (IllegalAccessException | IllegalArgumentException |InvocationTargetException e ) {
				log.error(e.getMessage());
			} 
			
			return value;
		}
			
	}
	
	public static Field getField(Class<?> clazz, String fieldName) {
	    Class<?> tmpClass = clazz;
	    do {
	        try {
	            Field f = tmpClass.getDeclaredField(fieldName);
	            return f;
	        } catch (NoSuchFieldException e) {
	            tmpClass = tmpClass.getSuperclass();
	        }
	    } while (tmpClass != null);

	    throw new RuntimeException("Field '" + fieldName+ "' not found on class " + clazz);
	}
	
}