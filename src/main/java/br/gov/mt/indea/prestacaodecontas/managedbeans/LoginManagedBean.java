package br.gov.mt.indea.prestacaodecontas.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.net.ConnectException;

import javax.enterprise.context.SessionScoped;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.web.WebAttributes;

import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.prestacaodecontas.security.UserSecurity;
import br.gov.mt.indea.prestacaodecontas.util.ExceptionUtil;
import br.gov.mt.indea.prestacaodecontas.util.FacesMessageUtil;
import br.gov.mt.indea.prestacaodecontas.util.FacesUtil;

@Named("loginManagedBean")
@SessionScoped
@URLBeanName("loginManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "login", pattern = "/login", viewId = "/login.jsf")})
public class LoginManagedBean implements Serializable {

	private static final long serialVersionUID = -7990403902080061184L;

	@Inject
	private FacesContext context;

	@Inject
	private HttpServletRequest request;

	@Inject
	private HttpServletResponse response;
	
	public void preRender() {
		if ("true".equals(request.getParameter("failed"))) {
			Exception ex = (Exception) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
					.get(WebAttributes.AUTHENTICATION_EXCEPTION);

			if (ex != null) {

				if (ex instanceof BadCredentialsException)
					FacesMessageUtil.addInfoFacesMessage(null, "Usu�rio ou senha inv�lidos", "");
				else if (ex instanceof AccountExpiredException)
					FacesMessageUtil.addInfoFacesMessage(null, "Usu�rio expirado", "Entre em contato com o administrador do sistema");
				else if (ex instanceof LockedException)
					FacesMessageUtil.addInfoFacesMessage(null, "Usu�rio inativo", "Entre em contato com o administrador do sistema");
				else{
					Throwable findException = ExceptionUtil.findException(ex, ConnectException.class);
					if (findException != null){
						FacesMessageUtil.addErrorFacesMessage(null, "N�o foi poss�vel conectar com o banco de dados.", "Entre em contato com o administrador");
					} else
						FacesMessageUtil.addErrorFacesMessage(null, "Erro ao tentar realizar login", "Causa: " + ex.getMessage());
				}

			}
		}
	}
	
	public void redirect() throws FacesException, IOException{
		if (this.isUsuarioAutenticado()){
			FacesUtil.doRedirect(FacesContext.getCurrentInstance(), "/dashboard");
		} else
			return;
	}

	public void login() throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/login");
		dispatcher.forward(request, response);

		context.responseComplete();
	}

	public boolean isUsuarioAutenticado() {
		return this.getUserSecurity() != null;
	}

	public UserSecurity getUserSecurity() {
		UserSecurity userSecurity = null;

		UsernamePasswordAuthenticationToken userPrincipal = (UsernamePasswordAuthenticationToken) context
				.getExternalContext().getUserPrincipal();

		if (userPrincipal != null && userPrincipal.getPrincipal() != null)
			userSecurity = (UserSecurity) userPrincipal.getPrincipal();

		return userSecurity;
	}
	
	public void keepAlive(){
	}
	
}
