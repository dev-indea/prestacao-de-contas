package br.gov.mt.indea.prestacaodecontas.memory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.logging.Level;
import java.util.logging.Logger;
 
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class MemoryLeakFixer {
 
	private static final Logger log = Logger
			.getLogger(MemoryLeakFixer.class.getName());
 
	@PreDestroy
	public void clean() {
		try {
			log.info("Fixing Memory Leak from: WFLY-7037");
			Class<?> classUtil = Class
					.forName("com.fasterxml.jackson.databind.util.ClassUtil");
			// https://stackoverflow.com/questions/3301635/change-private-static-final-field-using-java-reflection
			Field sCachedField = classUtil.getDeclaredField("sCached");
			sCachedField.setAccessible(true);
			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(sCachedField,
					sCachedField.getModifiers() & ~Modifier.FINAL);
 
			Class<?> lruMapClass = Class
					.forName("com.fasterxml.jackson.databind.util.LRUMap");
 
			// Get a fresh new lruMap cache
			Object lruMap = lruMapClass
					.getConstructor(int.class, int.class)
					.newInstance(48, 48);
 
			// Reset the cache
			sCachedField.set(null, lruMap);
		} catch (ClassNotFoundException | NoSuchFieldException
				| SecurityException | IllegalArgumentException
				| IllegalAccessException | InstantiationException
				| InvocationTargetException | NoSuchMethodException e) {
			log.log(Level.WARNING, "Could not clean Jackson", e);
		}
	}
}