package br.gov.mt.indea.prestacaodecontas.util;

import java.io.IOException;

import javax.faces.FacesException;
import javax.faces.FactoryFinder;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialResponseWriter;
import javax.faces.context.ResponseWriter;
import javax.faces.render.RenderKit;
import javax.faces.render.RenderKitFactory;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.lang.StringUtils;

public class FacesUtil {
	
	public static void doRedirect(FacesContext context, String redirectPage) throws FacesException, IOException {
		ExternalContext externalContext = context.getExternalContext();

		if ((context.getPartialViewContext().isAjaxRequest() || context.getPartialViewContext().isPartialRequest())
				&& (context.getResponseWriter() == null) && (context.getRenderKit() == null)) {

			ServletResponse response = (ServletResponse) externalContext.getResponse();
			ServletRequest request = (ServletRequest) externalContext.getRequest();

			RenderKitFactory factory = (RenderKitFactory) FactoryFinder
					.getFactory(FactoryFinder.RENDER_KIT_FACTORY);
			RenderKit renderKit = factory.getRenderKit(context,
					context.getApplication().getViewHandler().calculateRenderKitId(context));
			ResponseWriter responseWriter = renderKit.createResponseWriter(response.getWriter(), null,
					request.getCharacterEncoding());
			responseWriter = new PartialResponseWriter(responseWriter);
			context.setResponseWriter(responseWriter);

		}

		externalContext.redirect(externalContext.getRequestContextPath()
				+ (redirectPage != null ? redirectPage : StringUtils.EMPTY));
	}

}
