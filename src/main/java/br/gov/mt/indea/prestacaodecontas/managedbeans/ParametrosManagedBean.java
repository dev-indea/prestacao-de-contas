package br.gov.mt.indea.prestacaodecontas.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.Parametros;
import br.gov.mt.indea.prestacaodecontas.service.ParametrosService;

@Named
@ViewScoped
@URLBeanName("parametrosManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "exibirParametros", pattern = "/parametros/pesquisar", viewId = "/pages/parametros/novo.jsf"),
		@URLMapping(id = "incluirParametros", pattern = "/parametros/incluir", viewId = "/pages/parametros/novo.jsf"),
		@URLMapping(id = "alterarParametros", pattern = "/parametros/alterar/#{parametrosManagedBean.id}", viewId = "/pages/parametros/novo.jsf"),
		@URLMapping(id = "permissoesParametros", pattern = "/parametros/#{parametrosManagedBean.id}/permissoes", viewId = "/pages/parametros/permissoes.jsf")})
public class ParametrosManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -304452587253375002L;

	private Long id;
	
	@Inject
	private Parametros parametros;
	
	@Inject
	private ParametrosService parametrosService;
	
	//private LazyObjectDataModel<Parametros> listaParametros;
	
	private boolean editando = false;
	private boolean incluindo = false;
	
	@PostConstruct
	private void init(){
		List<Parametros> parametrosList = parametrosService.findAll();
		if(parametrosList.isEmpty()) {
			this.parametros = new Parametros();
			this.editando = true;
			this.incluindo = true;
		}else {
			this.parametros = parametrosList.get(0);
			this.id = this.parametros.getId();
		}
	}
	
	public void limpar(){
		List<Parametros> parametrosList = parametrosService.findAll();
			this.incluindo = false;
			this.editando = false;
			
			this.parametros = parametrosList.get(0);
			this.id = this.parametros.getId();
		
	}
	
	@URLAction(mappingId = "alterarParametros", onPostback = false)
	public void alterar() {
		this.setEditando(true);
		this.parametros = parametrosService.findById(this.getId());
	}
	
	public String adicionar() {
		this.parametrosService.saveOrUpdate(this.parametros);
		
//		FacesContext context = FacesContext.getCurrentInstance();
//		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
//		request.getSession().setAttribute("idParametros", this.parametros.getId());
		
		limpar();
		
		return "pretty:exibirParametros";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Parametros getParametros() {
		return parametros;
	}

	public void setParametros(Parametros parametros) {
		this.parametros = parametros;
	}

	public ParametrosService getParametrosService() {
		return parametrosService;
	}

	public void setParametrosService(ParametrosService parametrosService) {
		this.parametrosService = parametrosService;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}

	public boolean isIncluindo() {
		return incluindo;
	}

	public void setIncluindo(boolean incluindo) {
		this.incluindo = incluindo;
	}
	
}
