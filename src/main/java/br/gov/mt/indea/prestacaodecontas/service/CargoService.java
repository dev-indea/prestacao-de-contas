package br.gov.mt.indea.prestacaodecontas.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.prestacaodecontas.entity.dbindea.Cargo;
import br.gov.mt.indea.prestacaodecontas.util.StringUtil;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class CargoService extends PaginableService<Cargo, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(Cargo.class);

	protected CargoService(Class<Cargo> type) {
		super(type);
	}
	
	protected CargoService() {
		super(Cargo.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<Cargo> findAll(String orderBy) {
		List<Cargo> listaCargo = null;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select Cargo")
		   .append("  from Cargo Cargo")
		   .append(" order by :orderBy");
		
		Query query = getSession().createQuery(sql.toString()).setString("orderBy", orderBy);
		listaCargo = query.list();
		
		return listaCargo;
	}
	
	@SuppressWarnings("unchecked")
	public List<Cargo> findByNome(String nome) {
		StringBuilder sql = new StringBuilder();
		
		sql.append(" ")
		   .append("  from Cargo as Cargo")
		   .append(" WHERE ")
		   .append(" Cargo.nome LIKE :nome ");
		
		Query query = getSession().createQuery(sql.toString());
		query.setString("nome",StringUtil.removeAcentos('%'+nome.toUpperCase()+'%'));
		
		List<Cargo> listaCargo = query.list();
		
		return listaCargo;
	}

	@Override
	public void validar(Cargo model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarPersist(Cargo model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarMerge(Cargo model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarDelete(Cargo model) {
		// TODO Auto-generated method stub
		
	}

	
	
	

}
