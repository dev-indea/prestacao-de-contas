package br.gov.mt.indea.prestacaodecontas.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.criterion.Order;

import br.gov.mt.indea.prestacaodecontas.entity.dto.AbstractDTO;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.BaseEntity;
import br.gov.mt.indea.prestacaodecontas.util.ReflectionUtil;

public abstract class PaginableService<T extends BaseEntity<PK>, PK extends Serializable> extends BaseEntityService<T, PK> {

	protected PaginableService(Class<T> type){
		super(type);
	}
	
	protected PaginableService(Class<T> type, Order[] orders) {
		super(type, orders);
	}

    @SuppressWarnings("unchecked")
	public T findByIdFetchAll(PK id) {    	
    	String idAttribute = ReflectionUtil.getIdAnnotatedAttribute(getType());
    	
		StringBuilder hqlBuilder = new StringBuilder();

		hqlBuilder.append("select entity from " + getType().getName() + " as entity ");
		hqlBuilder.append("where entity." + idAttribute + " = :" + idAttribute);

		Query query = getSession().createQuery(hqlBuilder.toString());
		query.setParameter(idAttribute, id);
		
		return (T) query.uniqueResult();
	}
    
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long countAll(){
    	StringBuilder sql = new StringBuilder();
		sql.append("select count(entity) ")
		   .append("  from " + this.getType().getSimpleName() + " as entity");
		
		Query query = getSession().createQuery(sql.toString());
    	
    	
		return (Long) query.uniqueResult();
	}
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long countAll(AbstractDTO dto){
    	StringBuilder sql = new StringBuilder();
		sql.append("select count(entity) ")
		   .append("  from " + this.getType().getSimpleName() + " as entity");
		
		Query query = getSession().createQuery(sql.toString());
    	
		return (Long) query.uniqueResult();
	}
    
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<T> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from " + this.getType().getSimpleName() + " as entity");
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by " + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		return query.list();
    }

}
