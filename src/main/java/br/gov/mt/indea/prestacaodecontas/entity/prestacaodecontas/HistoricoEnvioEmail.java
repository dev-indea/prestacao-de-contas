package br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name="historico_envio_email")
public class HistoricoEnvioEmail extends BaseEntity<Long> implements Serializable, Comparable<HistoricoEnvioEmail>{

	private static final long serialVersionUID = -3972600345667980049L;

	@Id
	@SequenceGenerator(name="historico_envio_email_seq", sequenceName="historico_envio_email_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="historico_envio_email_seq")
	private Long id;
	
	@Column(name="email", length=500)
	private String email;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_envio")
	private Calendar dataEnvio;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_prestacao_de_contas")
	private PrestacaoDeContas prestacaoDeContas;
	
	@Column(name="enviado_com_sucesso")
	private boolean enviadoComSucesso;
	
	@Column(name="motivo_falha", length=500)
	private String motivoFalha;

	public HistoricoEnvioEmail(String destinatario, PrestacaoDeContas prestacaoDeContas, boolean enviadoComSucesso) {
		this.email = destinatario;
		this.prestacaoDeContas = prestacaoDeContas;
		this.enviadoComSucesso = enviadoComSucesso;
		this.dataEnvio = Calendar.getInstance();
	}
	
	public HistoricoEnvioEmail() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Calendar getDataEnvio() {
		return dataEnvio;
	}

	public void setDataEnvio(Calendar dataEnvio) {
		this.dataEnvio = dataEnvio;
	}

	public PrestacaoDeContas getPrestacaoDeContas() {
		return prestacaoDeContas;
	}

	public void setPrestacaoDeContas(PrestacaoDeContas prestacaoDeContas) {
		this.prestacaoDeContas = prestacaoDeContas;
	}

	public boolean isEnviadoComSucesso() {
		return enviadoComSucesso;
	}

	public void setEnviadoComSucesso(boolean enviadoComSucesso) {
		this.enviadoComSucesso = enviadoComSucesso;
	}

	public String getMotivoFalha() {
		return motivoFalha;
	}

	public void setMotivoFalha(String motivoFalha) {
		this.motivoFalha = motivoFalha;
	}

	@Override
	public int compareTo(HistoricoEnvioEmail o) {
		if (this.getDataEnvio().after(o.getDataEnvio()))
			return -1;
		else if (this.getDataEnvio().before(o.getDataEnvio()))
			return 1;
		else
			return 0;
	}
	
}
