package br.gov.mt.indea.prestacaodecontas.enums;

public enum PostgreSQLErrorCodes {
	UNIQUE_VIOLATION("23505", "Registro duplicado"),
	FOREIGN_KEY_VIOLATION("23503", "N�o foi poss�vel excluir o registro. Registro filho localizado"),
	EXCLUSION_VIOLATION("23P01", "N�o foi poss�vel excluir o registro"),
	NOT_NULL_VIOLATION("23502", "Valor n�o pode ser nulo"),
	NO_DATA_FOUND("P0002", "Nenhum registro encontrado"),
	TOO_MANY_ROWS("P0003", "A consulta retornou mais de um registro"),
	CONNECTION_DOES_NOT_EXIST("08003", "N�o foi poss�vel conectar com o banco de dados"),
	CONNECTION_FAILURE("08006", "N�o foi poss�vel conectar com o banco de dados"),
	SQLCLIENT_UNABLE_TO_ESTABLISH_SQLCONNECTION("08001", "N�o foi poss�vel conectar com o banco de dados"),
	SQLSERVER_REJECTED_ESTABLISHMENT_OF_SQLCONNECTION("08004", "N�o foi poss�vel conectar com o banco de dados");
	
    private String vendorCode;
    private String message;

    private PostgreSQLErrorCodes(String vendorCode, String message) {
        this.vendorCode = vendorCode;
        this.message = message;
    }

    public static String getMessage(String vendorCode) {
        for (PostgreSQLErrorCodes valor : values()) {
            if (valor.vendorCode.equals(vendorCode)) {
                return valor.getMessage();
            }
        }
        return "";
    }
    
    public static PostgreSQLErrorCodes getPostgreSQLErrorCodesByVendorCode(String vendorCode){
    	for (PostgreSQLErrorCodes valor : values()) {
            if (valor.vendorCode.equals(vendorCode)) {
                return valor;
            }
        }
        return null;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public String getMessage() {
        return message;
    }
}
