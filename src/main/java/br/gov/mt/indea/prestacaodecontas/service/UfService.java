package br.gov.mt.indea.prestacaodecontas.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;

import br.gov.mt.indea.prestacaodecontas.entity.dbindea.Uf;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class UfService extends PaginableService<Uf, Long> {

	protected UfService() {
		super(Uf.class);
	}
	
	public Uf findByIdFetchAll(Long id){
		Uf uf;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select uf ")
		   .append("  from Uf uf ")
		   .append(" where uf.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		uf = (Uf) query.uniqueResult();
		
		return uf;
	}
	
	public Uf findBySigla(String sigla){
		Uf uf;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select uf ")
		   .append("  from Uf uf ")
		   .append(" where uf.uf = :uf ");
		
		Query query = getSession().createQuery(sql.toString());
		query.setString("uf", sigla);
		uf = (Uf) query.uniqueResult();
		
		return uf;
	}
	
	@SuppressWarnings("unchecked")
	public List<Uf> findAllByUf(Uf uf){
		List<Uf> listaUf = new ArrayList<>();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from Uf entity ")
		   .append("  join fetch entity.uf ")
		   .append(" where entity.uf = :uf ");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("uf", uf);
		listaUf = query.list();
		
		return listaUf;
	}
	
	@SuppressWarnings("unchecked")
	public List<Uf> findAllFetchMunicipio(){
		List<Uf> listaUf = new ArrayList<>();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct entity ")
		   .append("  from Uf as entity ")
		   .append("  join fetch entity.municipios ");
		
		Query query = getSession().createQuery(sql.toString());
		listaUf = query.list();
		
		return listaUf;
	}
	
	@Override
	public void validar(Uf Uf) {

	}

	@Override
	public void validarPersist(Uf Uf) {

	}

	@Override
	public void validarMerge(Uf Uf) {

	}

	@Override
	public void validarDelete(Uf Uf) {

	}

}
