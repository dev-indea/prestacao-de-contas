package br.gov.mt.indea.prestacaodecontas.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.PrestacaoDeContas;
import br.gov.mt.indea.prestacaodecontas.security.UserSecurity;
import br.gov.mt.indea.prestacaodecontas.service.PrestacaoDeContasService;
import br.gov.mt.indea.prestacaodecontas.util.Email;

@Named
@ViewScoped
@URLBeanName("dashBoardManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "dashboard", pattern = "/dashboard", viewId = "/dashboard.jsf")})
public class DashBoardManagedBean implements Serializable{

	private static final long serialVersionUID = 4902041813362871313L;

	@Inject
	UserSecurity userSecurity;
	
	private List<PrestacaoDeContas> listaPrestacaoDeContasComAtraso;
	
	private List<PrestacaoDeContas> listaPrestacaoDeContasComDefeito;
	
	private List<PrestacaoDeContas> filteredPrestacaoDeContasList;
	
	private List<Integer> listaExerciciosAtraso;
	
	@Inject
	private PrestacaoDeContasService prestacaoDeContasService;
	
	@PostConstruct
	private void init(){
		listaPrestacaoDeContasComAtraso = prestacaoDeContasService.findAllComAtraso();
		listaPrestacaoDeContasComDefeito = prestacaoDeContasService.findAllComDefeitos();
		listaExerciciosAtraso = new ArrayList<Integer>(extrairExerciciosPrestacao(listaPrestacaoDeContasComAtraso));
	}
	
	public Set<Integer> extrairExerciciosPrestacao(List<PrestacaoDeContas> listaPrestacao) {
		Set<Integer> listaExercicio = new HashSet<Integer>();
		Calendar cal = Calendar.getInstance();
		
		for(PrestacaoDeContas lista: listaPrestacao) {
			cal.setTime(lista.getDtInicio());
			listaExercicio.add(Integer.valueOf(cal.get(Calendar.YEAR)));
		}
		
		return listaExercicio;
	}
	
	public List<PrestacaoDeContas> getListaPrestacaoDeContasComAtraso() {
		return listaPrestacaoDeContasComAtraso;
	}

	public void setListaPrestacaoDeContasComAtraso(List<PrestacaoDeContas> listaPrestacaoDeContasComAtraso) {
		this.listaPrestacaoDeContasComAtraso = listaPrestacaoDeContasComAtraso;
	}

	public List<PrestacaoDeContas> getListaPrestacaoDeContasComDefeito() {
		return listaPrestacaoDeContasComDefeito;
	}

	public void setListaPrestacaoDeContasComDefeito(List<PrestacaoDeContas> listaPrestacaoDeContasComDefeito) {
		this.listaPrestacaoDeContasComDefeito = listaPrestacaoDeContasComDefeito;
	}

	public void sendEmail(PrestacaoDeContas prestacaoDeContas, String destinatario) {
		Email.sendEmailServidorComAtraso(prestacaoDeContas, destinatario, true);
	}

	public List<PrestacaoDeContas> getFilteredPrestacaoDeContasList() {
		return filteredPrestacaoDeContasList;
	}

	public void setFilteredPrestacaoDeContasList(List<PrestacaoDeContas> filteredPrestacaoDeContasList) {
		this.filteredPrestacaoDeContasList = filteredPrestacaoDeContasList;
	}

	public List<Integer> getListaExerciciosAtraso() {
		return listaExerciciosAtraso;
	}

	public void setListaExerciciosAtraso(List<Integer> listaExerciciosAtraso) {
		this.listaExerciciosAtraso = listaExerciciosAtraso;
	}
	
}