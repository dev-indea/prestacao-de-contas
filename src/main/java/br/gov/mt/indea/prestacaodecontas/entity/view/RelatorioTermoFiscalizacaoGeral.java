package br.gov.mt.indea.prestacaodecontas.entity.view;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class RelatorioTermoFiscalizacaoGeral implements Serializable{
	
	private static final long serialVersionUID = -1199772985943065459L;

	private String urs;

	private String ule;
	
	private String numero;
    
	private Date data_visita;
    
	private Date data_cadastro;
    
	private String nome_revenda;
    
	private String cnpj_revenda;
    
	private BigInteger codigo_revenda;
    
	private String motivo_da_visita;
    
	private Integer apreensoes;
    
	private Integer autuacoes;
    
	private Integer comercializacao;
    
	private Integer comercio_ambulante;
    
	private Integer estocagem_e_armazenagem;
    
	private Integer licenciamento;
    
	private Integer notificacoes;
    
	private Integer prazo_de_validade;
    
	private Integer recebimento_de_biologicos;
    
	private Integer receituarios_especificos;
    
	private Integer registro_produto_mapa;
    
	private Integer renovacao_anual_da_licenca;
    
	private Integer temperatura;
    
	private Integer troca_de_gelo_em_pontos_especificos;

	public String getUrs() {
		return urs;
	}

	public void setUrs(String urs) {
		this.urs = urs;
	}

	public String getUle() {
		return ule;
	}

	public void setUle(String ule) {
		this.ule = ule;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getData_visita() {
		return data_visita;
	}

	public void setData_visita(Date data_visita) {
		this.data_visita = data_visita;
	}

	public Date getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(Date data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	public String getNome_revenda() {
		return nome_revenda;
	}

	public void setNome_revenda(String nome_revenda) {
		this.nome_revenda = nome_revenda;
	}

	public String getCnpj_revenda() {
		return cnpj_revenda;
	}

	public void setCnpj_revenda(String cnpj_revenda) {
		this.cnpj_revenda = cnpj_revenda;
	}

	public BigInteger getCodigo_revenda() {
		return codigo_revenda;
	}

	public void setCodigo_revenda(BigInteger codigo_revenda) {
		this.codigo_revenda = codigo_revenda;
	}

	public String getMotivo_da_visita() {
		return motivo_da_visita;
	}

	public void setMotivo_da_visita(String motivo_da_visita) {
		this.motivo_da_visita = motivo_da_visita;
	}

	public Integer getApreensoes() {
		return apreensoes;
	}

	public void setApreensoes(Integer apreensoes) {
		this.apreensoes = apreensoes;
	}

	public Integer getAutuacoes() {
		return autuacoes;
	}

	public void setAutuacoes(Integer autuacoes) {
		this.autuacoes = autuacoes;
	}

	public Integer getComercializacao() {
		return comercializacao;
	}

	public void setComercializacao(Integer comercializacao) {
		this.comercializacao = comercializacao;
	}

	public Integer getComercio_ambulante() {
		return comercio_ambulante;
	}

	public void setComercio_ambulante(Integer comercio_ambulante) {
		this.comercio_ambulante = comercio_ambulante;
	}

	public Integer getEstocagem_e_armazenagem() {
		return estocagem_e_armazenagem;
	}

	public void setEstocagem_e_armazenagem(Integer estocagem_e_armazenagem) {
		this.estocagem_e_armazenagem = estocagem_e_armazenagem;
	}

	public Integer getLicenciamento() {
		return licenciamento;
	}

	public void setLicenciamento(Integer licenciamento) {
		this.licenciamento = licenciamento;
	}

	public Integer getNotificacoes() {
		return notificacoes;
	}

	public void setNotificacoes(Integer notificacoes) {
		this.notificacoes = notificacoes;
	}

	public Integer getPrazo_de_validade() {
		return prazo_de_validade;
	}

	public void setPrazo_de_validade(Integer prazo_de_validade) {
		this.prazo_de_validade = prazo_de_validade;
	}

	public Integer getRecebimento_de_biologicos() {
		return recebimento_de_biologicos;
	}

	public void setRecebimento_de_biologicos(Integer recebimento_de_biologicos) {
		this.recebimento_de_biologicos = recebimento_de_biologicos;
	}

	public Integer getReceituarios_especificos() {
		return receituarios_especificos;
	}

	public void setReceituarios_especificos(Integer receituarios_especificos) {
		this.receituarios_especificos = receituarios_especificos;
	}

	public Integer getRegistro_produto_mapa() {
		return registro_produto_mapa;
	}

	public void setRegistro_produto_mapa(Integer registro_produto_mapa) {
		this.registro_produto_mapa = registro_produto_mapa;
	}

	public Integer getRenovacao_anual_da_licenca() {
		return renovacao_anual_da_licenca;
	}

	public void setRenovacao_anual_da_licenca(Integer renovacao_anual_da_licenca) {
		this.renovacao_anual_da_licenca = renovacao_anual_da_licenca;
	}

	public Integer getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Integer temperatura) {
		this.temperatura = temperatura;
	}

	public Integer getTroca_de_gelo_em_pontos_especificos() {
		return troca_de_gelo_em_pontos_especificos;
	}

	public void setTroca_de_gelo_em_pontos_especificos(Integer troca_de_gelo_em_pontos_especificos) {
		this.troca_de_gelo_em_pontos_especificos = troca_de_gelo_em_pontos_especificos;
	}

}