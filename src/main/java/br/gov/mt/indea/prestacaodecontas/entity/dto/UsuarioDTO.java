package br.gov.mt.indea.prestacaodecontas.entity.dto;

import java.io.Serializable;

import br.gov.mt.indea.prestacaodecontas.enums.Dominio.AtivoInativo;

public class UsuarioDTO implements AbstractDTO, Serializable {

	private static final long serialVersionUID = 958534186952468759L;

	private String nome;

	private String cpf;
	
	private String username;
	
	private AtivoInativo status;
	
	public boolean isNull(){
		if (nome == null && cpf == null && username == null && status == null)
			return true;
		return false;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public AtivoInativo getStatus() {
		return status;
	}

	public void setStatus(AtivoInativo status) {
		this.status = status;
	}

}
