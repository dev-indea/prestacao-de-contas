package br.gov.mt.indea.prestacaodecontas.util;

import java.io.File;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.faces.context.FacesContext;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.HistoricoEnvioEmail;
import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.PrestacaoDeContas;
import br.gov.mt.indea.prestacaodecontas.event.MailEvent;
import br.gov.mt.indea.prestacaodecontas.service.HistoricoEnvioEmailService;

@Singleton
public class Email {
	
	private static final String EMAIL_EMITENTE = "naoresponda@indea.mt.gov.br";
	
	private static final String NOME_EMITENTE = "Presta��o de Contas Indea-MT";
	
	private static final String USERNAME = "app.indea.cadastroprodutor";
	
	private static final String PASSWORD = "pr0d1nd3@!";
	
	private static final String SMTP = "cuco.mt.gov.br";
	
	@Resource(mappedName = "java:jboss/mail/Gmail")
	private Session mailSession;
	
	
	public void sendSynchronousMail(@Observes(during = TransactionPhase.IN_PROGRESS) MailEvent event) {
		try {
			MimeMessage message = new MimeMessage(mailSession);
			Address[] to = new InternetAddress[] {new InternetAddress(event.getDestinatario())};

			message.setRecipients(Message.RecipientType.TO, to);
			message.setSubject(event.getAssunto());
			message.setSentDate(new Date());
			
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(event.getMensagem(), "text/html; charset=utf-8");
			
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			
			messageBodyPart = new MimeBodyPart();
			
			if (event.getListaAnexo() != null && !event.getListaAnexo().isEmpty()) {
				for (Map.Entry<String, FileDataSource> entry : event.getListaAnexo().entrySet()) {
					messageBodyPart.setDataHandler(new DataHandler(entry.getValue()));
					messageBodyPart.setFileName(entry.getKey() + ".pdf");
				}
				multipart.addBodyPart(messageBodyPart);
			}
			
			message.setContent(multipart);
			Transport.send(message);
		} catch (MessagingException e) {
            throw new RuntimeException(e);
        }
	}

	public static void send(String emailDestinatario, String nomeDestinatario, String assunto, String mensagem) throws EmailException {

		SimpleEmail email = new SimpleEmail();
		// Utilize o hostname do seu provedor de email
//		email.setHostName("smtp.gmail.com");
//		// Quando a porta utilizada n�o � a padr�o (gmail = 465)
//		email.setSmtpPort(465);
//		// Adicione os destinat�rios
//		email.addTo(emailDestinatario, nomeDestinatario);
//		// Configure o seu email do qual enviar�
//		email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
//		// Adicione um assunto
//		email.setSubject(assunto);
//		// Adicione a mensagem do email
//		email.setMsg(mensagem);
//		email.setSSL(true);
//		// Para autenticar no servidor � necess�rio chamar os dois m�todos abaixo
//		email.setAuthentication(USERNAME, PASSWORD);
		
		email.setHostName(SMTP);
        email.setAuthentication(USERNAME, PASSWORD);
        email.addTo(emailDestinatario, "");
        email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
        email.setSubject(assunto);
        email.setMsg(mensagem);
        email.setStartTLSEnabled(true);
        email.getMailSession().getProperties().put("mail.smtp.ssl.trust", SMTP);
        
		email.send();
		
	}
	
	public static void sendEmailForgotPassword(String emailDestinatario, String username, String assunto, String link) throws MalformedURLException, EmailException{
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty("resource.loader", "file");
		ve.setProperty("runtime.log", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "logs" + File.separator + "velocity.log");
		ve.setProperty("file.resource.loader.path", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "templates" + File.separator + "email");
		ve.init();
		
		Template template = ve.getTemplate("emailRedefinirPassword.vm");
		
		HtmlEmail email = new HtmlEmail();
		
		String cidImage1 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_mt.png").toURI().toURL(), "MT");
		String cidImage2 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_indea.png").toURI().toURL(), "Indea");
		
		VelocityContext context = new VelocityContext();
		context.put("image1", "cid:" + cidImage1);
		context.put("image2", "cid:" + cidImage2);
		context.put("username", username);
		context.put("link", link);

		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		
//		email.setHostName("smtp.gmail.com");
//		email.setAuthentication(USERNAME, PASSWORD);
////		email.setStartTLSEnabled(true);
//		email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
////		email.getMailSession().getProperties().put("mail.smtp.ssl.trust", "cuco.mt.gov.br");
//		email.setSmtpPort(465);
//		email.addTo(emailDestinatario, "");
//		email.setSubject(assunto);
//		email.setHtmlMsg(writer.toString());
//		email.setSSL(true);
		
		email.setHostName(SMTP);
        email.setAuthentication(USERNAME, PASSWORD);
        email.addTo(emailDestinatario, "");
        email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
        email.setSubject(assunto);
        email.setHtmlMsg(writer.toString());
        email.setStartTLSEnabled(true);
        email.getMailSession().getProperties().put("mail.smtp.ssl.trust", SMTP);
        
        email.send();
	}
	
	public static void sendEmailUsuarioInativo(String emailDestinatario, String username, String assunto) throws MalformedURLException, EmailException{
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty("resource.loader", "file");
		ve.setProperty("runtime.log", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "logs" + File.separator + "velocity.log");
		ve.setProperty("file.resource.loader.path", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "templates" + File.separator + "email");
		ve.init();
		
		Template template = ve.getTemplate("emailUsuarioInativo.vm");
		
		HtmlEmail email = new HtmlEmail();
		
		String cidImage1 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_mt.png").toURI().toURL(), "MT");
		String cidImage2 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_indea.png").toURI().toURL(), "Indea");
		
		VelocityContext context = new VelocityContext();
		context.put("image1", "cid:" + cidImage1);
		context.put("image2", "cid:" + cidImage2);
		context.put("username", username);

		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		
//		email.setHostName("smtp.gmail.com");
//		email.setAuthentication(USERNAME, PASSWORD);
////		email.setStartTLSEnabled(true);
//		email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
////		email.getMailSession().getProperties().put("mail.smtp.ssl.trust", "cuco.mt.gov.br");
//		email.setSmtpPort(465);
//		email.addTo(emailDestinatario, "");
//		email.setSubject(assunto);
//		email.setHtmlMsg(writer.toString());
//		email.setSSL(true);
		
		email.setHostName(SMTP);
        email.setAuthentication(USERNAME, PASSWORD);
        email.addTo(emailDestinatario, "");
        email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
        email.setSubject(assunto);
        email.setHtmlMsg(writer.toString());
        email.setStartTLSEnabled(true);
        email.getMailSession().getProperties().put("mail.smtp.ssl.trust", SMTP);
        
        email.send();
	}
	
	public static void sendEmailServidorComAtraso(PrestacaoDeContas prestacaoDeContas, String destinatario, boolean retornarMensagem) {
		HistoricoEnvioEmail historicoEnvioEmail = null;
		
		if (StringUtils.isEmpty(destinatario)) {
			historicoEnvioEmail = new HistoricoEnvioEmail(destinatario, prestacaoDeContas, false);
			historicoEnvioEmail.setMotivoFalha("Email n�o cadastrado");
			
			if (retornarMensagem)
				FacesMessageUtil.addWarnContextFacesMessage("N�o foi poss�vel enviar email para o destinat�rio " + destinatario, "Email inv�lido. Verifique o cadastro do servidor/unidade no IndeaWeb");
		}else {
			try {
				Email.sendEmailServidorComAtraso(destinatario, prestacaoDeContas.getServidor().getNome(), "Presta��o de Contas com Atraso OS n� " + prestacaoDeContas.getOs(), prestacaoDeContas);
				
				historicoEnvioEmail = new HistoricoEnvioEmail(destinatario, prestacaoDeContas, true);
				
				if (retornarMensagem)
					FacesMessageUtil.addInfoContextFacesMessage("Email enviado com sucesso para destinat�rio " + destinatario, "");
			} catch (MalformedURLException e) {
				historicoEnvioEmail = new HistoricoEnvioEmail(destinatario, prestacaoDeContas, false);
				historicoEnvioEmail.setMotivoFalha("Email inv�lido");
				
				if (retornarMensagem)
					FacesMessageUtil.addWarnContextFacesMessage("Erro ao enviar email para o destinat�rio " + destinatario, "Informe ao administrador do sistema");
				
				e.printStackTrace();
			} catch (EmailException e) {
				historicoEnvioEmail = new HistoricoEnvioEmail(destinatario, prestacaoDeContas, false);
				historicoEnvioEmail.setMotivoFalha("Email inv�lido");
				
				if (retornarMensagem)
					FacesMessageUtil.addWarnContextFacesMessage("N�o foi poss�vel enviar email para o destinat�rio " + destinatario, "Email inv�lido. Verifique o cadastro do servidor/unidade no IndeaWeb");
				
				e.printStackTrace();
			}
		}
		
		HistoricoEnvioEmailService historicoEnvioEmailService = CDIServiceLocator.getBean(HistoricoEnvioEmailService.class);
		historicoEnvioEmailService.saveOrUpdate(historicoEnvioEmail);
	}
	
	private static void sendEmailServidorComAtraso(String emailDestinatario, String username, String assunto, PrestacaoDeContas prestacaoDeContas) throws MalformedURLException, EmailException{
		StringBuilder sb = new StringBuilder();
		
		sb.append("<p>")
		  	.append("Sr(a)").append(prestacaoDeContas.getServidor().getNome())
		  .append("</p>")
		  .append("<p>")
		  	.append("Notificamos V. S.� para ENTREGA URGENTE do RELAT�RIO F�SICO da OS abaixo, tendo em vista a presta��o de contas do INDEA junto aos �rg�o de controle e fiscaliza��o, bem como junto aos fundos e Minist�rio da Agricultura.")
		  .append("</p>")
		  .append("<p>") 
		  	.append("O.S. n� ")
		  	.append(prestacaoDeContas.getOs())
		  .append("</p>")
		  .append("<p>")
		  	.append("Pedimos urg�ncia na regulariza��o da pend�ncia - PRAZO DE 72 H. Informamos que poder� ficar� impedido de realizar novas Ordens de Servi�os, al�m de estar sujeitos a outras penalidades.")
		  .append("<p>")
		  	.append("Atenciosamente.</br> Financeiro INDEA")
		  .append("</p>");
			
		HtmlEmail email = new HtmlEmail();
		
        email.setHostName(SMTP);
        email.setAuthentication(USERNAME, PASSWORD);
        email.addTo(emailDestinatario, "");
        email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
        email.setSubject(assunto);
        email.setHtmlMsg(sb.toString());
        email.setStartTLSEnabled(true);
        email.getMailSession().getProperties().put("mail.smtp.ssl.trust", SMTP);

        email.send();
	}
	
	public static void sendEmailServidorComContasReprovadas(PrestacaoDeContas prestacaoDeContas, String destinatario, boolean retornarMensagem) {
		HistoricoEnvioEmail historicoEnvioEmail = null;
		
		if (StringUtils.isEmpty(destinatario)) {
			historicoEnvioEmail = new HistoricoEnvioEmail(destinatario, prestacaoDeContas, false);
			historicoEnvioEmail.setMotivoFalha("Email n�o cadastrado");
			
			if (retornarMensagem)
				FacesMessageUtil.addWarnContextFacesMessage("N�o foi poss�vel enviar email para o destinat�rio " + destinatario, "Email inv�lido. Verifique o cadastro do servidor/unidade no IndeaWeb");
		}else {
			try {
				Email.sendEmailServidorComContasReprovadas(destinatario, prestacaoDeContas.getServidor().getNome(), "OS n� " + prestacaoDeContas.getOs() + " REPROVADA", prestacaoDeContas);
				
				historicoEnvioEmail = new HistoricoEnvioEmail(destinatario, prestacaoDeContas, true);
				
				if (retornarMensagem)
					FacesMessageUtil.addInfoContextFacesMessage("Email enviado com sucesso para destinat�rio " + destinatario, "");
			} catch (MalformedURLException e) {
				historicoEnvioEmail = new HistoricoEnvioEmail(destinatario, prestacaoDeContas, false);
				historicoEnvioEmail.setMotivoFalha("Email inv�lido");
				
				if (retornarMensagem)
					FacesMessageUtil.addWarnContextFacesMessage("Erro ao enviar email para o destinat�rio " + destinatario, "Informe ao administrador do sistema");
				
				e.printStackTrace();
			} catch (EmailException e) {
				historicoEnvioEmail = new HistoricoEnvioEmail(destinatario, prestacaoDeContas, false);
				historicoEnvioEmail.setMotivoFalha("Email inv�lido");
				
				if (retornarMensagem)
					FacesMessageUtil.addWarnContextFacesMessage("N�o foi poss�vel enviar email para o destinat�rio " + destinatario, "Email inv�lido. Verifique o cadastro do servidor/unidade no IndeaWeb");
				
				e.printStackTrace();
			}
		}
		
		HistoricoEnvioEmailService historicoEnvioEmailService = CDIServiceLocator.getBean(HistoricoEnvioEmailService.class);
		historicoEnvioEmailService.saveOrUpdate(historicoEnvioEmail);
	}
	
	private static void sendEmailServidorComContasReprovadas(String emailDestinatario, String username, String assunto, PrestacaoDeContas prestacaoDeContas) throws MalformedURLException, EmailException{
		StringBuilder sb = new StringBuilder();
		
		sb.append("<p>")
		  	.append("Sr(a)").append(prestacaoDeContas.getServidor().getNome())
		  .append("</p>")
		  .append("<p>")
		  	.append("Informamos que a Presta��o de Contas referente a OS n.� ")
		  	.append(prestacaoDeContas.getOs())
		  	.append("foi REPROVADA tendo em vista o seguinte motivo: ")
		  	.append(prestacaoDeContas.getMotivoReprovacao())
		  .append("</p>")
		  .append("<p>") 
		  	.append("Pedimos urg�ncia na regulariza��o da pendencia ou devolu��o do recurso recebido.")
		  .append("</p>")
		  
		  .append("<p>") 
		  	.append("Caso n�o regularize informamos que o valor recebido ser� enviado para Desconto em Folha no prazo de 72 horas, a partir do recebimento deste.")
		  .append("</p>")
		  
		  .append("<p>") 
		  	.append("Al�m disso, ficar� impedido de realizar novas Ordens de Servi�os junto ao sistema GV e FIPLAN, al�m de estar sujeitos a outras penalidades.")
		  .append("</p>")
		  	
	  	  .append("<p>")
		  	.append("Atenciosamente.</br> Financeiro INDEA")
		  .append("</p>");
			
		HtmlEmail email = new HtmlEmail();
		
        email.setHostName(SMTP);
        email.setAuthentication(USERNAME, PASSWORD);
        email.addTo(emailDestinatario, "");
        email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
        email.setSubject(assunto);
        email.setHtmlMsg(sb.toString());
        email.setStartTLSEnabled(true);
        email.getMailSession().getProperties().put("mail.smtp.ssl.trust", SMTP);

        email.send();
	}
	
}
