package br.gov.mt.indea.prestacaodecontas.entity.view;

import java.io.Serializable;
import java.util.Date;

public class RelatorioVisitaPropriedadeRuralGeral implements Serializable{

	private static final long serialVersionUID = 1667324840277718104L;
	
	private String urs;
	
	private String municipio;
	
	private String numero;
	
	private Date data_da_visita;
	
	private Date data_cadastro;
	
	private String tipo_estabelecimento;
	
	private String nome;
	
	private String codigo_estabelecimento;
	
	private Integer latitude_grau;
	
	private Integer latitude_minuto;
	
	private Float latitude_segundo;
	
	private Integer longitude_grau;
	
	private Integer longitude_minuto;
	
	private Float longitude_segundo;
	
	private String chave_principal;
	
	private Integer vigilancia_veterinaria;
	
	private Integer cadastramento_ou_atualizacao;
	
	private Integer educacao_sanitaria;
	
	private Integer vacinacao;
	
	private Integer apreensao;
	
	private Integer sacrificio;
	
	private Integer destruicao;
	
	private Integer captura;
	
	private Integer desinfeccao;
	
	private Integer saneamento;
	
	private Integer marcacao;
	
	private Integer isolamento;
	
	private Integer interdicao;
	
	private Integer desinterdicao;
	
	private Integer notificacao;
	
	private Integer autuacao;
	
	private Integer cadastro_de_aglomeracao;
	
	private Integer cadastro_de_evento;
	
	private Integer cadastro_de_recinto;
	
	private Integer denuncia;
	
	private Integer formulario_de_fiscalizacao_sanitaria_de_estabelecimento_avicola;
	
	private Integer inspecao_de_animais;
	
	private Integer laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola;
	
	private Integer termo_de_verificacao;
	
	private Integer vistoria_de_rebanho;
	
	private Integer outro;
    
	private String servidores_que_realizaram_visita;

	public String getUrs() {
		return urs;
	}

	public void setUrs(String urs) {
		this.urs = urs;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getData_da_visita() {
		return data_da_visita;
	}

	public void setData_da_visita(Date data_da_visita) {
		this.data_da_visita = data_da_visita;
	}

	public Date getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(Date data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	public String getTipo_estabelecimento() {
		return tipo_estabelecimento;
	}

	public void setTipo_estabelecimento(String tipo_estabelecimento) {
		this.tipo_estabelecimento = tipo_estabelecimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigo_estabelecimento() {
		return codigo_estabelecimento;
	}

	public void setCodigo_estabelecimento(String codigo_estabelecimento) {
		this.codigo_estabelecimento = codigo_estabelecimento;
	}

	public String getChave_principal() {
		return chave_principal;
	}

	public void setChave_principal(String chave_principal) {
		this.chave_principal = chave_principal;
	}

	public Integer getVigilancia_veterinaria() {
		return vigilancia_veterinaria;
	}

	public void setVigilancia_veterinaria(Integer vigilancia_veterinaria) {
		this.vigilancia_veterinaria = vigilancia_veterinaria;
	}

	public Integer getCadastramento_ou_atualizacao() {
		return cadastramento_ou_atualizacao;
	}

	public void setCadastramento_ou_atualizacao(Integer cadastramento_ou_atualizacao) {
		this.cadastramento_ou_atualizacao = cadastramento_ou_atualizacao;
	}

	public Integer getEducacao_sanitaria() {
		return educacao_sanitaria;
	}

	public void setEducacao_sanitaria(Integer educacao_sanitaria) {
		this.educacao_sanitaria = educacao_sanitaria;
	}

	public Integer getVacinacao() {
		return vacinacao;
	}

	public void setVacinacao(Integer vacinacao) {
		this.vacinacao = vacinacao;
	}

	public Integer getApreensao() {
		return apreensao;
	}

	public void setApreensao(Integer apreensao) {
		this.apreensao = apreensao;
	}

	public Integer getSacrificio() {
		return sacrificio;
	}

	public void setSacrificio(Integer sacrificio) {
		this.sacrificio = sacrificio;
	}

	public Integer getDestruicao() {
		return destruicao;
	}

	public void setDestruicao(Integer destruicao) {
		this.destruicao = destruicao;
	}

	public Integer getCaptura() {
		return captura;
	}

	public void setCaptura(Integer captura) {
		this.captura = captura;
	}

	public Integer getDesinfeccao() {
		return desinfeccao;
	}

	public void setDesinfeccao(Integer desinfeccao) {
		this.desinfeccao = desinfeccao;
	}

	public Integer getSaneamento() {
		return saneamento;
	}

	public void setSaneamento(Integer saneamento) {
		this.saneamento = saneamento;
	}

	public Integer getMarcacao() {
		return marcacao;
	}

	public void setMarcacao(Integer marcacao) {
		this.marcacao = marcacao;
	}

	public Integer getIsolamento() {
		return isolamento;
	}

	public void setIsolamento(Integer isolamento) {
		this.isolamento = isolamento;
	}

	public Integer getInterdicao() {
		return interdicao;
	}

	public void setInterdicao(Integer interdicao) {
		this.interdicao = interdicao;
	}

	public Integer getDesinterdicao() {
		return desinterdicao;
	}

	public void setDesinterdicao(Integer desinterdicao) {
		this.desinterdicao = desinterdicao;
	}

	public Integer getNotificacao() {
		return notificacao;
	}

	public void setNotificacao(Integer notificacao) {
		this.notificacao = notificacao;
	}

	public Integer getAutuacao() {
		return autuacao;
	}

	public void setAutuacao(Integer autuacao) {
		this.autuacao = autuacao;
	}

	public Integer getCadastro_de_aglomeracao() {
		return cadastro_de_aglomeracao;
	}

	public void setCadastro_de_aglomeracao(Integer cadastro_de_aglomeracao) {
		this.cadastro_de_aglomeracao = cadastro_de_aglomeracao;
	}

	public Integer getCadastro_de_evento() {
		return cadastro_de_evento;
	}

	public void setCadastro_de_evento(Integer cadastro_de_evento) {
		this.cadastro_de_evento = cadastro_de_evento;
	}

	public Integer getCadastro_de_recinto() {
		return cadastro_de_recinto;
	}

	public void setCadastro_de_recinto(Integer cadastro_de_recinto) {
		this.cadastro_de_recinto = cadastro_de_recinto;
	}

	public Integer getDenuncia() {
		return denuncia;
	}

	public void setDenuncia(Integer denuncia) {
		this.denuncia = denuncia;
	}

	public Integer getFormulario_de_fiscalizacao_sanitaria_de_estabelecimento_avicola() {
		return formulario_de_fiscalizacao_sanitaria_de_estabelecimento_avicola;
	}

	public void setFormulario_de_fiscalizacao_sanitaria_de_estabelecimento_avicola(
			Integer formulario_de_fiscalizacao_sanitaria_de_estabelecimento_avicola) {
		this.formulario_de_fiscalizacao_sanitaria_de_estabelecimento_avicola = formulario_de_fiscalizacao_sanitaria_de_estabelecimento_avicola;
	}

	public Integer getInspecao_de_animais() {
		return inspecao_de_animais;
	}

	public void setInspecao_de_animais(Integer inspecao_de_animais) {
		this.inspecao_de_animais = inspecao_de_animais;
	}

	public Integer getLaudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola() {
		return laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola;
	}

	public void setLaudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola(
			Integer laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola) {
		this.laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola = laudo_de_inspecao_fisica_e_sanitaria_a_estabelevimento_avicola;
	}

	public Integer getTermo_de_verificacao() {
		return termo_de_verificacao;
	}

	public void setTermo_de_verificacao(Integer termo_de_verificacao) {
		this.termo_de_verificacao = termo_de_verificacao;
	}

	public Integer getVistoria_de_rebanho() {
		return vistoria_de_rebanho;
	}

	public void setVistoria_de_rebanho(Integer vistoria_de_rebanho) {
		this.vistoria_de_rebanho = vistoria_de_rebanho;
	}

	public Integer getOutro() {
		return outro;
	}

	public void setOutro(Integer outro) {
		this.outro = outro;
	}

	public String getServidores_que_realizaram_visita() {
		return servidores_que_realizaram_visita;
	}

	public void setServidores_que_realizaram_visita(String servidores_que_realizaram_visita) {
		this.servidores_que_realizaram_visita = servidores_que_realizaram_visita;
	}

	public Integer getLatitude_grau() {
		return latitude_grau;
	}

	public void setLatitude_grau(Integer latitude_grau) {
		this.latitude_grau = latitude_grau;
	}

	public Integer getLatitude_minuto() {
		return latitude_minuto;
	}

	public void setLatitude_minuto(Integer latitude_minuto) {
		this.latitude_minuto = latitude_minuto;
	}

	public Float getLatitude_segundo() {
		return latitude_segundo;
	}

	public void setLatitude_segundo(Float latitude_segundo) {
		this.latitude_segundo = latitude_segundo;
	}

	public Integer getLongitude_grau() {
		return longitude_grau;
	}

	public void setLongitude_grau(Integer longitude_grau) {
		this.longitude_grau = longitude_grau;
	}

	public Integer getLongitude_minuto() {
		return longitude_minuto;
	}

	public void setLongitude_minuto(Integer longitude_minuto) {
		this.longitude_minuto = longitude_minuto;
	}

	public Float getLongitude_segundo() {
		return longitude_segundo;
	}

	public void setLongitude_segundo(Float longitude_segundo) {
		this.longitude_segundo = longitude_segundo;
	}
	
	
}
