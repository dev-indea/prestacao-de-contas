package br.gov.mt.indea.prestacaodecontas.security;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import br.gov.mt.indea.prestacaodecontas.entity.prestacaodecontas.Usuario;
import br.gov.mt.indea.prestacaodecontas.util.CDIServiceLocator;

public class UserSecurity extends User{

	private static final long serialVersionUID = -5419226623542090801L;
	
	public static final String ANONYMOUS_USER = "anonymousUser"; 
	
	private Usuario usuario;
	
	private String ip;

	public UserSecurity(Usuario usuario, Collection<? extends GrantedAuthority> authorities) {
		super(usuario.getId(), usuario.getPassword(), true, !usuario.isUsuarioExpirado(), true, usuario.isUsuarioAtivo(), authorities);
		this.usuario = usuario;
		this.ip = getIpUsuarioLogado();
	}
	
	private String getIpUsuarioLogado(){
		
		try{
			HttpServletRequest request = CDIServiceLocator.getBean(HttpServletRequest.class);
					
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
			    ipAddress = request.getRemoteAddr();
			} else{
				ipAddress = ipAddress.replaceFirst(",.*", "");
			}
			
			return ipAddress;
		} catch (Exception e) {
			return "127.0.0.1";
		}
		
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public String getIp() {
		return ip;
	}

	public boolean hasAuthorityDigitador(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("digitador"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityGestor(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("gestor"))
				return true;
		}
		return false;
	}
	
	public boolean hasAuthorityAdmin(){
		for (GrantedAuthority authority : this.getAuthorities()) {
			if (authority.getAuthority().equals("admin"))
				return true;
		}
		return false;
	}
	
	
	
}
